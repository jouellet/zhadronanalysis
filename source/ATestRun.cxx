#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <ZTrackAnalysis/TreeMaker.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>
#include <EventLoop/LSFDriver.h>


void ATestRun ()
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/mc15_valid/");
  const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/data17_5TeV/");
  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/mc16_5TeV/");
  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/mc16_valid/");
  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/user/o/olszewsk/workspace/public/evgen/tests/420292_PbPb5TeV");
  SH::ScanDir ().filePattern ("*AOD*.1").scan (sh, inputFilePath); // data17_5TeV run 340718
  //SH::ScanDir ().filePattern ("*AOD.179*.1").scan (sh, inputFilePath); // Z->mumu sample
  //SH::ScanDir ().filePattern ("*AOD.133*.1").scan (sh, inputFilePath); // Z->ee sample
  //SH::ScanDir ().filePattern ("*AOD.208*.1").scan (sh, inputFilePath); // Z->mumu test sample with Hijing overlay
  //SH::ScanDir ().filePattern ("*AOD.186*.1").scan (sh, inputFilePath); // ttbar bkg sample

  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/data18_hi/");
  //SH::ScanDir ().filePattern ("data18_hi.00365502.physics_HardProbes.merge.AOD.f1021_m2037._lb0040._0001.1").scan (sh, inputFilePath);

  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/data15_hi/");
  //SH::ScanDir ().filePattern ("*AOD.166*.1").scan (sh, inputFilePath);

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  job.options()->setDouble (EL::Job::optMaxEvents, 2000); // for testing purposes, limit to run over the first 500 events only! // for tutorial
  
  std::string output = "myOutput";
  EL::OutputStream outputFile  (output);
  job.outputAdd (outputFile);

  // add our algorithm to the job
  TreeMaker *alg = new TreeMaker;
  alg->m_collisionSystem = pp17;
  alg->m_dataType = Collisions;
  alg->m_outputName = output;
  

  // set the name of the algorithm (this is the name use with messages)
  alg->SetName ("AnalysisAlg");

  job.algsAdd (alg);

  // Standard driver
  EL::DirectDriver driver;

  //// Condor driver
  //EL::CondorDriver driver;
  //job.options()->setDouble (EL::Job::optFilesPerWorker, 5);
  //driver.options()->setString (EL::Job::optSubmitFlags, "-terse");
  //driver.shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";

  /***** Job submission *****/
 
  job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1); 
  // process the job using the driver
  driver.submit (job, "submitDir"); // for direct driver
  //driver.submitOnly (job, "submitDir");   // for running on condor

  /***** End job submission *****/

}
