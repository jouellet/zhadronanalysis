#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <ZTrackAnalysis/TreeMaker.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>

void Submit (const std::string submitDir, const std::string fileName, const std::string outFileName, const std::string dataType, const std::string collSys) 
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;
  
  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.
  SH::scanRucio (sh, fileName);
   
  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");
  sh.setMetaString ("nc_grid_filter", "*AOD*");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  //job.options ()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!

  // define an output and an ntuple associated to that output
  EL::OutputStream output  ("myOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  job.algsAdd (ntuple);

  // add our algorithm to the job
  TreeMaker *alg = new TreeMaker;

  cout << "Collision system = " << flush;
  if (collSys == "PbPb18") { 
    cout << "PbPb18" << endl;
    alg->m_collisionSystem = PbPb18;
  }
  else if (collSys == "pp17") {
    cout << "pp17" << endl;
    alg->m_collisionSystem = pp17;
  }
  else if (collSys == "Pbp16") {
    cout << "Pbp16" << endl;
    alg->m_collisionSystem = Pbp16;
  }
  else if (collSys == "pPb16") {
    cout << "pPb16" << endl;
    alg->m_collisionSystem = pPb16;
  }
  else if (collSys == "PbPb15") {
    cout << "PbPb15" << endl;
    alg->m_collisionSystem = PbPb15;
  }
  else if (collSys == "pp15") {
    cout << "pp15" << endl;
    alg->m_collisionSystem = pp15;
  }
  else {
    std::cout << "Invalid collision system! Exiting" << std::endl;
    return;
  }

  cout << "Data type = " << flush;
  if (dataType == "Collisions") { 
    cout << "Collisions" << endl;
    alg->m_dataType = Collisions;
  }
  else if (dataType == "MCSignal") {
    cout << "MCSignal" << endl;
    alg->m_dataType = MCSignal;
  }
  else if (dataType == "MCDataOverlay") {
    cout << "MCDataOverlay" << endl;
    alg->m_dataType = MCDataOverlay;
  }
  else if (dataType == "MCHijing") {
    cout << "MCHijing" << endl;
    alg->m_dataType = MCHijing;
  }
  else if (dataType == "MCHijingOverlay") {
    cout << "MCHijingOverlay" << endl;
    alg->m_dataType = MCHijingOverlay;
  }
  else {
    std::cout << "Invalid data type! Exiting" << std::endl;
    return;
  }

  alg->m_outputName = "myOutput"; // give the name of the output to our algorithm

  // set the name of the algorithm (this is the name use with
  // messages)
  alg->SetName ("AnalysisAlg");

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);
  job.options ()->setDouble(EL::Job::optRemoveSubmitDir, 1);

  // make the driver we want to use:
  EL::PrunDriver driver; // Driver to run on the grid
  driver.options ()->setString("nc_outputSampleName", outFileName.c_str ()); // Rename the output to be a bit shorter.
  driver.options ()->setString (EL::Job::optGridNFilesPerJob, "5");
  

  // process the job using the driver
  driver.submitOnly (job, submitDir);

}
