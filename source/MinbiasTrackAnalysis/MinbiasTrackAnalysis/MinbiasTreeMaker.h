#ifndef MinbiasTrackAnalysis_MinbiasTreeMaker_H
#define MinbiasTrackAnalysis_MinbiasTreeMaker_H

#include <TTree.h>
#include <TFile.h>
#include <TH2.h>
#include <TEfficiency.h>
#include <TRandom3.h>

#include <EventLoop/Algorithm.h>
#include <AsgTools/AnaToolHandle.h>

// Trigger
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include <TrigDecisionTool/TrigDecisionTool.h>

// ZDC
#include "ZdcAnalysis/ZdcAnalysisTool.h"

// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

// HI event includes
#include "xAODHIEvent/HIEventShapeContainer.h"

// Track selection tool
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// MC truth classification
#include "MCTruthClassifier/MCTruthClassifier.h"

#include <vector>

enum CollisionSystem { pp15 = 0, PbPb15 = 1, pPb16 = 2, Pbp16 = 3, pp17 = 4, PbPb18 = 5 };
enum DataType { Collisions = 0, MCSignal = 1, MCDataOverlay = 2, MCHijing = 3, MCHijingOverlay = 4 };

class MinbiasTreeMaker : public EL::Algorithm
{
private:

  const float m_trkPtCut = 1;
  const float m_truthTrkPtCut = 0.7;
  const int m_nside = 1;
  const std::string m_oop_fname = "all_fit.root";
  const std::string filePath = "$UserAnalysis_DIR/data/MinbiasTrackAnalysis";

  TH1D* m_oop_hMean = nullptr;
  TH1D* m_oop_hSigma = nullptr;


public:
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  // configuration variables
  CollisionSystem m_collisionSystem;
  DataType m_dataType;
  std::string m_outputName;

  TTree* m_tree; //!

  // event info 
  bool m_b_passesToroid; //! 
  unsigned int m_b_runNum; //!
  unsigned int m_b_lbn; //!
  unsigned int m_b_evtNum; //!
  bool m_b_isOOTPU; //!
  bool m_b_BlayerDesyn; //!

  // collision info
  float m_b_actualInteractionsPerCrossing; //!
  float m_b_averageInteractionsPerCrossing; //!

  // truth event info
  std::vector<float> m_b_mcEventWeights; //!
  int m_b_nTruthEvt; //!
  int m_b_nPart1[5]; //!
  int m_b_nPart2[5]; //!
  float m_b_impactParameter[5]; //!
  int m_b_nColl[5]; //!
  float m_b_sigmaInelasticNN[5]; //!
  int m_b_nSpectatorNeutrons[5]; //!
  int m_b_nSpectatorProtons[5]; //!
  float m_b_eccentricity[5]; //!
  float m_b_eventPlaneAngle[5]; //!

  // trigger info
  bool m_b_HLT_mb_sptrk; //!
  float m_b_HLT_mb_sptrk_prescale; //!
  bool m_b_HLT_mb_sptrk_L1ZDC_A_C_VTE50; //!
  float m_b_HLT_mb_sptrk_L1ZDC_A_C_VTE50_prescale; //!
  bool m_b_HLT_noalg_pc_L1TE50_VTE600_0ETA49; //!
  float m_b_HLT_noalg_pc_L1TE50_VTE600_0ETA49_prescale; //!
  bool m_b_HLT_noalg_cc_L1TE600_0ETA49; //!
  float m_b_HLT_noalg_cc_L1TE600_0ETA49_prescale; //!
  bool m_b_HLT_noalg_mb_L1TE50; //!
  float m_b_HLT_noalg_mb_L1TE50_prescale; //!
  bool m_b_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50; //!
  float m_b_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_prescale; //!

  // vertex info
  int m_b_nvert; //!
  float m_b_vert_x[30]; //!
  float m_b_vert_y[30]; //!
  float m_b_vert_z[30]; //!
  int m_b_vert_ntrk[30]; //!
  int m_b_vert_type[30]; //!
  float m_b_vert_sumpt[30]; //!

  // FCal Et info
  float m_b_fcalA_et; //!
  float m_b_fcalC_et; //!
  float m_b_fcalA_et_Cos2; //!
  float m_b_fcalC_et_Cos2; //!
  float m_b_fcalA_et_Sin2; //!
  float m_b_fcalC_et_Sin2; //!
  float m_b_fcalA_et_Cos3; //!
  float m_b_fcalC_et_Cos3; //!
  float m_b_fcalA_et_Sin3; //!
  float m_b_fcalC_et_Sin3; //!
  float m_b_fcalA_et_Cos4; //!
  float m_b_fcalC_et_Cos4; //!
  float m_b_fcalA_et_Sin4; //!
  float m_b_fcalC_et_Sin4; //!

  // ZDC
  float  m_b_ZdcCalibEnergy_A; //!
  float  m_b_ZdcCalibEnergy_C; //!
  bool   m_b_L1_ZDC_A; //!
  bool   m_b_L1_ZDC_A_tbp; //!
  bool   m_b_L1_ZDC_A_tap; //!
  bool   m_b_L1_ZDC_A_tav; //!
  float  m_b_L1_ZDC_A_prescale; //!
  bool   m_b_L1_ZDC_C; //!
  bool   m_b_L1_ZDC_C_tbp; //!
  bool   m_b_L1_ZDC_C_tap; //!
  bool   m_b_L1_ZDC_C_tav; //!
  float  m_b_L1_ZDC_C_prescale; //!

  // sum of gaps for UPC background
  float m_b_clusterOnly_sum_gap_A; //!
  float m_b_clusterOnly_sum_gap_C; //!
  float m_b_clusterOnly_edge_gap_A; //!
  float m_b_clusterOnly_edge_gap_C; //!
  float m_b_sum_gap_A; //!
  float m_b_sum_gap_C; //!
  float m_b_edge_gap_A; //!
  float m_b_edge_gap_C; //!

  // tracking info (0th or primary vertex only)
  int m_b_ntrk; //!
  float m_b_trk_pt[10000]; //!
  float m_b_trk_eta[10000]; //!
  float m_b_trk_phi[10000]; //!
  float m_b_trk_charge[10000]; //!
  bool m_b_trk_HItight[10000]; //!
  bool m_b_trk_HIloose[10000]; //!
  bool m_b_trk_TightPrimary[10000]; //!
  float m_b_trk_d0[10000]; //!
  float m_b_trk_d0sig[10000]; //!
  float m_b_trk_z0[10000]; //!
  float m_b_trk_z0sig[10000]; //!
  float m_b_trk_theta[10000]; //!
  float m_b_trk_vz[10000]; //!
  int m_b_trk_nBLayerHits[10000]; //!
  int m_b_trk_nBLayerSharedHits[10000]; //!
  int m_b_trk_nPixelHits[10000]; //!
  int m_b_trk_nPixelDeadSensors[10000]; //!
  int m_b_trk_nPixelSharedHits[10000]; //!
  int m_b_trk_nSCTHits[10000]; //!
  int m_b_trk_nSCTDeadSensors[10000]; //!
  int m_b_trk_nSCTSharedHits[10000]; //!
  int m_b_trk_nTRTHits[10000]; //!
  int m_b_trk_nTRTSharedHits[10000]; //!

  float m_b_trk_prob_truth[10000]; //!
  float m_b_trk_truth_pt[10000]; //!
  float m_b_trk_truth_eta[10000]; //!
  float m_b_trk_truth_phi[10000]; //!
  float m_b_trk_truth_charge[10000]; //!
  int m_b_trk_truth_type[10000]; //!
  int m_b_trk_truth_orig[10000]; //!
  int m_b_trk_truth_barcode[10000]; //!
  int m_b_trk_truth_pdgid[10000]; //!
  float m_b_trk_truth_vz[10000]; //!
  int m_b_trk_truth_nIn[10000]; //!
  bool m_b_trk_truth_isHadron[10000]; //!

  // truth track info
  int m_b_truth_trk_n; //!
  float m_b_truth_trk_pt[10000]; //!
  float m_b_truth_trk_eta[10000]; //!
  float m_b_truth_trk_phi[10000]; //!
  float m_b_truth_trk_charge[10000]; //!
  int m_b_truth_trk_pdgid[10000]; //!
  int m_b_truth_trk_barcode[10000]; //!
  bool m_b_truth_trk_isHadron[10000]; //!


  TRandom3* rndm; //!

  // Trigger tools
  Trig::TrigDecisionTool* m_trigDecisionTool; //!
  TrigConf::xAODConfigTool* m_trigConfigTool; //!

  // Zdc Tool
  ZDC::ZdcAnalysisTool* m_zdcAnalysisTool; //!

  // GRL tools
  GoodRunsListSelectionTool* m_pp17_grl; //!
  GoodRunsListSelectionTool* m_PbPb15_grl; //!
  GoodRunsListSelectionTool* m_PbPb18_grl; //!
  GoodRunsListSelectionTool* m_PbPb18_ignoreToroid_grl; //!

  // Track selection tools
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHITight; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHILoose; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolTightPrimary; //!

  // MC Truth Classifier
  MCTruthClassifier* m_mcTruthClassifier; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // this is a standard constructor
  MinbiasTreeMaker ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  bool isDesynEvent (const int _runNumber, const int _lumiBlock);
  bool is_Outpileup (const xAOD::HIEventShapeContainer& evShCont, const int nTrack);
  bool passSinfigCut (float eta, int cellsigsamp, float cellsig);
  float CellSigCut (float x);
  void ClusterGapCut (std::vector <float>& clus);
  void GapCut (std::vector<float>& trks, std::vector<float>& clus);
  uint8_t getSum (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType);

  // this is needed to distribute the algorithm to the workers
  ClassDef(MinbiasTreeMaker, 1);
};

#endif
