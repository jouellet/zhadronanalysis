#ifndef MinbiasTrackAnalysis_LINKDEF_H
#define MinbiasTrackAnalysis_LINKDEF_H

// Some common definitions:
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

// Declare the class(es) to generate dictionaries for:
#pragma link C++ class MinbiasTreeMaker+;

#endif // MinbiasTrackAnalysis_LINKDEF_H
