#include <MinbiasTrackAnalysis/MinbiasTreeMaker.h>

// EventLoop includes
#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

// root includes
#include <TFile.h>
#include <TSystem.h>
#include <TRandom.h>

// xAOD includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"

// Tracking & vertex includes
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

// Truth includes
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"

// ZDC includes
#include "xAODForward/ZdcModuleContainer.h"

// Calo clusters for use in UPC bkg.
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloCluster.h"

// Path resolver tool
#include <PathResolver/PathResolver.h>

// this is needed to distribute the algorithm to the workers
ClassImp(MinbiasTreeMaker)


MinbiasTreeMaker :: MinbiasTreeMaker () {
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  rndm = nullptr;

  m_trigDecisionTool = nullptr;
  m_trigConfigTool = nullptr;

  m_pp17_grl = nullptr;
  m_PbPb15_grl = nullptr;
  m_PbPb18_grl = nullptr;
  m_PbPb18_ignoreToroid_grl = nullptr;

  m_trackSelectionToolHITight = nullptr;
  m_trackSelectionToolHILoose = nullptr;
  m_trackSelectionToolTightPrimary = nullptr;
}



EL::StatusCode MinbiasTreeMaker :: setupJob (EL::Job& job) {
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD (); 
 
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MinbiasTreeMaker :: histInitialize () {
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk()->getOutputFile (m_outputName);
 
  //if (m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15)
  //  m_tree = new TTree ("PbPbTrackTree", "PbPbTrackTree");
  //else if (m_collisionSystem == pp17)
  //  m_tree = new TTree ("ppTrackTree", "ppTrackTree");
  //else {
  //  Error ("histInitialize ()", "Unsupported collision system!");
  //  return EL::StatusCode::FAILURE;
  //}
  m_tree = new TTree ("bush", "bush");
  m_tree->SetDirectory (outputFile);

  // event info 
  m_tree->Branch ("passes_toroid",  &m_b_passesToroid,  "passes_toroid/O");
  m_tree->Branch ("run_number",     &m_b_runNum,        "run_number/i");
  m_tree->Branch ("lumi_block",     &m_b_lbn,           "lumi_block/i"); 
  m_tree->Branch ("event_number",   &m_b_evtNum,        "event_number/i");
  m_tree->Branch ("isOOTPU",        &m_b_isOOTPU,       "isOOTPU/O");
  m_tree->Branch ("BlayerDesyn",    &m_b_BlayerDesyn,   "BlayerDesyn/O");

  // collision info
  m_tree->Branch ("actualInteractionsPerCrossing",  &m_b_actualInteractionsPerCrossing,   "actualInteractionsPerCrossing/F");
  m_tree->Branch ("averageInteractionsPerCrossing", &m_b_averageInteractionsPerCrossing,  "averageInteractionsPerCrossing/F");

  if (m_dataType == MCHijing || m_dataType == MCHijingOverlay) {
    m_tree->Branch ("mcEventWeights",     &m_b_mcEventWeights);
    m_tree->Branch ("nTruthEvt",          &m_b_nTruthEvt,           "nTruthEvt/I");
    m_tree->Branch ("nPart1",             &m_b_nPart1,              "nPart1[nTruthEvt]/I");
    m_tree->Branch ("nPart2",             &m_b_nPart2,              "nPart2[nTruthEvt]/I");
    m_tree->Branch ("impactParameter",    &m_b_impactParameter,     "impactParameter[nTruthEvt]/F");
    m_tree->Branch ("nColl",              &m_b_nColl,               "nColl[nTruthEvt]/I");
    m_tree->Branch ("nSpectatorNeutrons", &m_b_nSpectatorNeutrons,  "nSpectatorNeutrons[nTruthEvt]/I");
    m_tree->Branch ("nSpectatorProtons",  &m_b_nSpectatorProtons,   "nSpectatorProtons[nTruthEvt]/I");
    m_tree->Branch ("eccentricity",       &m_b_eccentricity,        "eccentricity[nTruthEvt]/F");
    m_tree->Branch ("eventPlaneAngle",    &m_b_eventPlaneAngle,     "eventPlaneAngle[nTruthEvt]/F");
  }

  if (m_dataType == Collisions) {
    if (m_collisionSystem == pp17) {
      m_tree->Branch ("HLT_mb_sptrk", &m_b_HLT_mb_sptrk);
      m_tree->Branch ("HLT_mb_sptrk_prescale", &m_b_HLT_mb_sptrk_prescale);
    }

    if (m_collisionSystem == PbPb18) {
      m_tree->Branch ("HLT_mb_sptrk_L1ZDC_A_C_VTE50", &m_b_HLT_mb_sptrk_L1ZDC_A_C_VTE50);
      m_tree->Branch ("HLT_mb_sptrk_L1ZDC_A_C_VTE50_prescale", &m_b_HLT_mb_sptrk_L1ZDC_A_C_VTE50_prescale);
      m_tree->Branch ("HLT_noalg_pc_L1TE50_VTE600.0ETA49", &m_b_HLT_noalg_pc_L1TE50_VTE600_0ETA49);
      m_tree->Branch ("HLT_noalg_pc_L1TE50_VTE600.0ETA49_prescale", &m_b_HLT_noalg_pc_L1TE50_VTE600_0ETA49_prescale);
      m_tree->Branch ("HLT_noalg_cc_L1TE600_0ETA49", &m_b_HLT_noalg_cc_L1TE600_0ETA49);
      m_tree->Branch ("HLT_noalg_cc_L1TE600_0ETA49_prescale", &m_b_HLT_noalg_cc_L1TE600_0ETA49_prescale);
    }

    if (m_collisionSystem == PbPb15) {
      m_tree->Branch ("HLT_noalg_mb_L1TE50", &m_b_HLT_noalg_mb_L1TE50);
      m_tree->Branch ("HLT_noalg_mb_L1TE50_prescale", &m_b_HLT_noalg_mb_L1TE50_prescale);
      m_tree->Branch ("HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50", &m_b_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50);
      m_tree->Branch ("HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_prescale", &m_b_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_prescale);
    }
  }


  // vertices
  m_tree->Branch ("nvert",      &m_b_nvert,       "nvert/I");
  m_tree->Branch ("vert_x",     &m_b_vert_x,      "vert_x[nvert]/F");
  m_tree->Branch ("vert_y",     &m_b_vert_y,      "vert_y[nvert]/F");
  m_tree->Branch ("vert_z",     &m_b_vert_z,      "vert_z[nvert]/F");
  m_tree->Branch ("vert_ntrk",  &m_b_vert_ntrk,   "vert_ntrk[nvert]/I");
  m_tree->Branch ("vert_type",  &m_b_vert_type,   "vert_type[nvert]/I");
  m_tree->Branch ("vert_sumpt", &m_b_vert_sumpt,  "vert_sumpt[nvert]/F");

  // forward calorimeters
  m_tree->Branch ("fcalA_et",       &m_b_fcalA_et,      "fcalA_et/F");
  m_tree->Branch ("fcalC_et",       &m_b_fcalC_et,      "fcalC_et/F");
  m_tree->Branch ("fcalA_et_Cos2",  &m_b_fcalA_et_Cos2, "fcalA_et_Cos2/F");
  m_tree->Branch ("fcalC_et_Cos2",  &m_b_fcalC_et_Cos2, "fcalC_et_Cos2/F");
  m_tree->Branch ("fcalA_et_Sin2",  &m_b_fcalA_et_Sin2, "fcalA_et_Sin2/F");
  m_tree->Branch ("fcalC_et_Sin2",  &m_b_fcalC_et_Sin2, "fcalC_et_Sin2/F");
  m_tree->Branch ("fcalA_et_Cos3",  &m_b_fcalA_et_Cos3, "fcalA_et_Cos3/F");
  m_tree->Branch ("fcalC_et_Cos3",  &m_b_fcalC_et_Cos3, "fcalC_et_Cos3/F");
  m_tree->Branch ("fcalA_et_Sin3",  &m_b_fcalA_et_Sin3, "fcalA_et_Sin3/F");
  m_tree->Branch ("fcalC_et_Sin3",  &m_b_fcalC_et_Sin3, "fcalC_et_Sin3/F");
  m_tree->Branch ("fcalA_et_Cos4",  &m_b_fcalA_et_Cos4, "fcalA_et_Cos4/F");
  m_tree->Branch ("fcalC_et_Cos4",  &m_b_fcalC_et_Cos4, "fcalC_et_Cos4/F");
  m_tree->Branch ("fcalA_et_Sin4",  &m_b_fcalA_et_Sin4, "fcalA_et_Sin4/F");
  m_tree->Branch ("fcalC_et_Sin4",  &m_b_fcalC_et_Sin4, "fcalC_et_Sin4/F");

  // ZDC
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18) {
    m_tree->Branch ("ZdcCalibEnergy_A",  &m_b_ZdcCalibEnergy_A,   "ZdcCalibEnergy_A/F");
    m_tree->Branch ("ZdcCalibEnergy_C",  &m_b_ZdcCalibEnergy_C,   "ZdcCalibEnergy_C/F");
    m_tree->Branch ("L1_ZDC_A",          &m_b_L1_ZDC_A,           "L1_ZDC_A/O");
    m_tree->Branch ("L1_ZDC_A_tbp",      &m_b_L1_ZDC_A_tbp,       "L1_ZDC_A_tbp/O");
    m_tree->Branch ("L1_ZDC_A_tap",      &m_b_L1_ZDC_A_tap,       "L1_ZDC_A_tap/O");
    m_tree->Branch ("L1_ZDC_A_tav",      &m_b_L1_ZDC_A_tav,       "L1_ZDC_A_tav/O");
    m_tree->Branch ("L1_ZDC_A_prescale", &m_b_L1_ZDC_A_prescale,  "L1_ZDC_A_prescale/F");
    m_tree->Branch ("L1_ZDC_C",          &m_b_L1_ZDC_C,           "L1_ZDC_C/O");
    m_tree->Branch ("L1_ZDC_C_tbp",      &m_b_L1_ZDC_C_tbp,       "L1_ZDC_C_tbp/O");
    m_tree->Branch ("L1_ZDC_C_tap",      &m_b_L1_ZDC_C_tap,       "L1_ZDC_C_tap/O");
    m_tree->Branch ("L1_ZDC_C_tav",      &m_b_L1_ZDC_C_tav,       "L1_ZDC_C_tav/O");
    m_tree->Branch ("L1_ZDC_C_prescale", &m_b_L1_ZDC_C_prescale,  "L1_ZDC_C_prescale/F");
  }

  // sum of gaps and edge gaps
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18) {
    m_tree->Branch("cluster_sumGap_A",  &m_b_clusterOnly_sum_gap_A,   "cluster_sumGap_A/F");
    m_tree->Branch("cluster_sumGap_C",  &m_b_clusterOnly_sum_gap_C,   "cluster_sumGap_C/F");
    m_tree->Branch("cluster_edgeGap_A", &m_b_clusterOnly_edge_gap_A,  "cluster_edgeGap_A/F");
    m_tree->Branch("cluster_edgeGap_C", &m_b_clusterOnly_edge_gap_C,  "cluster_edgeGap_C/F");
    m_tree->Branch("sumGap_A",          &m_b_sum_gap_A,               "sumGap_A/F");
    m_tree->Branch("sumGap_C",          &m_b_sum_gap_C,               "sumGap_C/F");
    m_tree->Branch("edgeGap_A",         &m_b_edge_gap_A,              "edgeGap_A/F");
    m_tree->Branch("edgeGap_C",         &m_b_edge_gap_C,              "edgeGap_C/F");
  }

  m_tree->Branch ("ntrk",                   &m_b_ntrk,                  "ntrk/I");
  m_tree->Branch ("trk_pt",                 &m_b_trk_pt,                "trk_pt[ntrk]/F");
  m_tree->Branch ("trk_eta",                &m_b_trk_eta,               "trk_eta[ntrk]/F");
  m_tree->Branch ("trk_phi",                &m_b_trk_phi,               "trk_phi[ntrk]/F");
  m_tree->Branch ("trk_charge",             &m_b_trk_charge,            "trk_charge[ntrk]/F");
  m_tree->Branch ("trk_HItight",            &m_b_trk_HItight,           "trk_HItight[ntrk]/O");
  m_tree->Branch ("trk_HIloose",            &m_b_trk_HIloose,           "trk_HIloose[ntrk]/O");
  m_tree->Branch ("trk_TightPrimary",       &m_b_trk_TightPrimary,      "trk_TightPrimary[ntrk]/O");
  m_tree->Branch ("trk_d0",                 &m_b_trk_d0,                "trk_d0[ntrk]/F");
  m_tree->Branch ("trk_d0sig",              &m_b_trk_d0sig,             "trk_d0sig[ntrk]/F");
  m_tree->Branch ("trk_z0",                 &m_b_trk_z0,                "trk_z0[ntrk]/F");
  m_tree->Branch ("trk_z0sig",              &m_b_trk_z0sig,             "trk_z0sig[ntrk]/F");
  m_tree->Branch ("trk_theta",              &m_b_trk_theta,             "trk_theta[ntrk]/F");
  m_tree->Branch ("trk_vz",                 &m_b_trk_vz,                "trk_vz[ntrk]/F");
  m_tree->Branch ("trk_nBLayerHits",        &m_b_trk_nBLayerHits,       "trk_nBLayerHits[ntrk]/I");
  m_tree->Branch ("trk_nBLayerSharedHits",  &m_b_trk_nBLayerSharedHits, "trk_nBLayerSharedHits[ntrk]/I");
  m_tree->Branch ("trk_nPixelHits",         &m_b_trk_nPixelHits,        "trk_nPixelHits[ntrk]/I");
  m_tree->Branch ("trk_nPixelDeadSensors",  &m_b_trk_nPixelDeadSensors, "trk_nPixelDeadSensors[ntrk]/I");
  m_tree->Branch ("trk_nPixelSharedHits",   &m_b_trk_nPixelSharedHits,  "trk_nPixelSharedHits[ntrk]/I");
  m_tree->Branch ("trk_nSCTHits",           &m_b_trk_nSCTHits,          "trk_nSCTHits[ntrk]/I");
  m_tree->Branch ("trk_nSCTDeadSensors",    &m_b_trk_nSCTDeadSensors,   "trk_nSCTDeadSensors[ntrk]/I");
  m_tree->Branch ("trk_nSCTSharedHits",     &m_b_trk_nSCTSharedHits,    "trk_nSCTSharedHits[ntrk]/I");
  m_tree->Branch ("trk_nTRTHits",           &m_b_trk_nTRTHits,          "trk_nTRTHits[ntrk]/I");
  m_tree->Branch ("trk_nTRTSharedHits",     &m_b_trk_nTRTSharedHits,    "trk_nTRTSharedHits[ntrk]/I");


  if (m_dataType != Collisions) {
    m_tree->Branch ("trk_prob_truth",     &m_b_trk_prob_truth,      "trk_prob_truth[ntrk]/F");
    m_tree->Branch ("trk_truth_pt",       &m_b_trk_truth_pt,        "trk_truth_pt[ntrk]/F");
    m_tree->Branch ("trk_truth_eta",      &m_b_trk_truth_eta,       "trk_truth_eta[ntrk]/F");
    m_tree->Branch ("trk_truth_phi",      &m_b_trk_truth_phi,       "trk_truth_phi[ntrk]/F");
    m_tree->Branch ("trk_truth_charge",   &m_b_trk_truth_charge,    "trk_truth_charge[ntrk]/F");
    m_tree->Branch ("trk_truth_type",     &m_b_trk_truth_type,      "trk_truth_type[ntrk]/I");
    m_tree->Branch ("trk_truth_orig",     &m_b_trk_truth_orig,      "trk_truth_orig[ntrk]/I");
    m_tree->Branch ("trk_truth_barcode",  &m_b_trk_truth_barcode,   "trk_truth_barcode[ntrk]/I");
    m_tree->Branch ("trk_truth_pdgid",    &m_b_trk_truth_pdgid,     "trk_truth_pdgid[ntrk]/I");
    m_tree->Branch ("trk_truth_vz",       &m_b_trk_truth_vz,        "trk_truth_vz[ntrk]/F");
    m_tree->Branch ("trk_truth_nIn",      &m_b_trk_truth_nIn,       "trk_truth_nIn[ntrk]/I");
    m_tree->Branch ("trk_truth_isHadron", &m_b_trk_truth_isHadron,  "trk_truth_isHadron[ntrk]/O");
  }

  if (m_dataType != Collisions) {
    // Truth track info
    m_tree->Branch ("truth_trk_n",        &m_b_truth_trk_n,         "truth_trk_n/I");
    m_tree->Branch ("truth_trk_pt",       &m_b_truth_trk_pt,        "truth_trk_pt[truth_trk_n]/F");
    m_tree->Branch ("truth_trk_eta",      &m_b_truth_trk_eta,       "truth_trk_eta[truth_trk_n]/F");
    m_tree->Branch ("truth_trk_phi",      &m_b_truth_trk_phi,       "truth_trk_phi[truth_trk_n]/F");
    m_tree->Branch ("truth_trk_charge",   &m_b_truth_trk_charge,    "truth_trk_charge[truth_trk_n]/F");
    m_tree->Branch ("truth_trk_pdgid",    &m_b_truth_trk_pdgid,     "truth_trk_pdgid[truth_trk_n]/I");
    m_tree->Branch ("truth_trk_barcode",  &m_b_truth_trk_barcode,   "truth_trk_barcode[truth_trk_n]/I");
    m_tree->Branch ("truth_trk_isHadron", &m_b_truth_trk_isHadron,  "truth_trk_isHadron[truth_trk_n]/O");
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MinbiasTreeMaker :: fileExecute () {
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MinbiasTreeMaker :: changeInput (bool /*firstFile*/) {
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MinbiasTreeMaker :: initialize () {
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_CHECK_SET_TYPE (EL::StatusCode);

  rndm = new TRandom3 ();
  rndm->SetSeed ();


  //----------------------------------------------------------------------
  // Triggering tools
  //----------------------------------------------------------------------
  if (m_dataType == Collisions) {
    // Initialize trigger configuration tool
    m_trigConfigTool = new TrigConf::xAODConfigTool ("xAODConfigTool"); // gives us access to the meta-data
    ANA_CHECK (m_trigConfigTool->initialize ());
    ToolHandle<TrigConf::ITrigConfigTool> trigConfigHandle (m_trigConfigTool);

    // Initialize trigger decision tool and connect to trigger configuration tool (NOTE: must be after trigger configuration tool is initialized!!!)
    m_trigDecisionTool = new Trig::TrigDecisionTool ("TrigDecisionTool");
    ANA_CHECK (m_trigDecisionTool->setProperty ("ConfigTool", trigConfigHandle)); // connect the TrigDecisionTool to the ConfigTool
    ANA_CHECK (m_trigDecisionTool->setProperty ("TrigDecisionKey", "xTrigDecision"));
    ANA_CHECK (m_trigDecisionTool->initialize ());
  }
  else {
    m_trigConfigTool = nullptr;
    m_trigDecisionTool = nullptr;
  }

 
  //----------------------------------------------------------------------
  // GRL
  //----------------------------------------------------------------------
  if (m_dataType == Collisions || m_dataType == MCDataOverlay) {
    const char* GRLFilePath = "$UserAnalysis_DIR/data/MinbiasTrackAnalysis";
    const char* pp17_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml", GRLFilePath));
    const char* PbPb15_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data15_hi.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_HeavyIonP_All_Good.xml", filePath.c_str ()));
    const char* PbPb18_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good.xml", filePath.c_str ()));
    const char* PbPb18_ignoreToroid_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml", filePath.c_str ()));

    std::vector<std::string> vecStringGRL;

    vecStringGRL.clear ();
    m_pp17_grl = new GoodRunsListSelectionTool("pp17_GoodRunsListSelectionTool");
    vecStringGRL.push_back (pp17_fullGRLFilePath);
    ANA_CHECK (m_pp17_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_pp17_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_pp17_grl->initialize ());

    vecStringGRL.clear ();
    m_PbPb15_grl = new GoodRunsListSelectionTool("PbPb15_GoodRunsListSelectionTool");
    vecStringGRL.push_back (PbPb15_fullGRLFilePath);
    ANA_CHECK (m_PbPb15_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_PbPb15_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_PbPb15_grl->initialize ());

    vecStringGRL.clear ();
    m_PbPb18_grl = new GoodRunsListSelectionTool("PbPb18_GoodRunsListSelectionTool");
    vecStringGRL.push_back (PbPb18_fullGRLFilePath);
    ANA_CHECK (m_PbPb18_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_PbPb18_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_PbPb18_grl->initialize ());

    vecStringGRL.clear ();
    m_PbPb18_ignoreToroid_grl = new GoodRunsListSelectionTool("PbPb18_ignoreToroid_GoodRunsListSelectionTool");
    vecStringGRL.push_back (PbPb18_ignoreToroid_fullGRLFilePath);
    ANA_CHECK (m_PbPb18_ignoreToroid_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_PbPb18_ignoreToroid_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_PbPb18_ignoreToroid_grl->initialize ());
  }
  else {
    m_pp17_grl = nullptr;
    m_PbPb18_grl = nullptr;
    m_PbPb18_ignoreToroid_grl = nullptr;
    m_PbPb15_grl = nullptr;
  }
  

  //----------------------------------------------------------------------
  // Track selection tools
  //----------------------------------------------------------------------
  m_trackSelectionToolHITight = new InDet::InDetTrackSelectionTool ("TrackSelectionToolHITight");
  ANA_CHECK (m_trackSelectionToolHITight->setProperty ("CutLevel", "HITight"));
  ANA_CHECK (m_trackSelectionToolHITight->initialize ());

  m_trackSelectionToolHILoose = new InDet::InDetTrackSelectionTool ("TrackSelectionToolHILoose");
  ANA_CHECK (m_trackSelectionToolHILoose->setProperty ("CutLevel", "HILoose"));
  ANA_CHECK (m_trackSelectionToolHILoose->initialize ());

  m_trackSelectionToolTightPrimary = new InDet::InDetTrackSelectionTool ("TrackSelectionToolTightPrimary");
  ANA_CHECK (m_trackSelectionToolTightPrimary->setProperty ("CutLevel", "TightPrimary"));
  ANA_CHECK (m_trackSelectionToolTightPrimary->initialize ());


  //----------------------------------------------------------------------
  // OOTPU tool
  //----------------------------------------------------------------------
  ANA_MSG_INFO (PathResolverFindCalibFile (Form ("ZTrackAnalysis/%s", m_oop_fname.c_str ())).c_str ());
  TFile* f_oop_In = TFile::Open (PathResolverFindCalibFile (Form ("ZTrackAnalysis/%s", m_oop_fname.c_str ())).c_str (), "READ");
  ANA_MSG_INFO ("Search for pileup file at " << PathResolverFindCalibFile (Form ("ZTrackAnalysis/%s", m_oop_fname.c_str ())).c_str ());
  if (!f_oop_In) {
    ANA_MSG_ERROR ("Could not find input Out-of-time Pileup calibration file " << m_oop_fname << ", exiting");
    return EL::StatusCode::FAILURE;
  }
  ANA_MSG_INFO ("Read Out-of-time pileup cuts from "<< m_oop_fname);
  m_oop_hMean  = (TH1D*)((TH1D*)f_oop_In->Get("hMeanTotal")) ->Clone("hMeanTotal_HIPileTool");  m_oop_hMean ->SetDirectory(0);
  m_oop_hMean->SetDirectory (0);
  m_oop_hSigma = (TH1D*)((TH1D*)f_oop_In->Get("hSigmaTotal"))->Clone("hSigmaTotal_HIPileTool"); m_oop_hSigma->SetDirectory(0);
  m_oop_hSigma->SetDirectory (0);


  //----------------------------------------------------------------------
  // ZDC calibration tool
  //----------------------------------------------------------------------
  if ((m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) && m_dataType == Collisions) {
    m_zdcAnalysisTool = new ZDC::ZdcAnalysisTool ("ZdcAnalysisTool");

    ANA_CHECK (m_zdcAnalysisTool->setProperty ("FlipEMDelay", false)); // false
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("LowGainOnly", false)); // false
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoCalib", true)); // true
    if (m_collisionSystem == PbPb18) {
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("Configuration", "PbPb2018")); // "PbPb2018"
    }
    else if (m_collisionSystem == PbPb15) {
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("Configuration", "PbPb2015")); // "PbPb2015"
    }
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("AuxSuffix", "_RP")); // "RP"
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("ForceCalibRun", -1)); 

    if (m_collisionSystem == PbPb18) {
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoTrigEff", false)); // for now
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoTimeCalib", false)); // for now
    }
    ANA_CHECK (m_zdcAnalysisTool->initialize ());
  }
  else {
    m_zdcAnalysisTool = nullptr;
  }


  //----------------------------------------------------------------------
  // Initialize MC truth tool
  //----------------------------------------------------------------------
  if (m_dataType != Collisions) {
    m_mcTruthClassifier = new MCTruthClassifier ("MCTruthClassifier");
    ANA_CHECK (m_mcTruthClassifier->initialize ());
  }


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MinbiasTreeMaker :: execute () {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // set type of return code you are expecting
  // (add to top of each function once)

  ANA_CHECK_SET_TYPE (EL::StatusCode);



  //----------------------------------------------------------------------
  // Random slimming (don't need EVERY minimum bias event
  //----------------------------------------------------------------------
  //if (m_useTriggers && rndm->Rndm () > 0.2)
  //  return EL::StatusCode::SUCCESS;



  //----------------------------------------------------------------------
  // Triggering
  //----------------------------------------------------------------------
  if (m_dataType == Collisions) {
    m_b_HLT_mb_sptrk = false;
    m_b_HLT_mb_sptrk_L1ZDC_A_C_VTE50 = false;
    m_b_HLT_noalg_pc_L1TE50_VTE600_0ETA49 = false;
    m_b_HLT_noalg_cc_L1TE600_0ETA49 = false;
    m_b_HLT_noalg_mb_L1TE50 = false;
    m_b_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50 = false;

    if (m_collisionSystem == pp17) {
      auto cg = m_trigDecisionTool->getChainGroup ("HLT_mb_sptrk");
      m_b_HLT_mb_sptrk = cg->isPassed ();
      m_b_HLT_mb_sptrk_prescale = cg->getPrescale ();

      if (!m_b_HLT_mb_sptrk)
        return EL::StatusCode::SUCCESS;
    }
    else if (m_collisionSystem == PbPb18) {
      auto cg = m_trigDecisionTool->getChainGroup ("HLT_mb_sptrk_L1ZDC_A_C_VTE50");
      m_b_HLT_mb_sptrk_L1ZDC_A_C_VTE50 = cg->isPassed ();
      m_b_HLT_mb_sptrk_L1ZDC_A_C_VTE50_prescale = cg->getPrescale ();

      cg = m_trigDecisionTool->getChainGroup ("HLT_noalg_pc_L1TE50_VTE600.0ETA49");
      m_b_HLT_noalg_pc_L1TE50_VTE600_0ETA49 = cg->isPassed ();
      m_b_HLT_noalg_pc_L1TE50_VTE600_0ETA49_prescale = cg->getPrescale ();

      cg = m_trigDecisionTool->getChainGroup ("HLT_noalg_cc_L1TE600.0ETA49");
      m_b_HLT_noalg_cc_L1TE600_0ETA49 = cg->isPassed ();
      m_b_HLT_noalg_cc_L1TE600_0ETA49_prescale = cg->getPrescale ();

      if (!m_b_HLT_mb_sptrk_L1ZDC_A_C_VTE50 && !m_b_HLT_noalg_pc_L1TE50_VTE600_0ETA49 && !m_b_HLT_noalg_cc_L1TE600_0ETA49)
        return EL::StatusCode::SUCCESS;
    }
    else if (m_collisionSystem == PbPb15) {
      auto cg = m_trigDecisionTool->getChainGroup ("HLT_noalg_mb_L1TE50");
      m_b_HLT_noalg_mb_L1TE50 = cg->isPassed ();
      m_b_HLT_noalg_mb_L1TE50_prescale = cg->getPrescale ();

      cg = m_trigDecisionTool->getChainGroup ("HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50");
      m_b_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50 = cg->isPassed ();
      m_b_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50_prescale = cg->getPrescale ();

      if (!m_b_HLT_noalg_mb_L1TE50 && !m_b_HLT_mb_sptrk_ion_L1ZDC_A_C_VTE50)
        return EL::StatusCode::SUCCESS;
    }
    else {
      return EL::StatusCode::SUCCESS;
    }
  } // end triggering scope



  //----------------------------------------------------------------------
  // Event information
  //----------------------------------------------------------------------
  const xAOD::EventInfo* eventInfo = 0;
  if (!evtStore ()->retrieve (eventInfo, "EventInfo").isSuccess ()) {
    Error ("GetEventInfo ()", "Failed to retrieve EventInfo container. Exiting.");
    return EL::StatusCode::FAILURE;
  }



  //----------------------------------------------------------------------
  // Event & collision information
  //----------------------------------------------------------------------
  m_b_runNum = eventInfo->runNumber ();
  m_b_lbn = eventInfo->lumiBlock ();
  m_b_evtNum = eventInfo->eventNumber ();
  m_b_actualInteractionsPerCrossing = eventInfo->actualInteractionsPerCrossing ();
  m_b_averageInteractionsPerCrossing = eventInfo->averageInteractionsPerCrossing ();
 
  if (m_dataType == Collisions || m_dataType == MCDataOverlay) {
    m_b_BlayerDesyn = isDesynEvent (eventInfo->runNumber (), eventInfo->lumiBlock ());

    // if data, check if event passes GRL
    if (m_collisionSystem == pp17) {
      if (!m_pp17_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
      m_b_passesToroid = true;
    } 
    else if (m_collisionSystem == PbPb18) {
      if (!m_PbPb18_ignoreToroid_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
      m_b_passesToroid = m_PbPb18_grl->passRunLB (*eventInfo);
    }
    else if (m_collisionSystem == PbPb15) {
      if (!m_PbPb15_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
      m_b_passesToroid = true;
    }
    else {
      Error ("CheckGRL ()", "Undefined collision system. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    // if events passes event cleaning
    if (eventInfo->errorState (xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ||
        eventInfo->errorState (xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ||
        eventInfo->errorState (xAOD::EventInfo::SCT) == xAOD::EventInfo::Error ||
        eventInfo->isEventFlagBitSet (xAOD::EventInfo::Core, 18)) {
      return EL::StatusCode::SUCCESS;
    }
  } // end GRL scope
  else {
    m_b_BlayerDesyn = false;
  }


  //----------------------------------------------------------------------
  // Truth event information
  //----------------------------------------------------------------------
  if (m_dataType != Collisions) {

    m_b_mcEventWeights = eventInfo->mcEventWeights ();

    const xAOD::TruthEventContainer* truthEvents = 0;
    if (!evtStore ()->retrieve (truthEvents, "TruthEvents").isSuccess ()) {
      Error ("GetTruthEvents ()", "Failed to retrieve TruthEvents container. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    m_b_nTruthEvt = 0;
    for (const auto* truthEvent : *truthEvents) {
      if (m_b_nTruthEvt >= 5) {
        Error ("GetTruthEvents ()", "Tried to overflow truth event arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      truthEvent->heavyIonParameter (m_b_nPart1[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::NPARTPROJ);
      truthEvent->heavyIonParameter (m_b_nPart2[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::NPARTTARG);
      truthEvent->heavyIonParameter (m_b_impactParameter[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::IMPACTPARAMETER);
      truthEvent->heavyIonParameter (m_b_nColl[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::NCOLL);
      truthEvent->heavyIonParameter (m_b_nSpectatorNeutrons[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::SPECTATORNEUTRONS);
      truthEvent->heavyIonParameter (m_b_nSpectatorProtons[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::SPECTATORPROTONS);
      truthEvent->heavyIonParameter (m_b_eccentricity[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::ECCENTRICITY);
      truthEvent->heavyIonParameter (m_b_eventPlaneAngle[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::EVENTPLANEANGLE);
      m_b_nTruthEvt++;
    }
  } // end truth events scope


  //----------------------------------------------------------------------
  // Gather verticies information
  //----------------------------------------------------------------------
  const xAOD::Vertex* priVtx = nullptr;
  {
    m_b_nvert = 0;

    const xAOD::VertexContainer* primaryVertices = 0;
    if (!evtStore ()->retrieve (primaryVertices, "PrimaryVertices") .isSuccess ())  {
      Error ("GetPrimaryVertices ()", "Failed to retrieve PrimaryVertices container. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* vert : *primaryVertices) {
      if (m_b_nvert >= 30) {
        Error ("GetPrimaryVertices ()", "Tried to overflow vertex arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      //if (m_b_nvert == 0) {
      //  std::cout << "vertex type is ";
      //  switch (vert->vertexType ()) {
      //    case xAOD::VxType::NoVtx:         std::cout << "NoVtx";        break;
      //    case xAOD::VxType::PriVtx:        std::cout << "PriVtx";       break;
      //    case xAOD::VxType::SecVtx:        std::cout << "SecVtx";       break;
      //    case xAOD::VxType::PileUp:        std::cout << "PileUp";       break;
      //    case xAOD::VxType::ConvVtx:       std::cout << "ConvVtx";      break;
      //    case xAOD::VxType::V0Vtx:         std::cout << "V0Vtx";        break;
      //    case xAOD::VxType::KinkVtx:       std::cout << "KinkVtx";      break;
      //    case xAOD::VxType::NotSpecified:  std::cout << "NotSpecified"; break;
      //  }
      //  std::cout << std::endl;
      //}

      //if (vert->vertexType () == xAOD::VxType::PriVtx)
      if (m_b_nvert == 0 && vert)
        priVtx = vert;
      m_b_vert_x[m_b_nvert] = vert->x ();
      m_b_vert_y[m_b_nvert] = vert->y ();
      m_b_vert_z[m_b_nvert] = vert->z ();
      m_b_vert_ntrk[m_b_nvert] = vert->nTrackParticles ();
      m_b_vert_type[m_b_nvert] = vert->vertexType ();

      float sumpt = 0;
      for (const auto track : vert->trackParticleLinks ()) {
        if (!track.isValid ())
          continue;
        if (!m_trackSelectionToolHILoose->accept (**track, vert))
          continue;

        sumpt += (*track)->pt () * 1e-3;
      } // end loop over tracks
      m_b_vert_sumpt[m_b_nvert] = sumpt;
      m_b_nvert++;
    }
  } // end vertex scope

  if (m_b_nvert == 0 || priVtx == nullptr) {
    Info ("GetPrimaryVertices ()", "Warning: Did not find a primary vertex!");
    //return EL::StatusCode::SUCCESS;
  }


  //----------------------------------------------------------------------
  // Calculate total FCal energies
  //----------------------------------------------------------------------
  //if (m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) {
  {
    if (m_collisionSystem == PbPb18 && m_dataType == Collisions) {
      const xAOD::TrackParticleContainer* trackContainer = 0;
      if (!evtStore ()->retrieve (trackContainer, "InDetTrackParticles").isSuccess ()) {
        Error ("GetInDetTrackParticles()", "Failed to retrieve InDetTrackParticles container. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      const xAOD::HIEventShapeContainer* caloSums = 0;  
      if (!evtStore ()->retrieve (caloSums, "CaloSums").isSuccess ()) {
        Error ("GetCaloSums ()", "Failed to retrieve CaloSums container. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      int nTracks = 0;
      for (const auto* track : *trackContainer) {
        if (track->pt () < 500) // cut at 500 MeV
          continue;
        if (m_trackSelectionToolHITight->accept (*track, priVtx))
          nTracks++;
      } 

      m_b_isOOTPU = is_Outpileup (*caloSums, nTracks);
    }
    else {
      m_b_isOOTPU = false;
    }

    m_b_fcalA_et = 0;
    m_b_fcalC_et = 0;
    m_b_fcalA_et_Cos2 = 0; 
    m_b_fcalA_et_Sin2 = 0; 
    m_b_fcalC_et_Cos2 = 0; 
    m_b_fcalC_et_Sin2 = 0; 
    m_b_fcalA_et_Cos3 = 0; 
    m_b_fcalA_et_Sin3 = 0; 
    m_b_fcalC_et_Cos3 = 0; 
    m_b_fcalC_et_Sin3 = 0; 
    m_b_fcalA_et_Cos4 = 0; 
    m_b_fcalA_et_Sin4 = 0; 
    m_b_fcalC_et_Cos4 = 0; 
    m_b_fcalC_et_Sin4 = 0; 

    const xAOD::HIEventShapeContainer* hiueContainer = 0;  
    if (!evtStore ()->retrieve (hiueContainer, "HIEventShape").isSuccess ()) {
      Error ("GetHIEventShape ()", "Failed to retrieve HIEventShape container. Exiting.");
      return EL::StatusCode::FAILURE; 
    }
    for (const auto* hiue : *hiueContainer) {
      int layer = hiue->layer ();

      if (layer != 21 && layer != 22 && layer != 23)
        continue;

      double et = hiue->et ();
      double eta = hiue->etaMin ();
      const std::vector<float>& c1 = hiue->etCos ();
      const std::vector<float>& s1 = hiue->etSin ();

      if (eta > 0) {
        m_b_fcalA_et += et * 1e-3;
        m_b_fcalA_et_Cos2 += c1.at (1) * 1e-3;
        m_b_fcalA_et_Sin2 += s1.at (1) * 1e-3;
        m_b_fcalA_et_Cos3 += c1.at (2) * 1e-3;
        m_b_fcalA_et_Sin3 += s1.at (2) * 1e-3;
        m_b_fcalA_et_Cos4 += c1.at (3) * 1e-3;
        m_b_fcalA_et_Sin4 += s1.at (3) * 1e-3;
      }
      else {
        m_b_fcalC_et += et * 1e-3;
        m_b_fcalC_et_Cos2 += c1.at (1) * 1e-3;
        m_b_fcalC_et_Sin2 += s1.at (1) * 1e-3;
        m_b_fcalC_et_Cos3 += c1.at (2) * 1e-3;
        m_b_fcalC_et_Sin3 += s1.at (2) * 1e-3;
        m_b_fcalC_et_Cos4 += c1.at (3) * 1e-3;
        m_b_fcalC_et_Sin4 += s1.at (3) * 1e-3;
      }
    }

    if ((m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) && m_dataType == Collisions && m_b_fcalA_et + m_b_fcalC_et < 50) {
      return EL::StatusCode::SUCCESS;

    // Random slimming (don't need EVERY minimum bias event)
    if (m_dataType == Collisions && m_b_fcalA_et + m_b_fcalC_et < 3920.41 && rndm->Rndm () > 0.2)
      return EL::StatusCode::SUCCESS;
    }
  } // end fcal scope


  //----------------------------------------------------------------------
  // Calculate ZDC quantities
  //----------------------------------------------------------------------
  if ((m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) && m_dataType == Collisions) {
    // Zdc block with online/offline info
    // offline calibration from Peter's Zdc-dev-3
    // online L1 ZDC trigger information
    m_b_L1_ZDC_A     = false;
    m_b_L1_ZDC_A_tbp = false;
    m_b_L1_ZDC_A_tap = false;
    m_b_L1_ZDC_A_tav = false;
    m_b_L1_ZDC_A_prescale = 0.;

    m_b_L1_ZDC_C     = false;
    m_b_L1_ZDC_C_tbp = false;
    m_b_L1_ZDC_C_tap = false;
    m_b_L1_ZDC_C_tav = false;
    m_b_L1_ZDC_C_prescale = 0.;

    {
      std::string thisTrig = "L1_ZDC_A";
      const unsigned int bits = m_trigDecisionTool->isPassedBits (thisTrig);
      auto cg = m_trigDecisionTool->getChainGroup (thisTrig);
      m_b_L1_ZDC_A     = cg->isPassed();
      m_b_L1_ZDC_A_tbp = bits&TrigDefs::L1_isPassedBeforePrescale;
      m_b_L1_ZDC_A_tap = bits&TrigDefs::L1_isPassedAfterPrescale;
      m_b_L1_ZDC_A_tav = bits&TrigDefs::L1_isPassedAfterVeto;
      m_b_L1_ZDC_A_prescale =  cg->getPrescale ();
    }

    {
      std::string thisTrig = "L1_ZDC_C";
      const unsigned int bits = m_trigDecisionTool->isPassedBits (thisTrig);
      auto cg = m_trigDecisionTool->getChainGroup (thisTrig);
      m_b_L1_ZDC_C     = cg->isPassed (); 
      m_b_L1_ZDC_C_tbp = bits&TrigDefs::L1_isPassedBeforePrescale;
      m_b_L1_ZDC_C_tap = bits&TrigDefs::L1_isPassedAfterPrescale;
      m_b_L1_ZDC_C_tav = bits&TrigDefs::L1_isPassedAfterVeto;
      m_b_L1_ZDC_C_prescale =  cg->getPrescale ();
    } 

    {
      // offline calibrated ZDC energy
      m_b_ZdcCalibEnergy_A = 0;
      m_b_ZdcCalibEnergy_C = 0;

      // run ZDC Calibration
      ANA_CHECK (m_zdcAnalysisTool->reprocessZdc ());

      const xAOD::ZdcModuleContainer* m_zdcSums = 0;
      if (!evtStore ()->retrieve (m_zdcSums, "ZdcSums_RP").isSuccess ()) {
        Error ("GetZdcSums ()", "Failed to retrieve ZdcSums container. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      for (const auto zdcSum : *m_zdcSums) {
        float energy = zdcSum->auxdecor <float> ("CalibEnergy") ;
        //float energy = zdcSum->energy () ;
        int side = zdcSum->side ();
        int type = zdcSum->type ();
        if (type != 0) continue;
        if (side == +1) m_b_ZdcCalibEnergy_A += energy * 1e-3;
        if (side == -1) m_b_ZdcCalibEnergy_C += energy * 1e-3;
        //std::cout << " ZDC SIDE / TYPE / MOD = " << side << " / " << type << " / " << zdcSum->zdcModule() << ", amp = " << zdcSum->amplitude() << ", energy = " << energy << std::endl;
      }
    }
  }



  //----------------------------------------------------------------------
  // Get tracking info
  //----------------------------------------------------------------------
  std::vector<float> vec_trk_eta;
  vec_trk_eta.clear ();
  {
    m_b_ntrk = 0;
    const xAOD::TrackParticleContainer* trackContainer = 0;
    if (!evtStore ()->retrieve (trackContainer, "InDetTrackParticles").isSuccess ()) {
      Error ("GetInDetTrackParticles ()", "Failed to retrieve InDetTrackParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    std::pair<unsigned int, unsigned int> res;
    
    for (const auto* track : *trackContainer) {
      if (m_b_ntrk >= 10000) {
        Error ("GetInDetTrackParticles ()", "Tried to overflow track arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      const bool passHITight = m_trackSelectionToolHITight->accept (*track, priVtx);
      const bool passHILoose = m_trackSelectionToolHILoose->accept (*track, priVtx);
      const bool passTightPrimary = m_trackSelectionToolTightPrimary->accept (*track, priVtx);

      if (track->pt () * 1e-3 > 0.4 && passHILoose)
        vec_trk_eta.push_back (track->eta ());

      if (track->pt () * 1e-3 < m_trkPtCut)
        continue;

      //if (!passHITight && !passHILoose)
      //  continue;

      m_b_trk_pt[m_b_ntrk] = track->pt () * 1e-3;
      m_b_trk_eta[m_b_ntrk] = track->eta ();
      m_b_trk_phi[m_b_ntrk] = track->phi ();
      m_b_trk_charge[m_b_ntrk] = track->charge ();

      m_b_trk_HItight[m_b_ntrk] = passHITight;
      m_b_trk_HIloose[m_b_ntrk] = passHILoose;
      m_b_trk_TightPrimary[m_b_ntrk] = passTightPrimary;

      m_b_trk_d0[m_b_ntrk] = track->d0 ();
      m_b_trk_d0sig[m_b_ntrk] = xAOD::TrackingHelpers::d0significance (track, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
      m_b_trk_z0[m_b_ntrk] = track->z0 ();
      m_b_trk_z0sig[m_b_ntrk] = xAOD::TrackingHelpers::z0significance (track);
      m_b_trk_theta[m_b_ntrk] = track->theta ();
      m_b_trk_vz[m_b_ntrk] = track->vz ();

      m_b_trk_nBLayerHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfBLayerHits);
      m_b_trk_nBLayerSharedHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfBLayerSharedHits);
      m_b_trk_nPixelHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfPixelHits);
      m_b_trk_nPixelDeadSensors[m_b_ntrk] = getSum (*track, xAOD :: numberOfPixelDeadSensors);
      m_b_trk_nPixelSharedHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfPixelSharedHits);
      m_b_trk_nSCTHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfSCTHits);
      m_b_trk_nSCTDeadSensors[m_b_ntrk] = getSum (*track, xAOD :: numberOfSCTDeadSensors);
      m_b_trk_nSCTSharedHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfSCTSharedHits);
      m_b_trk_nTRTHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfTRTHits);
      m_b_trk_nTRTSharedHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfTRTSharedHits);

      if (m_dataType != Collisions) {
        res = m_mcTruthClassifier->particleTruthClassifier (track);
        //m_b_trk_prob_truth[m_b_ntrk] = m_mcTruthClassifier->getProbTrktoTruth ());

        if (track->isAvailable <ElementLink <xAOD::TruthParticleContainer>> ("truthParticleLink")) {
          ElementLink <xAOD::TruthParticleContainer> link = track->auxdata <ElementLink<xAOD::TruthParticleContainer>> ("truthParticleLink");

          if (link.isValid ()) {
            m_b_trk_prob_truth[m_b_ntrk] = m_mcTruthClassifier->getProbTrktoTruth ();

            const xAOD::TruthParticle* thePart = xAOD::TruthHelpers::getTruthParticle (*(track));

            if (thePart) {
              m_b_trk_truth_pt[m_b_ntrk] = thePart->pt () * 1e-3;
              m_b_trk_truth_eta[m_b_ntrk] = thePart->eta ();
              m_b_trk_truth_phi[m_b_ntrk] = thePart->phi ();
              m_b_trk_truth_charge[m_b_ntrk] = thePart->charge ();

              res = m_mcTruthClassifier->particleTruthClassifier (track);
              m_b_trk_truth_type[m_b_ntrk] = res.first;
              m_b_trk_truth_orig[m_b_ntrk] = res.second;
              m_b_trk_truth_pdgid[m_b_ntrk] = thePart->pdgId ();
              m_b_trk_truth_barcode[m_b_ntrk] = thePart->barcode ();

              if (thePart->hasProdVtx ()) {
                const xAOD::TruthVertex* vtx = thePart->prodVtx ();
                m_b_trk_truth_vz[m_b_ntrk] = vtx->z ();
                m_b_trk_truth_nIn[m_b_ntrk] = vtx->nIncomingParticles ();
              }
              else {
                m_b_trk_truth_vz[m_b_ntrk] = -999;
                m_b_trk_truth_nIn[m_b_ntrk] = -999;
              }
              m_b_trk_truth_isHadron[m_b_ntrk] = thePart->isHadron ();

            }

            else {
              m_b_trk_truth_pt[m_b_ntrk] = -999;
              m_b_trk_truth_eta[m_b_ntrk] = -999;
              m_b_trk_truth_phi[m_b_ntrk] = -999;
              m_b_trk_truth_charge[m_b_ntrk] = -999;

              m_b_trk_truth_type[m_b_ntrk] = -999;
              m_b_trk_truth_orig[m_b_ntrk] = -999;
              m_b_trk_truth_pdgid[m_b_ntrk] = -999;
              m_b_trk_truth_barcode[m_b_ntrk] = -999;

              m_b_trk_truth_vz[m_b_ntrk] = -999;
              m_b_trk_truth_nIn[m_b_ntrk] = -999;
              m_b_trk_truth_isHadron[m_b_ntrk] = false;
            }
          }

          else {
            m_b_trk_prob_truth[m_b_ntrk] = 0;

            m_b_trk_truth_pt[m_b_ntrk] = -999;
            m_b_trk_truth_eta[m_b_ntrk] = -999;
            m_b_trk_truth_phi[m_b_ntrk] = -999;
            m_b_trk_truth_charge[m_b_ntrk] = -999;

            m_b_trk_truth_type[m_b_ntrk] = -999;
            m_b_trk_truth_orig[m_b_ntrk] = -999;
            m_b_trk_truth_pdgid[m_b_ntrk] = -999;
            m_b_trk_truth_barcode[m_b_ntrk] = -999;

            m_b_trk_truth_vz[m_b_ntrk] = -999;
            m_b_trk_truth_nIn[m_b_ntrk] = -999;
            m_b_trk_truth_isHadron[m_b_ntrk] = false;
          }
          
        } // end if TruthParticleLink is available

      } // end if not collisions

      m_b_ntrk++;

    } // end tracks loop

  } // end tracks scope



  //----------------------------------------------------------------------
  // Gets gap information for PbPb collisions (for UPC bkg. est.)
  //----------------------------------------------------------------------
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18) {
    // Gap information
    const xAOD::CaloClusterContainer* caloClusters = 0;
    ANA_CHECK (evtStore ()->retrieve (caloClusters, "CaloCalTopoClusters"));
    std::vector<float> vec_cl_eta;
    vec_cl_eta.clear ();

    for (const auto cluster : *caloClusters) {
      if (cluster->pt () < 200.0) continue;
      //if (!passSinfigCut (cluster->eta(), cluster->auxdata< float >("CELL_SIG_SAMPLING"), cluster->auxdata<float>("CELL_SIGNIFICANCE"))) continue;
      vec_cl_eta.push_back (cluster->eta());
    }

    m_b_sum_gap_A  = -1;
    m_b_sum_gap_C  = -1;
    m_b_edge_gap_A = -1;
    m_b_edge_gap_C = -1;
    GapCut (vec_trk_eta, vec_cl_eta);

    m_b_clusterOnly_sum_gap_A  = -1;
    m_b_clusterOnly_sum_gap_C  = -1;
    m_b_clusterOnly_edge_gap_A = -1;
    m_b_clusterOnly_edge_gap_C = -1;
    ClusterGapCut (vec_cl_eta);

    vec_cl_eta.clear ();
    vec_trk_eta.clear ();
  }



  //----------------------------------------------------------------------
  // Get truth particles if MC
  //----------------------------------------------------------------------
  if (m_dataType != Collisions) {
    m_b_truth_trk_n = 0;

    const xAOD::TruthParticleContainer* truthParticleContainer = 0;
    if (!evtStore ()->retrieve (truthParticleContainer, "TruthParticles").isSuccess ()) {
      Error ("GetTruthParticles()", "Failed to retrieve TruthParticles collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* truthParticle : *truthParticleContainer) {
      if (m_b_truth_trk_n >= 10000) {
        Error ("GetTruthTracks ()", "Tried to overflow truth track arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
    
      if (truthParticle->status() != 1)
        continue; // if not final state continue
      if (truthParticle->pt() * 1e-3 < m_truthTrkPtCut)
        continue; // pT cut
      if (!truthParticle->isCharged ())
        continue; // require charged particles
      if (fabs (truthParticle->eta()) > 3)
        continue; // require particles inside tracker
      if (truthParticle->absPdgId () == 12 || truthParticle->absPdgId () == 14 || truthParticle->absPdgId () == 16)
        continue; // don't count neutrinos
      if (truthParticle->barcode() >= 2e5 || truthParticle->barcode() == 0)
        continue;

      m_b_truth_trk_pt[m_b_truth_trk_n] = truthParticle->pt () * 1e-3;
      m_b_truth_trk_eta[m_b_truth_trk_n] = truthParticle->eta ();
      m_b_truth_trk_phi[m_b_truth_trk_n] = truthParticle->phi ();
      m_b_truth_trk_charge[m_b_truth_trk_n] = truthParticle->charge ();
      m_b_truth_trk_pdgid[m_b_truth_trk_n] = truthParticle->pdgId ();
      m_b_truth_trk_barcode[m_b_truth_trk_n] = truthParticle->barcode ();
      m_b_truth_trk_isHadron[m_b_truth_trk_n] = truthParticle->isHadron ();
      m_b_truth_trk_n++;
    } // end truth particles loop

  } // end truth info scope


  m_tree->Fill();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MinbiasTreeMaker :: postExecute () {
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MinbiasTreeMaker :: finalize () {
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  delete rndm;
  rndm = nullptr;

  delete m_oop_hMean;
  m_oop_hMean = nullptr;

  delete m_oop_hSigma;
  m_oop_hSigma = nullptr;

  if (m_dataType == Collisions) {
    if (m_trigConfigTool) {
      delete m_trigConfigTool;
      m_trigConfigTool = nullptr;
    }
    if (m_trigDecisionTool) {
      delete m_trigDecisionTool;
      m_trigDecisionTool = nullptr;
    }
  }


  if ((m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) && m_dataType == Collisions) {
    delete m_zdcAnalysisTool;
    m_zdcAnalysisTool = nullptr;
  }

  if (m_dataType == Collisions || m_dataType == MCDataOverlay) {
    if (m_pp17_grl) {
      delete m_pp17_grl;
      m_pp17_grl = nullptr;
    }
    if (m_PbPb15_grl) {
      delete m_PbPb15_grl;
      m_PbPb15_grl = nullptr;
    }
    if (m_PbPb18_grl) {
      delete m_PbPb18_grl;
      m_PbPb18_grl = nullptr;
    }
    if (m_PbPb18_ignoreToroid_grl) {
      delete m_PbPb18_ignoreToroid_grl;
      m_PbPb18_ignoreToroid_grl = nullptr;
    }
  }

  if (m_trackSelectionToolHITight) {
    delete m_trackSelectionToolHITight;
    m_trackSelectionToolHITight = nullptr;
  }
  if (m_trackSelectionToolHILoose) {
    delete m_trackSelectionToolHILoose;
    m_trackSelectionToolHILoose = nullptr;
  }
  if (m_trackSelectionToolTightPrimary) {
    delete m_trackSelectionToolTightPrimary;
    m_trackSelectionToolTightPrimary = nullptr;
  }

  if (m_dataType != Collisions) {
    delete m_mcTruthClassifier;
    m_mcTruthClassifier = nullptr;
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MinbiasTreeMaker :: histFinalize () {
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}



/////////////////////////////////////////////////////////////////////
// 2018 tracker misalignment
/////////////////////////////////////////////////////////////////////
bool MinbiasTreeMaker :: isDesynEvent (const int _runNumber, const int _lumiBlock) {
  switch (_runNumber) {
    case 366627: { if (_lumiBlock >= 139  && _lumiBlock <= 250) return true; return false; }
    case 366691: { if (_lumiBlock >= 236  && _lumiBlock <= 360) return true; return false; }
    case 366754: { if (_lumiBlock >= 208  && _lumiBlock <= 340) return true; return false; }
    case 366805: { if (_lumiBlock >= 81   && _lumiBlock <= 200) return true; return false; }
    case 366860: { if (_lumiBlock >= 147  && _lumiBlock <= 210) return true; return false; }
    case 366878: { if (_lumiBlock >= 87   && _lumiBlock <= 160) return true; return false; }
    case 366919: { if (_lumiBlock >= 110  && _lumiBlock <= 200) return true; return false; }
    case 366931: { if (_lumiBlock >= 139  && _lumiBlock <= 200) return true; return false; }
    case 367023: { if (_lumiBlock >= 140  && _lumiBlock <= 220) return true; return false; }
    case 367099: { if (_lumiBlock >= 200  && _lumiBlock <= 260) return true; return false; }
    case 367134: { if (_lumiBlock >= 140  && _lumiBlock <= 170) return true; return false; }
    case 367233: { if (_lumiBlock >= 180  && _lumiBlock <= 250) return true; return false; }
    default: return false;
  }
}



/////////////////////////////////////////////////////////////////////
// 2018 out-of-time pile-up removal
/////////////////////////////////////////////////////////////////////
bool MinbiasTreeMaker :: is_Outpileup(const xAOD::HIEventShapeContainer& evShCont, const int nTrack) {

  if (nTrack > 3000) // The selection is only for [0, 3000]
    return 0;
  
  float Fcal_Et = 0.0;
  float Tot_Et = 0.0;
  float oop_Et = 0.0;
  Fcal_Et = evShCont.at(5)->et()*1e-6;
  Tot_Et = evShCont.at(0)->et()*1e-6;
  oop_Et = Tot_Et - Fcal_Et;// Barrel + Endcap calo
  
  int nBin{m_oop_hMean->GetXaxis()->FindFixBin(nTrack)};
  double mean{m_oop_hMean->GetBinContent(nBin)};
  double sigma{m_oop_hSigma->GetBinContent(nBin)};

  ANA_MSG_DEBUG (" oop_Et = " << oop_Et << "TeV,  ntrk = " << nTrack );
  
  if (m_nside == 1) // one side cut
    if (oop_Et - mean > -4 * sigma) // 4 sigma cut
      return 0;

  if (m_nside == 2) // two side cut
    if (abs(oop_Et - mean) < 4 * sigma) // 4 sigma cut
      return 0;

  return 1;
}



/////////////////////////////////////////////////////////////////////
// Helper function for gap cuts
/////////////////////////////////////////////////////////////////////
bool MinbiasTreeMaker :: passSinfigCut (float eta, int cellsigsamp, float cellsig) {
  bool Use_cluster = false;
  float sig_cut = CellSigCut (eta);
  //Check if cell sig is above threshold
  if (cellsig > sig_cut) Use_cluster = 1;
  (void) cellsigsamp;
  //Tile cut off!!!!!!!!!
  //Check if significant cell is in tile calorimeter
  //if (cellsigsamp < 21 && cellsigsamp > 11)
  //Use_cluster = 0;
  if (fabs (eta) > 4.9) Use_cluster = 0;
  return Use_cluster;
}



/////////////////////////////////////////////////////////////////////
// Helper function for gap cuts
/////////////////////////////////////////////////////////////////////
float MinbiasTreeMaker :: CellSigCut (float x) {
  float eta[101] = {-5, -4.9, -4.8, -4.7, -4.6, -4.5, -4.4, -4.3, -4.2, -4.1, -4, -3.9, -3.8, -3.7, -3.6, -3.5, -3.4, -3.3, -3.2, -3.1, -3, -2.9, -2.8, -2.7, -2.6, -2.5, -2.4, -2.3, -2.2, -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.5, -1.4, -1.3, -1.2, -1.1, -1, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9};
  float sig[101] = {0, 4.7426, 5.11018, 5.07498, 5.0969, 5.10695, 5.04098, 5.07106, 4.98087, 5.11647, 5.08988, 5.16267, 5.17202, 5.23803, 5.25314, 5.29551, 5.35092, 5.40863, 5.44375, 5.38075, 5.25022, 5.37933, 5.25459, 5.37719, 5.25169, 5.73985, 5.79174, 5.79266, 5.79588, 5.7963, 5.81949, 5.82273, 5.85658, 5.85442, 5.84779, 5.77679, 5.83323, 5.84524, 5.84439, 5.84488, 5.84744, 5.84683, 5.84524, 5.84594, 5.84656, 5.84639, 5.84461, 5.84515, 5.84206, 5.8396, 5.84497, 5.84801, 5.84608, 5.84608, 5.84783, 5.84726, 5.84844, 5.8477, 5.84796, 5.84757, 5.84822, 5.84814, 5.84617, 5.83451, 5.77658, 5.84309, 5.85496, 5.85761, 5.82555, 5.82206, 5.78982, 5.78482, 5.7778, 5.78327, 5.74898, 5.25459, 5.37503, 5.25459, 5.37283, 5.25169, 5.37862, 5.44473, 5.41041, 5.34498, 5.29551, 5.25602, 5.2283, 5.17428, 5.14504, 5.09342, 5.12256, 4.98721, 5.07106, 5.02642, 5.10031, 5.11018, 5.05447, 5.10031, 4.7426, 0};
  float sig_cut = 0;
  for (int i = 0; i < 100; i++) {
    if (x < eta[i]) {
      sig_cut = sig[i];
      break;
    }
  }
  return sig_cut;
}



/////////////////////////////////////////////////////////////////////
// Calculate gaps using only clusters
/////////////////////////////////////////////////////////////////////
void MinbiasTreeMaker :: ClusterGapCut (std::vector <float>& clus) {
  //Define a c++ Set for organiZed list eta vals fro gap calc
  //C++ Set automatically organizes inserted vals
  //Intialized with with tracks and then clusters are added.
  std::set<float> eta_vals (clus.begin (), clus.end ());

  //add goast particles to define edge of detector
  eta_vals.insert (4.9);
  eta_vals.insert (-4.9);
  //----------------------------------------------------
  //Edge gap calculations
  // A side eta > 0 
  // C side eta < 0 
  float edge_gap_A = 0;
  float edge_gap_C = 0;

  //Edge gap calc (gap between detector edge and closest particle)
  std::set<float>::iterator neg_itr1 = ++eta_vals.begin ();
  std::set<float>::iterator neg_itr2 = eta_vals.begin ();
  edge_gap_C = *neg_itr1 - *neg_itr2;

  std::set<float>::iterator pos_itr1 = ----eta_vals.end ();
  std::set<float>::iterator pos_itr2 = --eta_vals.end ();
  edge_gap_A = *pos_itr2 - *pos_itr1;

  /*  
  int MedianEta = round(eta_vals.size()/2);
  for (set<float>::iterator itr1 = ++eta_vals.begin(), itr2 = eta_vals.begin(); itr1 != next(eta_vals.begin(),MedianEta); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_a+=delta_eta;
  }

  for ( set<float>::iterator itr1= next(eta_vals.begin(),MedianEta), itr2 = next(eta_vals.begin(),MedianEta-1); itr1 != eta_vals.end(); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_p+=delta_eta;
  }
  */

  //insert goast particle at eta=0.
  //This breaks up the gaps makeing the max gap=4.9
  //This also splits the detector into two separate sides
  //the photon side and the nucleus side
  //this causes a pile up effect in gap quantities at 4.9
  //because this is a synthetic particle.
  eta_vals.insert (0);

  //Sum of gaps to zero rapidity for both sides using gap cut
  float sum_gap_A = 0;
  float sum_gap_C = 0;
  float eta_cut = 0.5;

  for (std::set<float>::iterator itr1 = ++eta_vals.begin (), itr2 = eta_vals.begin (); itr1!=eta_vals.end (); itr1++, itr2++) {
    float delta_eta = *itr1 - *itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (*itr2 < 0) {
      if (delta_eta > eta_cut) sum_gap_C += delta_eta;
    }
    //define the photon side of the detector
    if (*itr1 > 0) {
      if (delta_eta > eta_cut) sum_gap_A += delta_eta;
    }
  }

  //Store gap info
  m_b_clusterOnly_sum_gap_A  = sum_gap_A;
  m_b_clusterOnly_sum_gap_C  = sum_gap_C;
  m_b_clusterOnly_edge_gap_A = edge_gap_A;
  m_b_clusterOnly_edge_gap_C = edge_gap_C;

  eta_vals.erase (0.0);//erase goast particle
}



/////////////////////////////////////////////////////////////////////
// Calculate gaps using only clusters
/////////////////////////////////////////////////////////////////////
void MinbiasTreeMaker :: GapCut (std::vector<float>& trks, std::vector<float>& clus) {
  //Define a c++ Set for organiZed list eta vals fro gap calc
  //C++ Set automatically organizes inserted vals
  //Intialized with with tracks and then clusters are added.
  std::set<float> eta_vals (clus.begin (), clus.end ());

  //add goast particles to define edge of detector
  eta_vals.insert (4.9);
  eta_vals.insert (-4.9);

  std::vector<float>::iterator itrE = trks.begin ();
  for(; itrE!=trks.end (); itrE++) {
    eta_vals.insert (*itrE);
  }
  //----------------------------------------------------
  //Edge gap calculations
  // A side eta > 0 
  // C side eta < 0 
  float edge_gap_A = 0;
  float edge_gap_C = 0;

  //Edge gap calc (gap between detector edge and closest particle)
  std::set<float>::iterator neg_itr1 = ++eta_vals.begin ();
  std::set<float>::iterator neg_itr2 = eta_vals.begin ();
  edge_gap_C = *neg_itr1 - *neg_itr2;

  std::set<float>::iterator pos_itr1 = ----eta_vals.end ();
  std::set<float>::iterator pos_itr2 = --eta_vals.end ();
  edge_gap_A = *pos_itr2 - *pos_itr1;
  /*  
  int MedianEta = round(eta_vals.size()/2);
  for (set<float>::iterator itr1 = ++eta_vals.begin(), itr2 = eta_vals.begin(); itr1 != next(eta_vals.begin(),MedianEta); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_a+=delta_eta;
  }

  for ( set<float>::iterator itr1= next(eta_vals.begin(),MedianEta), itr2 = next(eta_vals.begin(),MedianEta-1); itr1 != eta_vals.end(); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_p+=delta_eta;
  }
  */

  //insert goast particle at eta=0.
  //This breaks up the gaps makeing the max gap=4.9
  //This also splits the detector into two separate sides
  //the photon side and the nucleus side
  //this causes a pile up effect in gap quantities at 4.9
  //because this is a synthetic particle.
  eta_vals.insert (0);

  //Sum of gaps to zero rapidity for both sides using gap cut
  float sum_gap_A = 0;
  float sum_gap_C = 0;
  float eta_cut = 0.5;

  for (std::set<float>::iterator itr1=++eta_vals.begin (), itr2=eta_vals.begin (); itr1!=eta_vals.end (); itr1++, itr2++) {
    float delta_eta = *itr1 - *itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (*itr2 < 0) {
      if (delta_eta > eta_cut) sum_gap_C += delta_eta;
    }
    //define the photon side of the detector
    if (*itr1 > 0) {
      if (delta_eta > eta_cut) sum_gap_A += delta_eta;
    }
  }

  //Store gap info
  m_b_sum_gap_A  = sum_gap_A;
  m_b_sum_gap_C  = sum_gap_C;
  m_b_edge_gap_A = edge_gap_A;
  m_b_edge_gap_C = edge_gap_C;

  eta_vals.erase (0.0);//erase goast particle
}



/////////////////////////////////////////////////////////////////////
// Returns a summary value for this track
/////////////////////////////////////////////////////////////////////
uint8_t MinbiasTreeMaker :: getSum (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType) {
  uint8_t sumVal=0;
  if (!trk.summaryValue(sumVal, sumType)) {
    Error ("getSum ()", "Could not get summary type %i", sumType);
  }
  return sumVal;
}
