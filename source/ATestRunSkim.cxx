#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <MinBiasSkimmer/MinBiasSkimmer.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>
#include <EventLoop/LSFDriver.h>
#include <AsgTools/MsgLevel.h>


void ATestRunSkim ()
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/data18_hi/");
  //SH::ScanDir ().filePattern ("*physics_CC*AOD*.1").scan (sh, inputFilePath);

  const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/downloads/mc16_5TeV/");
  SH::ScanDir ().filePattern ("AOD*.1").scan (sh, inputFilePath);

  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/mc16_5TeV/");
  //SH::ScanDir ().filePattern ("AOD.16856839._000152.pool.root.1").scan (sh, inputFilePath); // hijing test file
  //SH::ScanDir ().filePattern ("AOD.17835548._001782.pool.root.1").scan (sh, inputFilePath); // hijing test file
  

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  //job.options()->setDouble (EL::Job::optMaxEvents, 10); // for testing purposes, limit to run over the first 500 events only! // for tutorial
  
  std::string output = "myOutput";
  EL::OutputStream outputFile  (output);
  job.outputAdd (outputFile);

  // add our algorithm to the job
  MinBiasSkimmer *alg = new MinBiasSkimmer;
  alg->m_collisionSystem = PbPb18;
  //alg->m_collisionSystem = PbPb15;
  alg->m_outputName = output;
  alg->m_isCollisions = false;
  alg->m_useTriggers = false;
  alg->m_isOverlayMC = false;
  

  // set the name of the algorithm (this is the name use with messages)
  alg->SetName ("AnalysisAlg");
  //alg->setMsgLevel (MSG::DEBUG);

  job.algsAdd (alg);

  // Standard driver
  EL::DirectDriver driver;

  //// Condor driver
  //EL::CondorDriver driver;
  //job.options()->setDouble (EL::Job::optFilesPerWorker, 5);
  //driver.options()->setString (EL::Job::optSubmitFlags, "-terse");
  //driver.shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";

  /***** Job submission *****/
 
  job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1); 
  // process the job using the driver
  driver.submit (job, "submitDir"); // for direct driver
  //driver.submitOnly (job, "submitDir");   // for running on condor

  /***** End job submission *****/

}
