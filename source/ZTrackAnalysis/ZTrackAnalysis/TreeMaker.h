#ifndef ZTrackAnalysis_TreeMaker_H
#define ZTrackAnalysis_TreeMaker_H

#include <TTree.h>
#include <TH1.h>

#include <EventLoop/Algorithm.h>
#include <AsgTools/AnaToolHandle.h>

// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

// Trigger
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "TriggerMatchingTool/MatchingTool.h"

// HI event includes
#include "xAODHIEvent/HIEventShapeContainer.h"

// ZDC
#include "ZdcAnalysis/ZdcAnalysisTool.h"

// Track selection tool
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// Egamma tools
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"

// Muon tools
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"

// Jet calibration tool
#include "JetCalibTools/JetCalibrationTool.h"

// MC truth classification
#include "MCTruthClassifier/MCTruthClassifier.h"

// in the header
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"

#include <vector>

enum CollisionSystem { pp15 = 0, PbPb15 = 1, pPb16 = 2, Pbp16 = 3, pp17 = 4, PbPb18 = 5 }; // pPb = period A, Pbp = period B
enum DataType { Collisions = 0, MCSignal = 1, MCDataOverlay = 2, MCHijing = 3, MCHijingOverlay = 4 };

class TreeMaker : public EL::Algorithm
{
private:
  const float m_electronPtCut = 10;
  const float m_muonPtCut = 10;
  const float m_trkPtCut = 0.9;
  const float m_truthTrkPtCut = 0.9;
  const float m_jetPtCut = 40;

  const int m_nside = 1;
  const std::string m_oop_fname = "all_fit.root";
  const std::string filePath = "$UserAnalysis_DIR/data/ZTrackAnalysis";

  std::vector<CP::SystematicSet> muonSysList; // list of systematics associated with muons
  std::vector<CP::SystematicSet> electronSysList; // list of systematics associated with electrons

  TH1D* m_oop_hMean = nullptr;
  TH1D* m_oop_hSigma = nullptr;

public:
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  // configuration variables
  CollisionSystem m_collisionSystem;
  DataType m_dataType;
  std::string m_outputName;

  TTree* m_tree; //!

  // event info 
  bool m_b_passesToroid; //! 
  unsigned int m_b_runNum; //!
  unsigned int m_b_lbn; //!
  unsigned int m_b_evtNum; //!
  bool m_b_isOOTPU; //!
  bool m_b_BlayerDesyn; //!

  // collision info
  float m_b_actualInteractionsPerCrossing; //!
  float m_b_averageInteractionsPerCrossing; //!

  std::vector<float> m_b_mcEventWeights; //!

  // truth event info
  int m_b_nTruthEvt; //!
  int m_b_nPart1[5]; //!
  int m_b_nPart2[5]; //!
  float m_b_impactParameter[5]; //!
  int m_b_nColl[5]; //!
  float m_b_sigmaInelasticNN[5]; //!
  int m_b_nSpectatorNeutrons[5]; //!
  int m_b_nSpectatorProtons[5]; //!
  float m_b_eccentricity[5]; //!
  float m_b_eventPlaneAngle[5]; //!

  // vertex info
  int m_b_nvert; //!
  float m_b_vert_x[30]; //!
  float m_b_vert_y[30]; //!
  float m_b_vert_z[30]; //!
  int m_b_vert_ntrk[30]; //!
  int m_b_vert_type[30]; //!
  float m_b_vert_sumpt[30]; //!

  // FCal Et info
  float m_b_fcalA_et; //!
  float m_b_fcalC_et; //!
  float m_b_fcalA_et_Cos2; //!
  float m_b_fcalC_et_Cos2; //!
  float m_b_fcalA_et_Sin2; //!
  float m_b_fcalC_et_Sin2; //!
  float m_b_fcalA_et_Cos3; //!
  float m_b_fcalC_et_Cos3; //!
  float m_b_fcalA_et_Sin3; //!
  float m_b_fcalC_et_Sin3; //!
  float m_b_fcalA_et_Cos4; //!
  float m_b_fcalC_et_Cos4; //!
  float m_b_fcalA_et_Sin4; //!
  float m_b_fcalC_et_Sin4; //!


  // ZDC
  float  m_b_ZdcCalibEnergy_A; //!
  float  m_b_ZdcCalibEnergy_C; //!
  bool   m_b_L1_ZDC_A; //!
  bool   m_b_L1_ZDC_A_tbp; //!
  bool   m_b_L1_ZDC_A_tap; //!
  bool   m_b_L1_ZDC_A_tav; //!
  float  m_b_L1_ZDC_A_prescale; //!
  bool   m_b_L1_ZDC_C; //!
  bool   m_b_L1_ZDC_C_tbp; //!
  bool   m_b_L1_ZDC_C_tap; //!
  bool   m_b_L1_ZDC_C_tav; //!
  float  m_b_L1_ZDC_C_prescale; //!

  // sum of gaps for UPC background
  float m_b_clusterOnly_sum_gap_A; //!
  float m_b_clusterOnly_sum_gap_C; //!
  float m_b_clusterOnly_edge_gap_A; //!
  float m_b_clusterOnly_edge_gap_C; //!
  float m_b_sum_gap_A; //!
  float m_b_sum_gap_C; //!
  float m_b_edge_gap_A; //!
  float m_b_edge_gap_C; //!

  // electron info 
  int m_b_electron_n; //!
  float m_b_electron_pt_precalib[40]; //!
  float m_b_electron_pt[40]; //!
  float m_b_electron_eta[40]; //!
  float m_b_electron_phi[40]; //!
  int m_b_electron_charge[40]; //!
  bool m_b_electron_lhtight[40]; //!
  bool m_b_electron_lhmedium[40]; //!
  bool m_b_electron_lhloose[40]; //!
  bool m_b_electron_lhmedium_hi[40]; //!
  bool m_b_electron_lhloose_hi[40]; //!
  bool m_b_electron_matched[40]; //!
  float m_b_electron_etcone20[40]; //!
  float m_b_electron_etcone30[40]; //!
  float m_b_electron_etcone40[40]; //!
  float m_b_electron_topoetcone20[40]; //!
  float m_b_electron_topoetcone30[40]; //!
  float m_b_electron_topoetcone40[40]; //!
  int m_b_electron_ntrk[40]; //!
  float m_b_electron_id_track_pt[40]; //!
  float m_b_electron_id_track_eta[40]; //!
  float m_b_electron_id_track_phi[40]; //!
  float m_b_electron_id_track_charge[40]; //!
  float m_b_electron_id_track_d0[40]; //!
  float m_b_electron_id_track_d0sig[40]; //!
  float m_b_electron_id_track_z0[40]; //!
  float m_b_electron_id_track_z0sig[40]; //!
  float m_b_electron_id_track_theta[40]; //!
  float m_b_electron_id_track_vz[40]; //!
  bool m_b_electron_id_track_tightprimary[40]; //!
  bool m_b_electron_id_track_hiloose[40]; //!
  bool m_b_electron_id_track_hitight[40]; //!
  float m_b_electron_pt_sys[40]; //!
  float m_b_electron_eta_sys[40]; //!
  float m_b_electron_phi_sys[40]; //!

  // egamma calo cluster info, see https://ucatlas.github.io/RootCoreDocumentation/2.4.28/dc/d4b/CaloCluster__v1_8h_source.html
  int m_b_Cluster_n; //!
  std::vector<float> m_b_Cluster_pt; //!
  std::vector<float> m_b_Cluster_et; //!
  std::vector<float> m_b_Cluster_eta; //!
  std::vector<float> m_b_Cluster_phi; //!
  std::vector<float> m_b_Cluster_energyBE; //!
  std::vector<float> m_b_Cluster_etaBE; //!
  std::vector<float> m_b_Cluster_phiBE; //!
  std::vector<float> m_b_Cluster_calE; //!
  std::vector<float> m_b_Cluster_calEta; //!
  std::vector<float> m_b_Cluster_calPhi; //!
  std::vector<int> m_b_Cluster_size; //!
  std::vector<int> m_b_Cluster_status; //!

  // muon info
  int m_b_muon_n; //!
  float m_b_muon_pt_precalib[40]; //!
  float m_b_muon_pt[40]; //!
  float m_b_muon_ms_pt_precalib[40]; //!
  float m_b_muon_ms_pt[40]; //!
  float m_b_muon_eta[40]; //!
  float m_b_muon_phi[40]; //!
  int m_b_muon_charge[40]; //!
  bool m_b_muon_tight[40]; //!
  bool m_b_muon_medium[40]; //!
  bool m_b_muon_loose[40]; //!
  bool m_b_muon_matched[40]; //!
  float m_b_muon_etcone20[40]; //!
  float m_b_muon_etcone30[40]; //!
  float m_b_muon_etcone40[40]; //!
  float m_b_muon_topoetcone20[40]; //!
  float m_b_muon_topoetcone30[40]; //!
  float m_b_muon_topoetcone40[40]; //!
  float m_b_muon_id_track_pt[40]; //!
  float m_b_muon_id_track_eta[40]; //!
  float m_b_muon_id_track_phi[40]; //!
  float m_b_muon_id_track_charge[40]; //!
  float m_b_muon_id_track_d0[40]; //!
  float m_b_muon_id_track_d0sig[40]; //!
  float m_b_muon_id_track_z0[40]; //!
  float m_b_muon_id_track_z0sig[40]; //!
  float m_b_muon_id_track_theta[40]; //!
  float m_b_muon_id_track_vz[40]; //!
  bool m_b_muon_id_track_tightprimary[40]; //!
  bool m_b_muon_id_track_hiloose[40]; //!
  bool m_b_muon_id_track_hitight[40]; //!
  float m_b_muon_ms_track_pt[40]; //!
  float m_b_muon_ms_track_eta[40]; //!
  float m_b_muon_ms_track_phi[40]; //!
  float m_b_muon_ms_track_charge[40]; //!
  float m_b_muon_ms_track_d0[40]; //!
  float m_b_muon_ms_track_d0sig[40]; //!
  float m_b_muon_ms_track_z0[40]; //!
  float m_b_muon_ms_track_z0sig[40]; //!
  float m_b_muon_ms_track_theta[40]; //!
  float m_b_muon_ms_track_vz[40]; //!
  std::vector <std::vector <double>> m_b_muon_pt_sys; //!
  std::vector <std::vector <double>> m_b_muon_eta_sys; //!
  std::vector <std::vector <double>> m_b_muon_phi_sys; //!

  // tracking info (0th or primary vertex only)
  int m_b_ntrk; //!
  float m_b_trk_pt[10000]; //!
  float m_b_trk_eta[10000]; //!
  float m_b_trk_phi[10000]; //!
  float m_b_trk_charge[10000]; //!
  bool m_b_trk_HItight[10000]; //!
  bool m_b_trk_HIloose[10000]; //!
  bool m_b_trk_TightPrimary[10000]; //!
  float m_b_trk_d0[10000]; //!
  float m_b_trk_d0sig[10000]; //!
  float m_b_trk_z0[10000]; //!
  float m_b_trk_z0sig[10000]; //!
  float m_b_trk_theta[10000]; //!
  float m_b_trk_vz[10000]; //!
  int m_b_trk_nBLayerHits[10000]; //!
  int m_b_trk_nBLayerSharedHits[10000]; //!
  int m_b_trk_nPixelHits[10000]; //!
  int m_b_trk_nPixelDeadSensors[10000]; //!
  int m_b_trk_nPixelSharedHits[10000]; //!
  int m_b_trk_nSCTHits[10000]; //!
  int m_b_trk_nSCTDeadSensors[10000]; //!
  int m_b_trk_nSCTSharedHits[10000]; //!
  int m_b_trk_nTRTHits[10000]; //!
  int m_b_trk_nTRTSharedHits[10000]; //!

  float m_b_trk_prob_truth[10000]; //!
  float m_b_trk_truth_pt[10000]; //!
  float m_b_trk_truth_eta[10000]; //!
  float m_b_trk_truth_phi[10000]; //!
  float m_b_trk_truth_charge[10000]; //!
  int m_b_trk_truth_type[10000]; //!
  int m_b_trk_truth_orig[10000]; //!
  int m_b_trk_truth_barcode[10000]; //!
  int m_b_trk_truth_pdgid[10000]; //!
  float m_b_trk_truth_vz[10000]; //!
  int m_b_trk_truth_nIn[10000]; //!
  bool m_b_trk_truth_isHadron[10000]; //!

  // EMTopo jets for pp collisions
  int m_b_akt4emtopo_jet_n; //!
  float m_b_akt4emtopo_jet_pt[100]; //!
  float m_b_akt4emtopo_jet_eta[100]; //!
  float m_b_akt4emtopo_jet_phi[100]; //!
  float m_b_akt4emtopo_jet_e[100]; //!

  // HI jets
  int m_b_akt4hi_jet_n; //!
  float m_b_akt4hi_jet_pt[100]; //!
  float m_b_akt4hi_jet_eta[100]; //!
  float m_b_akt4hi_jet_phi[100]; //!
  float m_b_akt4hi_jet_e[100]; //!

  // truth electron info
  int m_b_truth_electron_n; //!
  float m_b_truth_electron_pt[1000]; //!
  float m_b_truth_electron_eta[1000]; //!
  float m_b_truth_electron_phi[1000]; //!
  int m_b_truth_electron_charge[1000]; //!
  int m_b_truth_electron_barcode[1000]; //!

  // truth muon info
  int m_b_truth_muon_n; //!
  float m_b_truth_muon_pt[1000]; //!
  float m_b_truth_muon_eta[1000]; //!
  float m_b_truth_muon_phi[1000]; //!
  int m_b_truth_muon_charge[1000]; //!
  int m_b_truth_muon_barcode[1000]; //!

  // truth track info
  int m_b_truth_trk_n; //!
  float m_b_truth_trk_pt[10000]; //!
  float m_b_truth_trk_eta[10000]; //!
  float m_b_truth_trk_phi[10000]; //!
  float m_b_truth_trk_charge[10000]; //!
  int m_b_truth_trk_pdgid[10000]; //!
  int m_b_truth_trk_barcode[10000]; //!
  bool m_b_truth_trk_isHadron[10000]; //!

  // truth jet info
  int m_b_akt4_truth_jet_n; //!
  float m_b_akt4_truth_jet_pt[100]; //!
  float m_b_akt4_truth_jet_eta[100]; //!
  float m_b_akt4_truth_jet_phi[100]; //!
  float m_b_akt4_truth_jet_e[100]; //!

  const static int m_electronTrigN = 3; //!
  std::string m_electronTrigNames[m_electronTrigN] = { 
    "HLT_e15_lhloose_L1EM12",
    "HLT_e15_lhloose_ion_L1EM12",
    "HLT_e15_loose_ion_L1EM12"
  }; //!

  bool m_b_electronTrigBool[m_electronTrigN] = {}; //!
  float m_b_electronTrigPrescale[m_electronTrigN] = {}; //!

  const static int m_muonTrigN = 1; //!
  std::string m_muonTrigNames[m_muonTrigN] = {
    "HLT_mu14"
    //"HLT_mu8"
  }; //!

  bool m_b_muonTrigBool[m_muonTrigN] = {}; //!
  float m_b_muonTrigPrescale[m_muonTrigN] = {}; //!

  // Electron tools
  AsgElectronLikelihoodTool* m_electronLHTightSelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHMediumSelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHLooseSelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHMediumHISelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHLooseHISelectorTool; //!
  CP::EgammaCalibrationAndSmearingTool* m_egammaPtEtaPhiECorrector; //!

  // Muon tools
  CP::MuonSelectionTool* m_muonTightSelectorTool; //!
  CP::MuonSelectionTool* m_muonMediumSelectorTool; //!
  CP::MuonSelectionTool* m_muonLooseSelectorTool; //!
  CP::MuonCalibrationAndSmearingTool* m_muonPtEtaPhiECorrector; //!

  // Jet calibration tools
  // EtaJES calibration tool - EM scale
  //JetCalibrationTool* m_Akt4HI_EM_EtaJES_CalibTool; //!
  // 2015 insitu + cross calibration tool
  //JetCalibrationTool* m_Akt4HI_Insitu_CalibTool; //!

  // Track selection tool
  InDet::InDetTrackSelectionTool* m_trackSelectionToolTightPrimary; //!
  //InDet::InDetTrackSelectionTool* m_trackSelectionMinbias; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHITight; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHILoose; //!

  // GRL tools
  GoodRunsListSelectionTool* m_pp17_grl; //!
  GoodRunsListSelectionTool* m_PbPb15_grl; //!
  GoodRunsListSelectionTool* m_PbPb18_grl; //!
  GoodRunsListSelectionTool* m_PbPb18_ignoreToroid_grl; //!

  // Zdc Tool
  ZDC::ZdcAnalysisTool* m_zdcAnalysisTool; //!

  // Trigger tools
  Trig::TrigDecisionTool* m_trigDecisionTool; //!
  TrigConf::xAODConfigTool* m_trigConfigTool; //!
  Trig::MatchingTool* m_trigMatchingTool; //!

  // MC Truth Classifier
  MCTruthClassifier* m_mcTruthClassifier; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // this is a standard constructor
  TreeMaker ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  bool isDesynEvent (const int _runNumber, const int _lumiBlock);
  bool is_Outpileup (const xAOD::HIEventShapeContainer& evShCont, const int nTrack);
  bool passSinfigCut (float eta, int cellsigsamp, float cellsig);
  float CellSigCut (float x);
  void ClusterGapCut (std::vector <float>& clus);
  void GapCut (std::vector<float>& trks, std::vector<float>& clus);
  uint8_t getSum (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType );

  // this is needed to distribute the algorithm to the workers
  ClassDef(TreeMaker, 1);
};

#endif
