#include <ZTrackAnalysis/TreeMaker.h>

// EventLoop includes
#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

// root includes
#include <TFile.h>
#include <TSystem.h>
#include <TLorentzVector.h>

// xAOD includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"
#include "xAODTrigger/EmTauRoIContainer.h"

// ZDC includes
#include "xAODForward/ZdcModuleContainer.h"

// Tracking & vertex includes
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

// Truth includes
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"

// EGamma includes
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/EgammaTruthxAODHelpers.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloCluster.h"


// Muon includes
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/Muon.h>

// Jet includes
#include "xAODJet/JetContainer.h"

// Path resolver tool
#include <PathResolver/PathResolver.h>


// this is needed to distribute the algorithm to the workers
ClassImp(TreeMaker)

typedef std::vector <ElementLink <xAOD::TrackParticleContainer>> TPELVec_t;


TreeMaker :: TreeMaker () {
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  m_pp17_grl = nullptr;
  m_PbPb15_grl = nullptr;
  m_PbPb18_grl = nullptr;
  m_PbPb18_ignoreToroid_grl = nullptr;

  m_egammaPtEtaPhiECorrector = nullptr;
  m_electronLHTightSelectorTool = nullptr;
  m_electronLHMediumSelectorTool = nullptr;
  m_electronLHLooseSelectorTool = nullptr;
  m_electronLHMediumHISelectorTool = nullptr;
  m_electronLHLooseHISelectorTool = nullptr;

  m_muonPtEtaPhiECorrector = nullptr;
  m_muonTightSelectorTool = nullptr;
  m_muonMediumSelectorTool = nullptr;
  m_muonLooseSelectorTool = nullptr;

  //m_Akt4HI_EM_EtaJES_CalibTool = nullptr;

  m_trackSelectionToolTightPrimary = nullptr;
  //m_trackSelectionToolMinbias = nullptr;
  m_trackSelectionToolHITight = nullptr;
  m_trackSelectionToolHILoose = nullptr;

  m_trigDecisionTool = nullptr;
  m_trigConfigTool = nullptr;
  m_trigMatchingTool = nullptr;
}



EL::StatusCode TreeMaker :: setupJob (EL::Job& job) {
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD (); 
 
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: histInitialize () {
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk()->getOutputFile (m_outputName);
  
  m_tree = new TTree ("bush", "bush");
  m_tree->SetDirectory (outputFile);

  // event info 
  m_tree->Branch ("passes_toroid",  &m_b_passesToroid);
  m_tree->Branch ("run_number",     &m_b_runNum,  "run_number/i");
  m_tree->Branch ("lumi_block",     &m_b_lbn,     "lumi_block/i"); 
  m_tree->Branch ("event_number",   &m_b_evtNum,  "event_number/i");
  m_tree->Branch ("isOOTPU",        &m_b_isOOTPU,       "isOOTPU/O");
  m_tree->Branch ("BlayerDesyn",    &m_b_BlayerDesyn,   "BlayerDesyn/O");

  // collision info
  m_tree->Branch ("actualInteractionsPerCrossing",  &m_b_actualInteractionsPerCrossing,   "actualInteractionsPerCrossing/F");
  m_tree->Branch ("averageInteractionsPerCrossing", &m_b_averageInteractionsPerCrossing,  "averageInteractionsPerCrossing/F");

  if (m_dataType != Collisions) {
    m_tree->Branch ("mcEventWeights",     &m_b_mcEventWeights);
    m_tree->Branch ("nTruthEvt",          &m_b_nTruthEvt,           "nTruthEvt/I");
    m_tree->Branch ("nPart1",             &m_b_nPart1,              "nPart1[nTruthEvt]/I");
    m_tree->Branch ("nPart2",             &m_b_nPart2,              "nPart2[nTruthEvt]/I");
    m_tree->Branch ("impactParameter",    &m_b_impactParameter,     "impactParameter[nTruthEvt]/F");
    m_tree->Branch ("nColl",              &m_b_nColl,               "nColl[nTruthEvt]/I");
    m_tree->Branch ("nSpectatorNeutrons", &m_b_nSpectatorNeutrons,  "nSpectatorNeutrons[nTruthEvt]/I");
    m_tree->Branch ("nSpectatorProtons",  &m_b_nSpectatorProtons,   "nSpectatorProtons[nTruthEvt]/I");
    m_tree->Branch ("eccentricity",       &m_b_eccentricity,        "eccentricity[nTruthEvt]/F");
    m_tree->Branch ("eventPlaneAngle",    &m_b_eventPlaneAngle,     "eventPlaneAngle[nTruthEvt]/F");
  }

  // triggers
  if (m_dataType == Collisions) {
    for (int iTrig = 0; iTrig < m_electronTrigN; iTrig++) {
      m_tree->Branch (m_electronTrigNames[iTrig].c_str(), &(m_b_electronTrigBool[iTrig]), Form ("%s/O", m_electronTrigNames[iTrig].c_str()));
      m_tree->Branch (Form ("%s_prescale", m_electronTrigNames[iTrig].c_str()), &(m_b_electronTrigPrescale[iTrig]), Form ("%s_prescale/F", m_electronTrigNames[iTrig].c_str()));
    }
    for (int iTrig = 0; iTrig < m_muonTrigN; iTrig++) {
      m_tree->Branch (m_muonTrigNames[iTrig].c_str(), &(m_b_muonTrigBool[iTrig]), Form ("%s/O", m_muonTrigNames[iTrig].c_str()));
      m_tree->Branch (Form ("%s_prescale", m_muonTrigNames[iTrig].c_str()), &(m_b_muonTrigPrescale[iTrig]), Form ("%s_prescale/F", m_muonTrigNames[iTrig].c_str()));
    }
  }

  // vertices
  m_tree->Branch ("nvert",      &m_b_nvert,       "nvert/I");
  m_tree->Branch ("vert_x",     &m_b_vert_x,      "vert_x[nvert]/F");
  m_tree->Branch ("vert_y",     &m_b_vert_y,      "vert_y[nvert]/F");
  m_tree->Branch ("vert_z",     &m_b_vert_z,      "vert_z[nvert]/F");
  m_tree->Branch ("vert_ntrk",  &m_b_vert_ntrk,   "vert_ntrk[nvert]/I");
  m_tree->Branch ("vert_type",  &m_b_vert_type,   "vert_type[nvert]/I");
  m_tree->Branch ("vert_sumpt", &m_b_vert_sumpt,  "vert_sumpt[nvert]/F");

  // forward calorimeters
  //if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18) {
    m_tree->Branch ("fcalA_et",       &m_b_fcalA_et,      "fcalA_et/F");
    m_tree->Branch ("fcalC_et",       &m_b_fcalC_et,      "fcalC_et/F");
    m_tree->Branch ("fcalA_et_Cos2",  &m_b_fcalA_et_Cos2, "fcalA_et_Cos2/F");
    m_tree->Branch ("fcalC_et_Cos2",  &m_b_fcalC_et_Cos2, "fcalC_et_Cos2/F");
    m_tree->Branch ("fcalA_et_Sin2",  &m_b_fcalA_et_Sin2, "fcalA_et_Sin2/F");
    m_tree->Branch ("fcalC_et_Sin2",  &m_b_fcalC_et_Sin2, "fcalC_et_Sin2/F");
    m_tree->Branch ("fcalA_et_Cos3",  &m_b_fcalA_et_Cos3, "fcalA_et_Cos3/F");
    m_tree->Branch ("fcalC_et_Cos3",  &m_b_fcalC_et_Cos3, "fcalC_et_Cos3/F");
    m_tree->Branch ("fcalA_et_Sin3",  &m_b_fcalA_et_Sin3, "fcalA_et_Sin3/F");
    m_tree->Branch ("fcalC_et_Sin3",  &m_b_fcalC_et_Sin3, "fcalC_et_Sin3/F");
    m_tree->Branch ("fcalA_et_Cos4",  &m_b_fcalA_et_Cos4, "fcalA_et_Cos4/F");
    m_tree->Branch ("fcalC_et_Cos4",  &m_b_fcalC_et_Cos4, "fcalC_et_Cos4/F");
    m_tree->Branch ("fcalA_et_Sin4",  &m_b_fcalA_et_Sin4, "fcalA_et_Sin4/F");
    m_tree->Branch ("fcalC_et_Sin4",  &m_b_fcalC_et_Sin4, "fcalC_et_Sin4/F");
  //}

  // ZDC
  if ((m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) && m_dataType == Collisions) {
    m_tree->Branch ("ZdcCalibEnergy_A",   &m_b_ZdcCalibEnergy_A,  "ZdcCalibEnergy_A/F");
    m_tree->Branch ("ZdcCalibEnergy_C",   &m_b_ZdcCalibEnergy_C,  "ZdcCalibEnergy_C/F");
    m_tree->Branch ("L1_ZDC_A",           &m_b_L1_ZDC_A,          "L1_ZDC_A/O");
    m_tree->Branch ("L1_ZDC_A_tbp",       &m_b_L1_ZDC_A_tbp,      "L1_ZDC_A_tbp/O");
    m_tree->Branch ("L1_ZDC_A_tap",       &m_b_L1_ZDC_A_tap,      "L1_ZDC_A_tap/O");
    m_tree->Branch ("L1_ZDC_A_tav",       &m_b_L1_ZDC_A_tav,      "L1_ZDC_A_tav/O");
    m_tree->Branch ("L1_ZDC_A_prescale",  &m_b_L1_ZDC_A_prescale, "L1_ZDC_A_prescale/F");
    m_tree->Branch ("L1_ZDC_C",           &m_b_L1_ZDC_C,          "L1_ZDC_C/O");
    m_tree->Branch ("L1_ZDC_C_tbp",       &m_b_L1_ZDC_C_tbp,      "L1_ZDC_C_tbp/O");
    m_tree->Branch ("L1_ZDC_C_tap",       &m_b_L1_ZDC_C_tap,      "L1_ZDC_C_tap/O");
    m_tree->Branch ("L1_ZDC_C_tav",       &m_b_L1_ZDC_C_tav,      "L1_ZDC_C_tav/O");
    m_tree->Branch ("L1_ZDC_C_prescale",  &m_b_L1_ZDC_C_prescale, "L1_ZDC_C_prescale/F");
  }

  // sum of gaps and edge gaps
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18) {
    m_tree->Branch("cluster_sumGap_A",  &m_b_clusterOnly_sum_gap_A,   "cluster_sumGap_A/F");
    m_tree->Branch("cluster_sumGap_C",  &m_b_clusterOnly_sum_gap_C,   "cluster_sumGap_C/F");
    m_tree->Branch("cluster_edgeGap_A", &m_b_clusterOnly_edge_gap_A,  "cluster_edgeGap_A/F");
    m_tree->Branch("cluster_edgeGap_C", &m_b_clusterOnly_edge_gap_C,  "cluster_edgeGap_C/F");
    m_tree->Branch("sumGap_A",          &m_b_sum_gap_A,               "sumGap_A/F");
    m_tree->Branch("sumGap_C",          &m_b_sum_gap_C,               "sumGap_C/F");
    m_tree->Branch("edgeGap_A",         &m_b_edge_gap_A,              "edgeGap_A/F");
    m_tree->Branch("edgeGap_C",         &m_b_edge_gap_C,              "edgeGap_C/F");
  }

  // reco electrons
  m_tree->Branch ("electron_n",                     &m_b_electron_n,                      "electron_n/I");
  m_tree->Branch ("electron_pt_precalib",           &m_b_electron_pt_precalib,            "electron_pt_precalib[electron_n]/F");
  m_tree->Branch ("electron_pt",                    &m_b_electron_pt,                     "electron_pt[electron_n]/F");
  m_tree->Branch ("electron_eta",                   &m_b_electron_eta,                    "electron_eta[electron_n]/F");
  m_tree->Branch ("electron_phi",                   &m_b_electron_phi,                    "electron_phi[electron_n]/F");
  m_tree->Branch ("electron_charge",                &m_b_electron_charge,                 "electron_charge[electron_n]/I");
  m_tree->Branch ("electron_lhtight",               &m_b_electron_lhtight,                "electron_lhtight[electron_n]/O");
  m_tree->Branch ("electron_lhmedium",              &m_b_electron_lhmedium,               "electron_lhmedium[electron_n]/O");
  m_tree->Branch ("electron_lhloose",               &m_b_electron_lhloose,                "electron_lhloose[electron_n]/O");
  m_tree->Branch ("electron_lhmedium_hi",           &m_b_electron_lhmedium_hi,            "electron_lhmedium_hi[electron_n]/O");
  m_tree->Branch ("electron_lhloose_hi",            &m_b_electron_lhloose_hi,             "electron_lhloose_hi[electron_n]/O");
  if (m_dataType == Collisions)
    m_tree->Branch ("electron_matched",             &m_b_electron_matched,                "electron_matched[electron_n]/O");
  m_tree->Branch ("electron_etcone20",              &m_b_electron_etcone20,               "electron_etcone20[electron_n]/F");
  m_tree->Branch ("electron_etcone30",              &m_b_electron_etcone30,               "electron_etcone30[electron_n]/F");
  m_tree->Branch ("electron_etcone40",              &m_b_electron_etcone40,               "electron_etcone40[electron_n]/F");
  m_tree->Branch ("electron_topoetcone20",          &m_b_electron_topoetcone20,           "electron_topoetcone20[electron_n]/F");
  m_tree->Branch ("electron_topoetcone30",          &m_b_electron_topoetcone30,           "electron_topoetcone30[electron_n]/F");
  m_tree->Branch ("electron_topoetcone40",          &m_b_electron_topoetcone40,           "electron_topoetcone30[electron_n]/F");
  //m_tree->Branch ("electron_ntrk",                  &m_b_electron_ntrk);
  m_tree->Branch ("electron_id_track_pt",           &m_b_electron_id_track_pt,            "electron_id_track_pt[electron_n]/F");
  m_tree->Branch ("electron_id_track_eta",          &m_b_electron_id_track_eta,           "electron_id_track_eta[electron_n]/F");
  m_tree->Branch ("electron_id_track_phi",          &m_b_electron_id_track_phi,           "electron_id_track_phi[electron_n]/F");
  m_tree->Branch ("electron_id_track_charge",       &m_b_electron_id_track_charge,        "electron_id_track_charge[electron_n]/F");
  m_tree->Branch ("electron_id_track_d0",           &m_b_electron_id_track_d0,            "electron_id_track_d0[electron_n]/F");
  m_tree->Branch ("electron_id_track_d0sig",        &m_b_electron_id_track_d0sig,         "electron_id_track_d0sig[electron_n]/F");
  m_tree->Branch ("electron_id_track_z0",           &m_b_electron_id_track_z0,            "electron_id_track_z0[electron_n]/F");
  m_tree->Branch ("electron_id_track_z0sig",        &m_b_electron_id_track_z0sig,         "electron_id_track_z0sig[electron_n]/F");
  m_tree->Branch ("electron_id_track_theta",        &m_b_electron_id_track_theta,         "electron_id_track_theta[electron_n]/F");
  m_tree->Branch ("electron_id_track_vz",           &m_b_electron_id_track_vz,            "electron_id_track_vz[electron_n]/F");
  m_tree->Branch ("electron_id_track_tightprimary", &m_b_electron_id_track_tightprimary,  "electron_id_track_tightprimary[electron_n]/O");
  m_tree->Branch ("electron_id_track_hiloose",      &m_b_electron_id_track_hiloose,       "electron_id_track_hiloose[electron_n]/O");
  m_tree->Branch ("electron_id_track_hitight",      &m_b_electron_id_track_hitight,       "electron_id_track_hitight[electron_n]/O");
 
  m_tree->Branch ("electron_pt_sys",                &m_b_electron_pt_sys,                 "electron_pt_sys[electron_n]/F");
  m_tree->Branch ("electron_eta_sys",               &m_b_electron_eta_sys,                "electron_eta_sys[electron_n]/F");
  m_tree->Branch ("electron_phi_sys",               &m_b_electron_phi_sys,                "electron_phi_sys[electron_n]/F");

  m_tree->Branch ("Cluster_n",        &m_b_Cluster_n);
  m_tree->Branch ("Cluster_pt",       &m_b_Cluster_pt);
  m_tree->Branch ("Cluster_et",       &m_b_Cluster_et);
  m_tree->Branch ("Cluster_eta",      &m_b_Cluster_eta);
  m_tree->Branch ("Cluster_phi",      &m_b_Cluster_phi);
  m_tree->Branch ("Cluster_energyBE", &m_b_Cluster_energyBE);
  m_tree->Branch ("Cluster_etaBE",    &m_b_Cluster_etaBE);
  m_tree->Branch ("Cluster_phiBE",    &m_b_Cluster_phiBE);
  m_tree->Branch ("Cluster_calE",     &m_b_Cluster_calE);
  m_tree->Branch ("Cluster_calEta",   &m_b_Cluster_calEta);
  m_tree->Branch ("Cluster_calPhi",   &m_b_Cluster_calPhi);
  m_tree->Branch ("Cluster_size",     &m_b_Cluster_size);
  m_tree->Branch ("Cluster_status",   &m_b_Cluster_status);

  // reco muons
  m_tree->Branch ("muon_n",                     &m_b_muon_n,                      "muon_n/I");
  m_tree->Branch ("muon_pt_precalib",           &m_b_muon_pt_precalib,            "muon_pt_precalib[muon_n]/F");
  m_tree->Branch ("muon_pt",                    &m_b_muon_pt,                     "muon_pt[muon_n]/F");
  m_tree->Branch ("muon_ms_pt_precalib",        &m_b_muon_ms_pt_precalib,         "muon_ms_pt_precalib[muon_n]/F");
  m_tree->Branch ("muon_ms_pt",                 &m_b_muon_ms_pt,                  "muon_ms_pt[muon_n]/F");
  m_tree->Branch ("muon_eta",                   &m_b_muon_eta,                    "muon_eta[muon_n]/F");
  m_tree->Branch ("muon_phi",                   &m_b_muon_phi,                    "muon_phi[muon_n]/F");
  m_tree->Branch ("muon_charge",                &m_b_muon_charge,                 "muon_charge[muon_n]/I");
  m_tree->Branch ("muon_tight",                 &m_b_muon_tight,                  "muon_tight[muon_n]/O");
  m_tree->Branch ("muon_medium",                &m_b_muon_medium,                 "muon_medium[muon_n]/O");
  m_tree->Branch ("muon_loose",                 &m_b_muon_loose,                  "muon_loose[muon_n]/O");
  if (m_dataType == Collisions)
    m_tree->Branch ("muon_matched",             &m_b_muon_matched,                "muon_matched[muon_n]/O");
  m_tree->Branch ("muon_etcone20",              &m_b_muon_etcone20,               "muon_etcone20[muon_n]/F");
  m_tree->Branch ("muon_etcone30",              &m_b_muon_etcone30,               "muon_etcone30[muon_n]/F");
  m_tree->Branch ("muon_etcone40",              &m_b_muon_etcone40,               "muon_etcone40[muon_n]/F");
  m_tree->Branch ("muon_topoetcone20",          &m_b_muon_topoetcone20,           "muon_topoetcone20[muon_n]/F");
  m_tree->Branch ("muon_topoetcone30",          &m_b_muon_topoetcone30,           "muon_topoetcone30[muon_n]/F");
  m_tree->Branch ("muon_topoetcone40",          &m_b_muon_topoetcone40,           "muon_topoetcone40[muon_n]/F");
  m_tree->Branch ("muon_id_track_pt",           &m_b_muon_id_track_pt,            "muon_id_track_pt[muon_n]/F");
  m_tree->Branch ("muon_id_track_eta",          &m_b_muon_id_track_eta,           "muon_id_track_eta[muon_n]/F");
  m_tree->Branch ("muon_id_track_phi",          &m_b_muon_id_track_phi,           "muon_id_track_phi[muon_n]/F");
  m_tree->Branch ("muon_id_track_charge",       &m_b_muon_id_track_charge,        "muon_id_track_charge[muon_n]/F");
  m_tree->Branch ("muon_id_track_d0",           &m_b_muon_id_track_d0,            "muon_id_track_d0[muon_n]/F");
  m_tree->Branch ("muon_id_track_d0sig",        &m_b_muon_id_track_d0sig,         "muon_id_track_d0sig[muon_n]/F");
  m_tree->Branch ("muon_id_track_z0",           &m_b_muon_id_track_z0,            "muon_id_track_z0[muon_n]/F");
  m_tree->Branch ("muon_id_track_z0sig",        &m_b_muon_id_track_z0sig,         "muon_id_track_z0sig[muon_n]/F");
  m_tree->Branch ("muon_id_track_theta",        &m_b_muon_id_track_theta,         "muon_id_track_theta[muon_n]/F");
  m_tree->Branch ("muon_id_track_vz",           &m_b_muon_id_track_vz,            "muon_id_track_vz[muon_n]/F");
  m_tree->Branch ("muon_id_track_tightprimary", &m_b_muon_id_track_tightprimary,  "muon_id_track_tightprimary[muon_n]/O");
  m_tree->Branch ("muon_id_track_hiloose",      &m_b_muon_id_track_hiloose,       "muon_id_track_hiloose[muon_n]/O");
  m_tree->Branch ("muon_id_track_hitight",      &m_b_muon_id_track_hitight,       "muon_id_track_hitight[muon_n]/O");
  m_tree->Branch ("muon_ms_track_pt",           &m_b_muon_ms_track_pt,            "muon_ms_track_pt[muon_n]/F");
  m_tree->Branch ("muon_ms_track_eta",          &m_b_muon_ms_track_eta,           "muon_ms_track_eta[muon_n]/F");
  m_tree->Branch ("muon_ms_track_phi",          &m_b_muon_ms_track_phi,           "muon_ms_track_phi[muon_n]/F");
  m_tree->Branch ("muon_ms_track_charge",       &m_b_muon_ms_track_charge,        "muon_ms_track_charge[muon_n]/F");
  m_tree->Branch ("muon_ms_track_d0",           &m_b_muon_ms_track_d0,            "muon_ms_track_d0[muon_n]/F");
  m_tree->Branch ("muon_ms_track_d0sig",        &m_b_muon_ms_track_d0sig,         "muon_ms_track_d0sig[muon_n]/F");
  m_tree->Branch ("muon_ms_track_z0",           &m_b_muon_ms_track_z0,            "muon_ms_track_z0[muon_n]/F");
  m_tree->Branch ("muon_ms_track_z0sig",        &m_b_muon_ms_track_z0sig,         "muon_ms_track_z0sig[muon_n]/F");
  m_tree->Branch ("muon_ms_track_theta",        &m_b_muon_ms_track_theta,         "muon_ms_track_theta[muon_n]/F");
  m_tree->Branch ("muon_ms_track_vz",           &m_b_muon_ms_track_vz,            "muon_ms_track_vz[muon_n]/F");
  m_tree->Branch ("muon_pt_sys",                &m_b_muon_pt_sys);//,                 "muon_pt_sys[muon_n]/F");
  m_tree->Branch ("muon_eta_sys",               &m_b_muon_eta_sys);//,                "muon_eta_sys[muon_n]/F");
  m_tree->Branch ("muon_phi_sys",               &m_b_muon_phi_sys);//,                "muon_phi_sys[muon_n]/F");

  // reco tracks
  m_tree->Branch ("ntrk",                   &m_b_ntrk,                  "ntrk/I");
  m_tree->Branch ("trk_pt",                 &m_b_trk_pt,                "trk_pt[ntrk]/F");
  m_tree->Branch ("trk_eta",                &m_b_trk_eta,               "trk_eta[ntrk]/F");
  m_tree->Branch ("trk_phi",                &m_b_trk_phi,               "trk_phi[ntrk]/F");
  m_tree->Branch ("trk_charge",             &m_b_trk_charge,            "trk_charge[ntrk]/F");
  m_tree->Branch ("trk_HItight",            &m_b_trk_HItight,           "trk_HItight[ntrk]/O");
  m_tree->Branch ("trk_HIloose",            &m_b_trk_HIloose,           "trk_HIloose[ntrk]/O");
  m_tree->Branch ("trk_TightPrimary",       &m_b_trk_TightPrimary,      "trk_TightPrimary[ntrk]/O");
  m_tree->Branch ("trk_d0",                 &m_b_trk_d0,                "trk_d0[ntrk]/F");
  m_tree->Branch ("trk_d0sig",              &m_b_trk_d0sig,             "trk_d0sig[ntrk]/F");
  m_tree->Branch ("trk_z0",                 &m_b_trk_z0,                "trk_z0[ntrk]/F");
  m_tree->Branch ("trk_z0sig",              &m_b_trk_z0sig,             "trk_z0sig[ntrk]/F");
  m_tree->Branch ("trk_theta",              &m_b_trk_theta,             "trk_theta[ntrk]/F");
  m_tree->Branch ("trk_vz",                 &m_b_trk_vz,                "trk_vz[ntrk]/F");
  m_tree->Branch ("trk_nBLayerHits",        &m_b_trk_nBLayerHits,       "trk_nBLayerHits[ntrk]/I");
  m_tree->Branch ("trk_nBLayerSharedHits",  &m_b_trk_nBLayerSharedHits, "trk_nBLayerSharedHits[ntrk]/I");
  m_tree->Branch ("trk_nPixelHits",         &m_b_trk_nPixelHits,        "trk_nPixelHits[ntrk]/I");
  m_tree->Branch ("trk_nPixelDeadSensors",  &m_b_trk_nPixelDeadSensors, "trk_nPixelDeadSensors[ntrk]/I");
  m_tree->Branch ("trk_nPixelSharedHits",   &m_b_trk_nPixelSharedHits,  "trk_nPixelSharedHits[ntrk]/I");
  m_tree->Branch ("trk_nSCTHits",           &m_b_trk_nSCTHits,          "trk_nSCTHits[ntrk]/I");
  m_tree->Branch ("trk_nSCTDeadSensors",    &m_b_trk_nSCTDeadSensors,   "trk_nSCTDeadSensors[ntrk]/I");
  m_tree->Branch ("trk_nSCTSharedHits",     &m_b_trk_nSCTSharedHits,    "trk_nSCTSharedHits[ntrk]/I");
  m_tree->Branch ("trk_nTRTHits",           &m_b_trk_nTRTHits,          "trk_nTRTHits[ntrk]/I");
  m_tree->Branch ("trk_nTRTSharedHits",     &m_b_trk_nTRTSharedHits,    "trk_nTRTSharedHits[ntrk]/I");

  if (m_dataType != Collisions) {
    m_tree->Branch ("trk_prob_truth",     &m_b_trk_prob_truth,      "trk_prob_truth[ntrk]/F");
    m_tree->Branch ("trk_truth_pt",       &m_b_trk_truth_pt,        "trk_truth_pt[ntrk]/F");
    m_tree->Branch ("trk_truth_eta",      &m_b_trk_truth_eta,       "trk_truth_eta[ntrk]/F");
    m_tree->Branch ("trk_truth_phi",      &m_b_trk_truth_phi,       "trk_truth_phi[ntrk]/F");
    m_tree->Branch ("trk_truth_charge",   &m_b_trk_truth_charge,    "trk_truth_charge[ntrk]/F");
    m_tree->Branch ("trk_truth_type",     &m_b_trk_truth_type,      "trk_truth_type[ntrk]/I");
    m_tree->Branch ("trk_truth_orig",     &m_b_trk_truth_orig,      "trk_truth_orig[ntrk]/I");
    m_tree->Branch ("trk_truth_barcode",  &m_b_trk_truth_barcode,   "trk_truth_barcode[ntrk]/I");
    m_tree->Branch ("trk_truth_pdgid",    &m_b_trk_truth_pdgid,     "trk_truth_pdgid[ntrk]/I");
    m_tree->Branch ("trk_truth_vz",       &m_b_trk_truth_vz,        "trk_truth_vz[ntrk]/F");
    m_tree->Branch ("trk_truth_nIn",      &m_b_trk_truth_nIn,       "trk_truth_nIn[ntrk]/I");
    m_tree->Branch ("trk_truth_isHadron", &m_b_trk_truth_isHadron,  "trk_truth_isHadron[ntrk]/O");
  }

  if (m_collisionSystem == pp17) {
    m_tree->Branch ("akt4emtopo_jet_n",   &m_b_akt4emtopo_jet_n,    "akt4emtopo_jet_n/I");
    m_tree->Branch ("akt4emtopo_jet_pt",  &m_b_akt4emtopo_jet_pt,   "akt4emtopo_jet_pt[akt4emtopo_jet_n]/F");
    m_tree->Branch ("akt4emtopo_jet_eta", &m_b_akt4emtopo_jet_eta,  "akt4emtopo_jet_eta[akt4emtopo_jet_n]/F");
    m_tree->Branch ("akt4emtopo_jet_phi", &m_b_akt4emtopo_jet_phi,  "akt4emtopo_jet_phi[akt4emtopo_jet_n]/F");
    m_tree->Branch ("akt4emtopo_jet_e",   &m_b_akt4emtopo_jet_e,    "akt4emtopo_jet_e[akt4emtopo_jet_n]/F");
  }

  m_tree->Branch ("akt4hi_jet_n",   &m_b_akt4hi_jet_n,    "akt4hi_jet_n/I");
  m_tree->Branch ("akt4hi_jet_pt",  &m_b_akt4hi_jet_pt,   "akt4hi_jet_pt[akt4hi_jet_n]/F");
  m_tree->Branch ("akt4hi_jet_eta", &m_b_akt4hi_jet_eta,  "akt4hi_jet_eta[akt4hi_jet_n]/F");
  m_tree->Branch ("akt4hi_jet_phi", &m_b_akt4hi_jet_phi,  "akt4hi_jet_phi[akt4hi_jet_n]/F");
  m_tree->Branch ("akt4hi_jet_e",   &m_b_akt4hi_jet_e,    "akt4hi_jet_e[akt4hi_jet_n]/F");

  if (m_dataType != Collisions) {
    // Truth electrons
    m_tree->Branch ("truth_electron_n",       &m_b_truth_electron_n,        "truth_electron_n/I");
    m_tree->Branch ("truth_electron_pt",      &m_b_truth_electron_pt,       "truth_electron_pt[truth_electron_n]/F");
    m_tree->Branch ("truth_electron_eta",     &m_b_truth_electron_eta,      "truth_electron_eta[truth_electron_n]/F");
    m_tree->Branch ("truth_electron_phi",     &m_b_truth_electron_phi,      "truth_electron_phi[truth_electron_n]/F");
    m_tree->Branch ("truth_electron_charge",  &m_b_truth_electron_charge,   "truth_electron_charge[truth_electron_n]/I");
    m_tree->Branch ("truth_electron_barcode", &m_b_truth_electron_barcode,  "truth_electron_barcode[truth_electron_n]/I");

    // Truth muons
    m_tree->Branch ("truth_muon_n",       &m_b_truth_muon_n,        "truth_muon_n/I");
    m_tree->Branch ("truth_muon_pt",      &m_b_truth_muon_pt,       "truth_muon_pt[truth_muon_n]/F");
    m_tree->Branch ("truth_muon_eta",     &m_b_truth_muon_eta,      "truth_muon_eta[truth_muon_n]/F");
    m_tree->Branch ("truth_muon_phi",     &m_b_truth_muon_phi,      "truth_muon_phi[truth_muon_n]/F");
    m_tree->Branch ("truth_muon_charge",  &m_b_truth_muon_charge,   "truth_muon_charge[truth_muon_n]/I");
    m_tree->Branch ("truth_muon_barcode", &m_b_truth_muon_barcode,  "truth_muon_barcode[truth_muon_n]/I");
  
    // Truth track info
    m_tree->Branch ("truth_trk_n",        &m_b_truth_trk_n,         "truth_trk_n/I");
    m_tree->Branch ("truth_trk_pt",       &m_b_truth_trk_pt,        "truth_trk_pt[truth_trk_n]/F");
    m_tree->Branch ("truth_trk_eta",      &m_b_truth_trk_eta,       "truth_trk_eta[truth_trk_n]/F");
    m_tree->Branch ("truth_trk_phi",      &m_b_truth_trk_phi,       "truth_trk_phi[truth_trk_n]/F");
    m_tree->Branch ("truth_trk_charge",   &m_b_truth_trk_charge,    "truth_trk_charge[truth_trk_n]/F");
    m_tree->Branch ("truth_trk_pdgid",    &m_b_truth_trk_pdgid,     "truth_trk_pdgid[truth_trk_n]/I");
    m_tree->Branch ("truth_trk_barcode",  &m_b_truth_trk_barcode,   "truth_trk_barcode[truth_trk_n]/I");
    m_tree->Branch ("truth_trk_isHadron", &m_b_truth_trk_isHadron,  "truth_trk_isHadron[truth_trk_n]/O");

    // Truth jet info
    m_tree->Branch ("akt4_truth_jet_n",   &m_b_akt4_truth_jet_n,    "akt4_truth_jet_n/I");
    m_tree->Branch ("akt4_truth_jet_pt",  &m_b_akt4_truth_jet_pt,   "akt4_truth_jet_pt[akt4_truth_jet_n]/F");
    m_tree->Branch ("akt4_truth_jet_eta", &m_b_akt4_truth_jet_eta,  "akt4_truth_jet_eta[akt4_truth_jet_n]/F");
    m_tree->Branch ("akt4_truth_jet_phi", &m_b_akt4_truth_jet_phi,  "akt4_truth_jet_phi[akt4_truth_jet_n]/F");
    m_tree->Branch ("akt4_truth_jet_e",   &m_b_akt4_truth_jet_e,    "akt4_truth_jet_e[akt4_truth_jet_n]/F");
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: fileExecute () {
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: changeInput (bool /*firstFile*/) {
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: initialize () {
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_CHECK_SET_TYPE (EL::StatusCode);


  //----------------------------------------------------------------------
  // Trigger initialization
  //----------------------------------------------------------------------
  if (m_dataType == Collisions) {
    // Initialize trigger configuration tool
    m_trigConfigTool = new TrigConf::xAODConfigTool ("xAODConfigTool"); // gives us access to the meta-data
    ANA_CHECK (m_trigConfigTool->initialize ());
    ToolHandle<TrigConf::ITrigConfigTool> trigConfigHandle (m_trigConfigTool);

    // Initialize trigger decision tool and connect to trigger configuration tool (NOTE: must be after trigger configuration tool is initialized!!!)
    m_trigDecisionTool = new Trig::TrigDecisionTool ("TrigDecisionTool");
    ANA_CHECK (m_trigDecisionTool->setProperty ("ConfigTool", trigConfigHandle)); // connect the TrigDecisionTool to the ConfigTool
    ANA_CHECK (m_trigDecisionTool->setProperty ("TrigDecisionKey", "xTrigDecision"));
    ANA_CHECK (m_trigDecisionTool->initialize ());

    m_trigMatchingTool = new Trig::MatchingTool ("xAODMatchingTool");
    //ANA_CHECK (m_trigMatchingTool->setProperty ("OutputLevel", MSG::DEBUG));
    ANA_CHECK (m_trigMatchingTool->initialize ());
  }
  else {
    m_trigConfigTool = nullptr;
    m_trigDecisionTool = nullptr;
    m_trigMatchingTool = nullptr;
  }

 
  //----------------------------------------------------------------------
  // GRL
  //----------------------------------------------------------------------
  if (m_dataType == Collisions || m_dataType == MCDataOverlay) {
    const char* pp17_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml", filePath.c_str ()));
    const char* PbPb15_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data15_hi.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_HeavyIonP_All_Good.xml", filePath.c_str ()));
    const char* PbPb18_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good.xml", filePath.c_str ()));
    const char* PbPb18_ignoreToroid_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml", filePath.c_str ()));

    std::vector<std::string> vecStringGRL;

    vecStringGRL.clear ();
    m_pp17_grl = new GoodRunsListSelectionTool("pp17_GoodRunsListSelectionTool");
    vecStringGRL.push_back (pp17_fullGRLFilePath);
    ANA_CHECK (m_pp17_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_pp17_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_pp17_grl->initialize ());

    vecStringGRL.clear ();
    m_PbPb15_grl = new GoodRunsListSelectionTool("PbPb15_GoodRunsListSelectionTool");
    vecStringGRL.push_back (PbPb15_fullGRLFilePath);
    ANA_CHECK (m_PbPb15_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_PbPb15_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_PbPb15_grl->initialize ());

    vecStringGRL.clear ();
    m_PbPb18_grl = new GoodRunsListSelectionTool("PbPb18_GoodRunsListSelectionTool");
    vecStringGRL.push_back (PbPb18_fullGRLFilePath);
    ANA_CHECK (m_PbPb18_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_PbPb18_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_PbPb18_grl->initialize ());

    vecStringGRL.clear ();
    m_PbPb18_ignoreToroid_grl = new GoodRunsListSelectionTool("PbPb18_ignoreToroid_GoodRunsListSelectionTool");
    vecStringGRL.push_back (PbPb18_ignoreToroid_fullGRLFilePath);
    ANA_CHECK (m_PbPb18_ignoreToroid_grl->setProperty ("GoodRunsListVec", vecStringGRL));
    ANA_CHECK (m_PbPb18_ignoreToroid_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK (m_PbPb18_ignoreToroid_grl->initialize ());
  }
  else {
    m_pp17_grl = nullptr;
    m_PbPb18_grl = nullptr;
    m_PbPb18_ignoreToroid_grl = nullptr;
    m_PbPb15_grl = nullptr;
  }


  //----------------------------------------------------------------------
  // Egamma calibration tool
  //----------------------------------------------------------------------
  m_egammaPtEtaPhiECorrector = new CP::EgammaCalibrationAndSmearingTool ("EgammaPtEtaPhiECorrector");
  if (m_collisionSystem == PbPb18) {
    ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("ESModel", "es2017_R21_ofc0_v1"));
  }
  else if (m_collisionSystem == pp17) {
    ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("ESModel", "es2017_R21_v1"));
  }
  else if (m_collisionSystem == PbPb15 || m_collisionSystem == pp15) {
    ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("ESModel", "es2015_5TeV"));
  }
  else {
    Error ("InitEgammaCalibrationAndSmearingTool ()", "Failed to recognize collision system!");
    return EL::StatusCode::FAILURE;
  }
  ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("isOverlayMC", m_dataType == MCDataOverlay));
  ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("decorrelationModel", "FULL_v1")); // 1NP_v1
  ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("useAFII", 0));
  if (m_dataType != Collisions)
    ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("randomRunNumber", EgammaCalibPeriodRunNumbersExample::run_2016));
  ANA_CHECK (m_egammaPtEtaPhiECorrector->initialize ());


  //----------------------------------------------------------------------
  // Electron selector tools
  //----------------------------------------------------------------------
  m_electronLHTightSelectorTool = new AsgElectronLikelihoodTool ("ElectronLHTightSelectorTool");
  ANA_CHECK (m_electronLHTightSelectorTool->setProperty ("WorkingPoint", "TightLHElectron"));
  ANA_CHECK (m_electronLHTightSelectorTool->initialize ());

  m_electronLHMediumSelectorTool = new AsgElectronLikelihoodTool ("ElectronLHMediumSelectorTool");
  ANA_CHECK (m_electronLHMediumSelectorTool->setProperty ("WorkingPoint", "MediumLHElectron"));
  ANA_CHECK (m_electronLHMediumSelectorTool->initialize ());

  m_electronLHLooseSelectorTool = new AsgElectronLikelihoodTool ("ElectronLHLooseSelectorTool");
  ANA_CHECK (m_electronLHLooseSelectorTool->setProperty ("WorkingPoint", "LooseLHElectron"));
  ANA_CHECK (m_electronLHLooseSelectorTool->initialize ());

  m_electronLHMediumHISelectorTool = new AsgElectronLikelihoodTool ("ElectronLHMediumHISelectorTool");
  ANA_CHECK (m_electronLHMediumHISelectorTool->setProperty ("ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20160907_HI/ElectronLikelihoodMediumOfflineConfig2016_HI.conf"));
  ANA_CHECK (m_electronLHMediumHISelectorTool->initialize ());

  m_electronLHLooseHISelectorTool = new AsgElectronLikelihoodTool ("ElectronLHLooseHISelectorTool");
  ANA_CHECK (m_electronLHLooseHISelectorTool->setProperty ("ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20160907_HI/ElectronLikelihoodLooseOfflineConfig2016_HI.conf"));
  ANA_CHECK (m_electronLHLooseHISelectorTool->initialize ());


  //----------------------------------------------------------------------
  // Initialize muon corrector tool
  //----------------------------------------------------------------------
  m_muonPtEtaPhiECorrector = new CP::MuonCalibrationAndSmearingTool ("MuonPtEtaPhiECorrector");
  if (m_collisionSystem == PbPb18) {
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Year", "Data18"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Release", "Recs2019_05_30"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data18"));
  }
  else if (m_collisionSystem == pp17) {
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Year", "Data17"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Release", "Recs2019_05_30"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data17"));
  }
  else if (m_collisionSystem == PbPb15) {
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Year", "Data15"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Release", "Recs2019_05_30"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data16"));
  }
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("StatComb", false));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaCorr", true));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("doSagittaMCDistortion", false));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaCorrPhaseSpace", true));
  ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("isOverlayMC", m_dataType == MCDataOverlay));
  //ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("OutputLevel", MSG::VERBOSE));
  ANA_CHECK (m_muonPtEtaPhiECorrector->initialize ());


  //----------------------------------------------------------------------
  // Muon selector tools
  //----------------------------------------------------------------------
  m_muonLooseSelectorTool = new CP::MuonSelectionTool ("MuonLooseSelection");
  ANA_CHECK (m_muonLooseSelectorTool->setProperty ("MaxEta", 2.5));
  ANA_CHECK (m_muonLooseSelectorTool->setProperty ("MuQuality", 2));
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18)
    ANA_CHECK (m_muonLooseSelectorTool->setProperty ("TrtCutOff", true));
  ANA_CHECK (m_muonLooseSelectorTool->initialize ());

  m_muonMediumSelectorTool = new CP::MuonSelectionTool ("MuonMediumSelection");
  ANA_CHECK (m_muonMediumSelectorTool->setProperty ("MaxEta", 2.5));
  ANA_CHECK (m_muonMediumSelectorTool->setProperty ("MuQuality", 1));
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18)
    ANA_CHECK (m_muonMediumSelectorTool->setProperty ("TrtCutOff", true));
  ANA_CHECK (m_muonMediumSelectorTool->initialize ());

  m_muonTightSelectorTool = new CP::MuonSelectionTool ("MuonTightSelection");
  ANA_CHECK (m_muonTightSelectorTool->setProperty ("MaxEta", 2.5));
  ANA_CHECK (m_muonTightSelectorTool->setProperty ("MuQuality", 0));
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18)
    ANA_CHECK (m_muonTightSelectorTool->setProperty ("TrtCutOff", true));
  ANA_CHECK (m_muonTightSelectorTool->initialize ());

  
  ////----------------------------------------------------------------------
  //// Jet calibration tools
  ////----------------------------------------------------------------------
  //m_Akt4HI_EM_EtaJES_CalibTool = new JetCalibrationTool ("Akt4HI_EM_EtaJES_CalibTool");
  //ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("JetCollection", "AntiKt4HI"));
  //ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("ConfigFile", "JES_MC16_HI_Jul2019_5TeV.config"));
  //ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("CalibSequence", m_isCollisions ? "EtaJES" : "EtaJES_Insitu"));
  //ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("CalibArea", "00-04-82"));
  //ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->setProperty ("IsData", m_isCollisions));
  //ANA_CHECK (m_Akt4HI_EM_EtaJES_CalibTool->initialize ());


  //----------------------------------------------------------------------
  // Track selection tools
  //----------------------------------------------------------------------
  m_trackSelectionToolTightPrimary = new InDet::InDetTrackSelectionTool ("TrackSelectionToolTightPrimary");
  ANA_CHECK (m_trackSelectionToolTightPrimary->setProperty ("CutLevel", "TightPrimary"));
  ANA_CHECK (m_trackSelectionToolTightPrimary->initialize ());

  //m_trackSelectionToolMinbias = new InDet::InDetTrackSelectionTool ("TrackSelectionToolMinbias");
  //ANA_CHECK (m_trackSelectionToolMinbias->setProperty ("CutLevel", "MinBias"));
  //ANA_CHECK (m_trackSelectionToolMinbias->initialize ());

  m_trackSelectionToolHITight = new InDet::InDetTrackSelectionTool ("TrackSelectionToolHITight");
  ANA_CHECK (m_trackSelectionToolHITight->setProperty ("CutLevel", "HITight"));
  ANA_CHECK (m_trackSelectionToolHITight->initialize ());

  m_trackSelectionToolHILoose = new InDet::InDetTrackSelectionTool ("TrackSelectionToolHILoose");
  ANA_CHECK (m_trackSelectionToolHILoose->setProperty ("CutLevel", "HILoose"));
  ANA_CHECK (m_trackSelectionToolHILoose->initialize ());


  //----------------------------------------------------------------------
  // Initialize MC truth tool
  //----------------------------------------------------------------------
  if (m_dataType != Collisions) {
    m_mcTruthClassifier = new MCTruthClassifier ("MCTruthClassifier");
    ANA_CHECK (m_mcTruthClassifier->initialize ());
  }


  //----------------------------------------------------------------------
  // CP recommended systematics tools
  //----------------------------------------------------------------------
  CP::SystematicSet recommendedMuonSystematics;
  recommendedMuonSystematics.insert (m_muonPtEtaPhiECorrector->recommendedSystematics ());
  for (auto sys : recommendedMuonSystematics) {
    muonSysList.push_back (CP::SystematicSet ({sys}));
  }

  CP::SystematicSet recommendedElectronSystematics;
  recommendedElectronSystematics.insert (m_egammaPtEtaPhiECorrector->recommendedSystematics ());
  for (auto sys : recommendedElectronSystematics) {
    TString syst_name = TString (sys.name ());
    if (!syst_name.BeginsWith ("EL") && !syst_name.BeginsWith ("EG"))
      continue;
    electronSysList.push_back (CP::SystematicSet ({sys}));
  }


  //----------------------------------------------------------------------
  // OOTPU tool
  //----------------------------------------------------------------------
  ANA_MSG_INFO (PathResolverFindCalibFile (Form ("ZTrackAnalysis/%s", m_oop_fname.c_str ())).c_str ());
  TFile* f_oop_In = TFile::Open (PathResolverFindCalibFile (Form ("ZTrackAnalysis/%s", m_oop_fname.c_str ())).c_str (), "READ");
  ANA_MSG_INFO ("Search for pileup file at " << PathResolverFindCalibFile (Form ("ZTrackAnalysis/%s", m_oop_fname.c_str ())).c_str ());
  if (!f_oop_In) {
    ANA_MSG_ERROR ("Could not find input Out-of-time Pileup calibration file " << m_oop_fname << ", exiting");
    return EL::StatusCode::FAILURE;
  }
  ANA_MSG_INFO ("Read Out-of-time pileup cuts from "<< m_oop_fname);
  m_oop_hMean  = (TH1D*)((TH1D*)f_oop_In->Get("hMeanTotal")) ->Clone("hMeanTotal_HIPileTool");  m_oop_hMean ->SetDirectory(0);
  m_oop_hMean->SetDirectory (0);
  m_oop_hSigma = (TH1D*)((TH1D*)f_oop_In->Get("hSigmaTotal"))->Clone("hSigmaTotal_HIPileTool"); m_oop_hSigma->SetDirectory(0);
  m_oop_hSigma->SetDirectory (0);


  //----------------------------------------------------------------------
  // ZDC calibration tool
  //----------------------------------------------------------------------
  if ((m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) && m_dataType == Collisions) {
    m_zdcAnalysisTool = new ZDC::ZdcAnalysisTool ("ZdcAnalysisTool");

    ANA_CHECK (m_zdcAnalysisTool->setProperty ("FlipEMDelay", false)); // false
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("LowGainOnly", false)); // false
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoCalib", true)); // true
    if (m_collisionSystem == PbPb18) {
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("Configuration", "PbPb2018")); // "PbPb2018"
    }
    else if (m_collisionSystem == PbPb15) {
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("Configuration", "PbPb2015")); // "PbPb2015"
    }
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("AuxSuffix", "_RP")); // "RP"
    ANA_CHECK (m_zdcAnalysisTool->setProperty ("ForceCalibRun", -1)); 

    if (m_collisionSystem == PbPb18) {
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoTrigEff", false)); // for now
      ANA_CHECK (m_zdcAnalysisTool->setProperty ("DoTimeCalib", false)); // for now
    }
    ANA_CHECK (m_zdcAnalysisTool->initialize ());
  }
  else {
    m_zdcAnalysisTool = nullptr;
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: execute () {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // set type of return code you are expecting
  // (add to top of each function once)

  ANA_CHECK_SET_TYPE (EL::StatusCode);

  //----------------------------
  // Event & collision information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  if (!evtStore ()->retrieve (eventInfo, "EventInfo").isSuccess ()) {
    Error ("GetEventInfo ()", "Failed to retrieve EventInfo container. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  m_b_runNum = eventInfo->runNumber ();
  m_b_lbn = eventInfo->lumiBlock ();
  m_b_evtNum = eventInfo->eventNumber ();
  m_b_actualInteractionsPerCrossing = eventInfo->actualInteractionsPerCrossing ();
  m_b_averageInteractionsPerCrossing = eventInfo->averageInteractionsPerCrossing ();
  m_b_BlayerDesyn = isDesynEvent (eventInfo->runNumber (), eventInfo->lumiBlock ());
 
  if (m_dataType == Collisions || m_dataType == MCDataOverlay) {
    // if data, check if event passes GRL
    if (m_collisionSystem == pp17) {
      if (!m_pp17_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
      m_b_passesToroid = true;
    } 
    else if (m_collisionSystem == PbPb18) {
      if (!m_PbPb18_ignoreToroid_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
      m_b_passesToroid = m_PbPb18_grl->passRunLB (*eventInfo);
    }
    else if (m_collisionSystem == PbPb15) {
      if (!m_PbPb15_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
      m_b_passesToroid = true;
    }
    else {
      Error ("CheckGRL ()", "Undefined collision system. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    // if events passes event cleaning
    if (eventInfo->errorState (xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ||
        eventInfo->errorState (xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ||
        eventInfo->errorState (xAOD::EventInfo::SCT) == xAOD::EventInfo::Error ||
        eventInfo->isEventFlagBitSet (xAOD::EventInfo::Core, 18)) {
      return EL::StatusCode::SUCCESS;
    }
  } // end GRL scope


  
  //----------------------------------------------------------------------
  // Truth event information
  //----------------------------------------------------------------------
  if (m_dataType == MCSignal || m_dataType == MCDataOverlay || m_dataType == MCHijing || m_dataType == MCHijingOverlay)
    m_b_mcEventWeights = eventInfo->mcEventWeights ();

  if (m_dataType == MCHijing || m_dataType == MCHijingOverlay) {

    const xAOD::TruthEventContainer* truthEvents = 0;
    if (!evtStore ()->retrieve (truthEvents, "TruthEvents").isSuccess ()) {
      Error ("GetTruthEvents ()", "Failed to retrieve TruthEvents container. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    m_b_nTruthEvt = 0;
    for (const auto* truthEvent : *truthEvents) {
      if (m_b_nTruthEvt >= 5) {
        Error ("GetTruthEvents ()", "Tried to overflow truth event arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      truthEvent->heavyIonParameter (m_b_nPart1[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::NPARTPROJ);
      truthEvent->heavyIonParameter (m_b_nPart2[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::NPARTTARG);
      truthEvent->heavyIonParameter (m_b_impactParameter[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::IMPACTPARAMETER);
      truthEvent->heavyIonParameter (m_b_nColl[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::NCOLL);
      truthEvent->heavyIonParameter (m_b_nSpectatorNeutrons[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::SPECTATORNEUTRONS);
      truthEvent->heavyIonParameter (m_b_nSpectatorProtons[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::SPECTATORPROTONS);
      truthEvent->heavyIonParameter (m_b_eccentricity[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::ECCENTRICITY);
      truthEvent->heavyIonParameter (m_b_eventPlaneAngle[m_b_nTruthEvt], xAOD::TruthEvent::HIParam::EVENTPLANEANGLE);
      m_b_nTruthEvt++;
    }
  } // end truth events scope



  //----------------------------------------------------------------------
  // Triggers
  //----------------------------------------------------------------------
  if (m_dataType == Collisions) {
    bool trigFired = false;

    // Electron triggers
    for (short iTrig = 0; iTrig < m_electronTrigN; iTrig++) {
      auto cg = m_trigDecisionTool->getChainGroup (m_electronTrigNames[iTrig].c_str ());
      m_b_electronTrigBool[iTrig] = cg->isPassed ();
      m_b_electronTrigPrescale[iTrig] = cg->getPrescale ();
      trigFired = trigFired || m_b_electronTrigBool[iTrig];
    }
    // Muon triggers
    for (short iTrig = 0; iTrig < m_muonTrigN; iTrig++) {
      auto cg = m_trigDecisionTool->getChainGroup (m_muonTrigNames[iTrig].c_str ());
      m_b_muonTrigBool[iTrig] = cg->isPassed ();
      m_b_muonTrigPrescale[iTrig] = cg->getPrescale ();
      trigFired = trigFired || m_b_muonTrigBool[iTrig];
    }
 
    // Make sure one of the triggers fired
    if (!trigFired)
      return EL::StatusCode::SUCCESS;
  } // end trigger scope



  //----------------------------------------------------------------------
  // Gather verticies information
  //----------------------------------------------------------------------
  const xAOD::Vertex* priVtx = nullptr;
  {
    m_b_nvert = 0;

    const xAOD::VertexContainer* primaryVertices = 0;
    if (!evtStore ()->retrieve (primaryVertices, "PrimaryVertices") .isSuccess ())  {
      Error ("GetPrimaryVertices ()", "Failed to retrieve PrimaryVertices container. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* vert : *primaryVertices) {
      if (m_b_nvert >= 30) {
        Error ("GetPrimaryVertices ()", "Tried to overflow vertex arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      //if (m_b_nvert == 0) {
      //  std::cout << "vertex type is ";
      //  switch (vert->vertexType ()) {
      //    case xAOD::VxType::NoVtx:         std::cout << "NoVtx";        break;
      //    case xAOD::VxType::PriVtx:        std::cout << "PriVtx";       break;
      //    case xAOD::VxType::SecVtx:        std::cout << "SecVtx";       break;
      //    case xAOD::VxType::PileUp:        std::cout << "PileUp";       break;
      //    case xAOD::VxType::ConvVtx:       std::cout << "ConvVtx";      break;
      //    case xAOD::VxType::V0Vtx:         std::cout << "V0Vtx";        break;
      //    case xAOD::VxType::KinkVtx:       std::cout << "KinkVtx";      break;
      //    case xAOD::VxType::NotSpecified:  std::cout << "NotSpecified"; break;
      //  }
      //  std::cout << std::endl;
      //}

      //if (vert->vertexType () == xAOD::VxType::PriVtx)
      if (m_b_nvert == 0 && vert)
        priVtx = vert;
      m_b_vert_x[m_b_nvert] = vert->x ();
      m_b_vert_y[m_b_nvert] = vert->y ();
      m_b_vert_z[m_b_nvert] = vert->z ();
      m_b_vert_ntrk[m_b_nvert] = vert->nTrackParticles ();
      m_b_vert_type[m_b_nvert] = vert->vertexType ();

      float sumpt = 0;
      for (const auto track : vert->trackParticleLinks ()) {
        if (!track.isValid ())
          continue;
        if (!m_trackSelectionToolHILoose->accept (**track, vert))
          continue;

        sumpt += (*track)->pt () * 1e-3;
      } // end loop over tracks
      m_b_vert_sumpt[m_b_nvert] = sumpt;
      m_b_nvert++;
    }
  } // end vertex scope

  if (m_b_nvert == 0 || priVtx == nullptr) {
    Info ("GetPrimaryVertices ()", "Warning: Did not find a primary vertex!");
    //return EL::StatusCode::SUCCESS;
  }
  else assert (priVtx->vertexType () == xAOD::VxType::PriVtx);



  //----------------------------------------------------------------------
  // Check for out of time pile up 
  //----------------------------------------------------------------------
  {
    if (m_collisionSystem == PbPb18) {
      const xAOD::TrackParticleContainer* trackContainer = 0;
      if (!evtStore ()->retrieve (trackContainer, "InDetTrackParticles").isSuccess ()) {
        Error ("GetInDetTrackParticles()", "Failed to retrieve InDetTrackParticles container. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      const xAOD::HIEventShapeContainer* caloSums = 0;  
      if (!evtStore ()->retrieve (caloSums, "CaloSums").isSuccess ()) {
        Error ("GetCaloSums ()", "Failed to retrieve CaloSums container. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      //m_b_barrelet = caloSums->at(0)->et()*1e-6 - caloSums->at(5)->et()*1e-6;

      //m_b_ootpu_ntrk = 0;
      int nTracks = 0;
      for (const auto* track : *trackContainer) {
        if (track->pt () < 500) // cut at 500 MeV
          continue;
        if (m_trackSelectionToolHITight->accept (*track, priVtx))
          //m_b_ootpu_ntrk++;
          nTracks++;
      } 

      m_b_isOOTPU = is_Outpileup (*caloSums, nTracks);

      //if (is_Outpileup (*hiueContainer, nTrack)) {
      //  Info ("CheckOOTPU ()", "Event identified as out-of-time-pile-up. Exiting.");
      //  return EL::StatusCode::SUCCESS;
      //}
    }
    else {
      m_b_isOOTPU = false;
      //m_b_ootpu_ntrk = 0;
    }
  }



  //----------------------------------------------------------------------
  // Calculate total FCal energies
  //----------------------------------------------------------------------
  //if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18) {
  {
    m_b_fcalA_et = 0;
    m_b_fcalC_et = 0;
    m_b_fcalA_et_Cos2 = 0; 
    m_b_fcalA_et_Sin2 = 0; 
    m_b_fcalC_et_Cos2 = 0; 
    m_b_fcalC_et_Sin2 = 0; 
    m_b_fcalA_et_Cos3 = 0; 
    m_b_fcalA_et_Sin3 = 0; 
    m_b_fcalC_et_Cos3 = 0; 
    m_b_fcalC_et_Sin3 = 0; 
    m_b_fcalA_et_Cos4 = 0; 
    m_b_fcalA_et_Sin4 = 0; 
    m_b_fcalC_et_Cos4 = 0; 
    m_b_fcalC_et_Sin4 = 0; 

    const xAOD::HIEventShapeContainer* hiueContainer = 0;  
    if (!evtStore ()->retrieve (hiueContainer, "HIEventShape").isSuccess ()) {
      Error ("GetHIEventShape ()", "Failed to retrieve HIEventShape container. Exiting." );
      return EL::StatusCode::FAILURE; 
    }

    for (const auto* hiue : *hiueContainer) {
      int layer = hiue->layer ();

      if (layer != 21 && layer != 22 && layer != 23)
        continue;

      double et = hiue->et ();
      double eta = hiue->etaMin ();
      const std::vector<float>& c1 = hiue->etCos ();
      const std::vector<float>& s1 = hiue->etSin ();

      if (eta > 0) {
        m_b_fcalA_et += et * 1e-3;
        m_b_fcalA_et_Cos2 += c1.at (1) * 1e-3;
        m_b_fcalA_et_Sin2 += s1.at (1) * 1e-3;
        m_b_fcalA_et_Cos3 += c1.at (2) * 1e-3;
        m_b_fcalA_et_Sin3 += s1.at (2) * 1e-3;
        m_b_fcalA_et_Cos4 += c1.at (3) * 1e-3;
        m_b_fcalA_et_Sin4 += s1.at (3) * 1e-3;
      }
      else {
        m_b_fcalC_et += et * 1e-3;
        m_b_fcalC_et_Cos2 += c1.at (1) * 1e-3;
        m_b_fcalC_et_Sin2 += s1.at (1) * 1e-3;
        m_b_fcalC_et_Cos3 += c1.at (2) * 1e-3;
        m_b_fcalC_et_Sin3 += s1.at (2) * 1e-3;
        m_b_fcalC_et_Cos4 += c1.at (3) * 1e-3;
        m_b_fcalC_et_Sin4 += s1.at (3) * 1e-3;
      }
    }
  }



  //----------------------------------------------------------------------
  // Calculate ZDC quantities
  //----------------------------------------------------------------------
  if ((m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) && m_dataType == Collisions) {
    // Zdc block with online/offline info
    // offline calibration from Peter's Zdc-dev-3
    // online L1 ZDC trigger information
    m_b_L1_ZDC_A     = false;
    m_b_L1_ZDC_A_tbp = false;
    m_b_L1_ZDC_A_tap = false;
    m_b_L1_ZDC_A_tav = false;
    m_b_L1_ZDC_A_prescale = 0.;

    m_b_L1_ZDC_C     = false;
    m_b_L1_ZDC_C_tbp = false;
    m_b_L1_ZDC_C_tap = false;
    m_b_L1_ZDC_C_tav = false;
    m_b_L1_ZDC_C_prescale = 0.;

    {
      std::string thisTrig = "L1_ZDC_A";
      const unsigned int bits = m_trigDecisionTool->isPassedBits (thisTrig);
      auto cg = m_trigDecisionTool->getChainGroup (thisTrig);
      m_b_L1_ZDC_A     = cg->isPassed();
      m_b_L1_ZDC_A_tbp = bits&TrigDefs::L1_isPassedBeforePrescale;
      m_b_L1_ZDC_A_tap = bits&TrigDefs::L1_isPassedAfterPrescale;
      m_b_L1_ZDC_A_tav = bits&TrigDefs::L1_isPassedAfterVeto;
      m_b_L1_ZDC_A_prescale =  cg->getPrescale ();
    }

    {
      std::string thisTrig = "L1_ZDC_C";
      const unsigned int bits = m_trigDecisionTool->isPassedBits (thisTrig);
      auto cg = m_trigDecisionTool->getChainGroup (thisTrig);
      m_b_L1_ZDC_C     = cg->isPassed (); 
      m_b_L1_ZDC_C_tbp = bits&TrigDefs::L1_isPassedBeforePrescale;
      m_b_L1_ZDC_C_tap = bits&TrigDefs::L1_isPassedAfterPrescale;
      m_b_L1_ZDC_C_tav = bits&TrigDefs::L1_isPassedAfterVeto;
      m_b_L1_ZDC_C_prescale =  cg->getPrescale ();
    } 

    {
      // offline calibrated ZDC energy
      m_b_ZdcCalibEnergy_A = 0;
      m_b_ZdcCalibEnergy_C = 0;

      // run ZDC Calibration
      ANA_CHECK (m_zdcAnalysisTool->reprocessZdc ());

      const xAOD::ZdcModuleContainer* m_zdcSums = 0;
      if (!evtStore ()->retrieve (m_zdcSums, "ZdcSums_RP").isSuccess ()) {
        Error ("GetZdcSums ()", "Failed to retrieve ZdcSums container. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      for (const auto zdcSum : *m_zdcSums) {
        float energy = zdcSum->auxdecor <float> ("CalibEnergy") ;
        //float energy = zdcSum->energy () ;
        int side = zdcSum->side ();
        int type = zdcSum->type ();
        if (type != 0) continue;
        if (side == +1) m_b_ZdcCalibEnergy_A += energy * 1e-3;
        if (side == -1) m_b_ZdcCalibEnergy_C += energy * 1e-3;
        //std::cout << " ZDC SIDE / TYPE / MOD = " << side << " / " << type << " / " << zdcSum->zdcModule() << ", amp = " << zdcSum->amplitude() << ", energy = " << energy << std::endl;
      }
    }
  }



  //----------------------------------------------------------------------
  // Get electron container
  //----------------------------------------------------------------------
  {
    m_b_electron_n = 0;
    std::vector<const xAOD::IParticle*> myParticles;

    std::string electronTrigName;
    if (m_collisionSystem == PbPb18)
      electronTrigName = "HLT_e15_lhloose_ion_L1EM12";
    else if (m_collisionSystem == PbPb15)
      electronTrigName = "HLT_e15_loose_ion_L1EM12";
    else // default is non-ion lhloose trigger
      electronTrigName = "HLT_e15_lhloose_L1EM12";

    const xAOD::ElectronContainer* electrons = 0;
    if (!evtStore ()->retrieve (electrons, "Electrons").isSuccess()) {
      Error ("GetElectrons ()", "Failed to retrieve Electrons collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto init_electron : *electrons) { 
      if (m_b_electron_n >= 40) {
        Info ("GetElectrons ()", "Tried to overflow electron arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      // create calibrated copy of electron
      xAOD::Electron* electron = nullptr;
      if (m_egammaPtEtaPhiECorrector->correctedCopy (*init_electron, electron) != CP::CorrectionCode::Ok) {
        ANA_MSG_INFO ("Cannot calibrate particle!");
      }

      // electron pT cut
      if (electron && electron->pt () * 1e-3 < m_electronPtCut) {
        delete electron;
        continue;
      }

      const float electron_pt_precalib = init_electron->pt () * 1e-3;
      const float electron_pt = electron->pt () * 1e-3;
      const float electron_eta = electron->caloCluster ()->etaBE (2);
      const float electron_phi = electron->phi ();
      

      // electron kinematic info 
      m_b_electron_pt_precalib[m_b_electron_n] = electron_pt_precalib;
      m_b_electron_pt[m_b_electron_n] = electron_pt;
      m_b_electron_eta[m_b_electron_n] = electron_eta;
      m_b_electron_phi[m_b_electron_n] = electron_phi;
      m_b_electron_charge[m_b_electron_n] = electron->charge ();

      // electron ID info
      m_b_electron_lhtight[m_b_electron_n] = m_electronLHTightSelectorTool->accept (electron);
      m_b_electron_lhmedium[m_b_electron_n] = m_electronLHMediumSelectorTool->accept (electron);
      m_b_electron_lhloose[m_b_electron_n] = m_electronLHLooseSelectorTool->accept (electron);
      m_b_electron_lhmedium_hi[m_b_electron_n] = m_electronLHMediumHISelectorTool->accept (electron);
      m_b_electron_lhloose_hi[m_b_electron_n] = m_electronLHLooseHISelectorTool->accept (electron);

      if (m_dataType == Collisions) {
        myParticles.clear ();
        myParticles.push_back ((const xAOD::IParticle*)electron);
        bool matched = m_trigMatchingTool->match (myParticles, electronTrigName, 0.07, false);
        m_b_electron_matched[m_b_electron_n] = matched;
      }

      // electron isolation info
      m_b_electron_etcone20[m_b_electron_n] = electron->auxdata<float> ("etcone20") * 1e-3;
      m_b_electron_etcone30[m_b_electron_n] = electron->auxdata<float> ("etcone30") * 1e-3;
      m_b_electron_etcone40[m_b_electron_n] = electron->auxdata<float> ("etcone40") * 1e-3;
      m_b_electron_topoetcone20[m_b_electron_n] = electron->auxdata<float> ("topoetcone20") * 1e-3;
      m_b_electron_topoetcone30[m_b_electron_n] = electron->auxdata<float> ("topoetcone30") * 1e-3;
      m_b_electron_topoetcone40[m_b_electron_n] = electron->auxdata<float> ("topoetcone40") * 1e-3;

      // electron track particle info
      const xAOD::TrackParticle* electronTrack = xAOD::EgammaHelpers::getOriginalTrackParticle (electron);
      m_b_electron_id_track_pt[m_b_electron_n] = electronTrack->pt () * 1e-3;
      m_b_electron_id_track_eta[m_b_electron_n] = electronTrack->eta ();
      m_b_electron_id_track_phi[m_b_electron_n] = electronTrack->phi ();
      m_b_electron_id_track_charge[m_b_electron_n] = electronTrack->charge ();
      m_b_electron_id_track_d0[m_b_electron_n] = electronTrack->d0 ();
      m_b_electron_id_track_d0sig[m_b_electron_n] = xAOD::TrackingHelpers::d0significance (electronTrack, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
      m_b_electron_id_track_z0[m_b_electron_n] = electronTrack->z0 ();
      try {
        m_b_electron_id_track_z0sig[m_b_electron_n] = xAOD::TrackingHelpers::z0significance (electronTrack);
      }
      catch (const std::runtime_error& error) {
        return EL::StatusCode::SUCCESS;
      }
      m_b_electron_id_track_theta[m_b_electron_n] = electronTrack->theta ();
      m_b_electron_id_track_vz[m_b_electron_n] = electronTrack->vz ();
      m_b_electron_id_track_tightprimary[m_b_electron_n] = m_trackSelectionToolTightPrimary->accept (*electronTrack, priVtx);
      m_b_electron_id_track_hiloose[m_b_electron_n] = m_trackSelectionToolHILoose->accept (*electronTrack, priVtx);
      m_b_electron_id_track_hitight[m_b_electron_n] = m_trackSelectionToolHITight->accept (*electronTrack, priVtx);

      // electron systematic info 
      float max_dpt = 0, max_deta = 0, max_dphi = 0;
      for (auto& systematic : electronSysList) {
        m_egammaPtEtaPhiECorrector->applySystematicVariation (systematic);

        xAOD::Electron* electronSysVar = nullptr;
        if (m_egammaPtEtaPhiECorrector->correctedCopy (*init_electron, electronSysVar)  != CP::CorrectionCode::Ok) {
          ANA_MSG_INFO ("Cannot calibrate particle!");
        }

        max_dpt = std::fmax (max_dpt, std::fabs (electronSysVar->pt () * 1e-3 - electron_pt));
        max_deta = std::fmax (max_deta, std::fabs (electronSysVar->eta () - electron_eta));
        max_dphi = std::fmax (max_dphi, std::fabs (electronSysVar->phi () - electron_phi));

        if (electronSysVar)
          delete electronSysVar;
      }

      m_b_electron_pt_sys[m_b_electron_n] = max_dpt;
      m_b_electron_eta_sys[m_b_electron_n] = max_deta;
      m_b_electron_phi_sys[m_b_electron_n] = max_dphi;

      m_b_electron_n++;
      if (electron)
        delete electron;
    } // end electron loop
  } // end electron scope



  //----------------------------------------------------------------------
  // Get egamma cluster container
  //----------------------------------------------------------------------
  {
    m_b_Cluster_n = 0;
    m_b_Cluster_pt.clear ();
    m_b_Cluster_et.clear ();
    m_b_Cluster_eta.clear ();
    m_b_Cluster_phi.clear ();

    m_b_Cluster_energyBE.clear ();
    m_b_Cluster_etaBE.clear ();
    m_b_Cluster_phiBE.clear ();
    m_b_Cluster_calE.clear ();
    m_b_Cluster_calEta.clear ();
    m_b_Cluster_calPhi.clear ();

    m_b_Cluster_size.clear ();
    m_b_Cluster_status.clear ();

    const xAOD::CaloClusterContainer* clusters = 0;
    ANA_CHECK (evtStore ()->retrieve (clusters, "egammaClusters"));
    for (auto cluster : *clusters) {
      if (cluster->pt()*1e-3 < 15)
        continue;

      m_b_Cluster_n++;
      m_b_Cluster_pt.push_back (cluster->pt ()*1e-3);
      m_b_Cluster_et.push_back (cluster->et ()*1e-3);
      m_b_Cluster_eta.push_back (cluster->eta ());
      m_b_Cluster_phi.push_back (cluster->phi ());

      m_b_Cluster_energyBE.push_back (cluster->energyBE (2)*1e-3);
      m_b_Cluster_etaBE.push_back (cluster->etaBE (2));
      m_b_Cluster_phiBE.push_back (cluster->phiBE (2));
      m_b_Cluster_calE.push_back (cluster->calE ()*1e-3);
      m_b_Cluster_calEta.push_back (cluster->calEta ());
      m_b_Cluster_calPhi.push_back (cluster->calPhi ());

      m_b_Cluster_size.push_back (cluster->clusterSize ());
      m_b_Cluster_status.push_back (cluster->signalState ());
    }
  }



  //----------------------------------------------------------------------
  // Get muon container
  //----------------------------------------------------------------------
  {
    m_b_muon_n = 0;
    m_b_muon_pt_sys.clear ();
    m_b_muon_eta_sys.clear ();
    m_b_muon_phi_sys.clear ();
    std::vector<const xAOD::IParticle*> myParticles;

    std::string muonTrigName;
    if (m_collisionSystem == PbPb15)
      muonTrigName = "HLT_mu14";
    else // default is non-ion lhloose trigger
      muonTrigName = "HLT_mu14";

    const xAOD::MuonContainer* muons = 0;
    if (!evtStore ()->retrieve (muons, "Muons").isSuccess()) {
      Error ("GetMuons ()", "Failed to retrieve Muons collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto init_muon : *muons) { 
      if (m_b_muon_n >= 40) {
        Info ("GetMuons ()", "Tried to overflow muon arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      xAOD::Muon* muon = nullptr;
      if (m_muonPtEtaPhiECorrector->correctedCopy (*init_muon, muon) != CP::CorrectionCode::Ok) {
        ANA_MSG_INFO ("Cannot calibrate particle!");
      }

      // muon pT cut
      if (muon && muon->pt () * 1e-3 < m_muonPtCut) {
        delete muon;
        continue;
      }

      const float muon_pt_precalib = init_muon->pt () * 1e-3;
      const float muon_pt = muon->pt () * 1e-3;
      const float muon_eta = muon->eta ();
      const float muon_phi = muon->phi ();

      // muon kinematic info 
      m_b_muon_pt_precalib[m_b_muon_n] = muon_pt_precalib;
      m_b_muon_pt[m_b_muon_n] = muon_pt;

      auto* muon_ms = muon->trackParticle (xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle);
      if (muon_ms) {
        m_b_muon_ms_pt_precalib[m_b_muon_n] = muon_ms->pt () * 1e-3;
        m_b_muon_ms_pt[m_b_muon_n] = muon->auxdata<float> ("MuonSpectrometerPt") * 1e-3;
      }
      else {
        m_b_muon_ms_pt_precalib[m_b_muon_n] = -999;
        m_b_muon_ms_pt[m_b_muon_n] = -999;
      }
      m_b_muon_eta[m_b_muon_n] = muon_eta;
      m_b_muon_phi[m_b_muon_n] = muon_phi;
      m_b_muon_charge[m_b_muon_n] = muon->charge ();

      // muon ID info
      m_b_muon_tight[m_b_muon_n] = m_muonTightSelectorTool->accept (muon);
      m_b_muon_medium[m_b_muon_n] = m_muonMediumSelectorTool->accept (muon);
      m_b_muon_loose[m_b_muon_n] = m_muonLooseSelectorTool->accept (muon);

      if (m_dataType == Collisions) {
        myParticles.clear ();
        myParticles.push_back ((const xAOD::IParticle*)muon);
        bool matched = m_trigMatchingTool->match (myParticles, muonTrigName, 0.1, false);
        m_b_muon_matched[m_b_muon_n] = matched;
      }

      // muon isolation info
      m_b_muon_etcone20[m_b_muon_n] = muon->auxdata<float> ("etcone20") * 1e-3;
      m_b_muon_etcone30[m_b_muon_n] = muon->auxdata<float> ("etcone30") * 1e-3;
      m_b_muon_etcone40[m_b_muon_n] = muon->auxdata<float> ("etcone40") * 1e-3;
      m_b_muon_topoetcone20[m_b_muon_n] = muon->auxdata<float> ("topoetcone20") * 1e-3;
      m_b_muon_topoetcone30[m_b_muon_n] = muon->auxdata<float> ("topoetcone30") * 1e-3;
      m_b_muon_topoetcone40[m_b_muon_n] = muon->auxdata<float> ("topoetcone40") * 1e-3;

      // muon inner detector track info
      const auto muon_id_track = muon->inDetTrackParticleLink ();
      if (muon_id_track.isValid ()) {
        m_b_muon_id_track_pt[m_b_muon_n] = (*muon_id_track)->pt () * 1e-3;
        m_b_muon_id_track_eta[m_b_muon_n] = (*muon_id_track)->eta ();
        m_b_muon_id_track_phi[m_b_muon_n] = (*muon_id_track)->phi ();
        m_b_muon_id_track_charge[m_b_muon_n] = (*muon_id_track)->charge ();
        m_b_muon_id_track_d0[m_b_muon_n] = (*muon_id_track)->d0 ();
        m_b_muon_id_track_d0sig[m_b_muon_n] = xAOD::TrackingHelpers::d0significance (*muon_id_track, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
        m_b_muon_id_track_z0[m_b_muon_n] = (*muon_id_track)->z0 ();
        try {
          m_b_muon_id_track_z0sig[m_b_muon_n] = xAOD::TrackingHelpers::z0significance (*muon_id_track);
        }
        catch (const std::runtime_error& error) {
          return EL::StatusCode::SUCCESS;
        }
        m_b_muon_id_track_theta[m_b_muon_n] = (*muon_id_track)->theta ();
        m_b_muon_id_track_vz[m_b_muon_n] = (*muon_id_track)->vz ();
        m_b_muon_id_track_tightprimary[m_b_muon_n] = m_trackSelectionToolTightPrimary->accept (**muon_id_track, priVtx);
        m_b_muon_id_track_hiloose[m_b_muon_n] = m_trackSelectionToolHILoose->accept (**muon_id_track, priVtx);
        m_b_muon_id_track_hitight[m_b_muon_n] = m_trackSelectionToolHITight->accept (**muon_id_track, priVtx);
      }
      else {
        m_b_muon_id_track_pt[m_b_muon_n] = -999;
        m_b_muon_id_track_eta[m_b_muon_n] = -999;
        m_b_muon_id_track_phi[m_b_muon_n] = -999;
        m_b_muon_id_track_charge[m_b_muon_n] = -999;
        m_b_muon_id_track_d0[m_b_muon_n] = -999;
        m_b_muon_id_track_d0sig[m_b_muon_n] = -999;
        m_b_muon_id_track_z0[m_b_muon_n] = -999;
        m_b_muon_id_track_z0sig[m_b_muon_n] = -999;
        m_b_muon_id_track_theta[m_b_muon_n] = -999;
        m_b_muon_id_track_vz[m_b_muon_n] = -999;
        m_b_muon_id_track_tightprimary[m_b_muon_n] = false;
        m_b_muon_id_track_hiloose[m_b_muon_n] = false;
        m_b_muon_id_track_hitight[m_b_muon_n] = false;
      }

      // muon spectrometer track info
      const auto muon_ms_track = muon->extrapolatedMuonSpectrometerTrackParticleLink ();
      if (muon_ms_track.isValid ()) {
        m_b_muon_ms_track_pt[m_b_muon_n] = (*muon_ms_track)->pt () * 1e-3;
        m_b_muon_ms_track_eta[m_b_muon_n] = (*muon_ms_track)->eta ();
        m_b_muon_ms_track_phi[m_b_muon_n] = (*muon_ms_track)->phi ();
        m_b_muon_ms_track_charge[m_b_muon_n] = (*muon_ms_track)->charge ();
        m_b_muon_ms_track_d0[m_b_muon_n] = (*muon_ms_track)->d0 ();
        m_b_muon_ms_track_d0sig[m_b_muon_n] = xAOD::TrackingHelpers::d0significance (*muon_ms_track, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
        m_b_muon_ms_track_z0[m_b_muon_n] = (*muon_ms_track)->z0 ();
        try {
          m_b_muon_ms_track_z0sig[m_b_muon_n] = xAOD::TrackingHelpers::z0significance (*muon_ms_track);
        }
        catch (const std::runtime_error& error) {
          return EL::StatusCode::SUCCESS;
        }
        m_b_muon_ms_track_vz[m_b_muon_n] = (*muon_ms_track)->vz ();
        m_b_muon_ms_track_theta[m_b_muon_n] = (*muon_ms_track)->theta ();
      }
      else {
        m_b_muon_ms_track_pt[m_b_muon_n] = -999;
        m_b_muon_ms_track_eta[m_b_muon_n] = -999;
        m_b_muon_ms_track_phi[m_b_muon_n] = -999;
        m_b_muon_ms_track_charge[m_b_muon_n] = -999;
        m_b_muon_ms_track_d0[m_b_muon_n] = -999;
        m_b_muon_ms_track_d0sig[m_b_muon_n] = -999;
        m_b_muon_ms_track_z0[m_b_muon_n] = -999;
        m_b_muon_ms_track_z0sig[m_b_muon_n] = -999;
        m_b_muon_ms_track_theta[m_b_muon_n] = -999;
        m_b_muon_ms_track_vz[m_b_muon_n] = -999;
      }

      //float max_dpt = 0, max_deta = 0, max_dphi = 0;
      m_b_muon_pt_sys.push_back (std::vector <double> (0));
      m_b_muon_eta_sys.push_back (std::vector <double> (0));
      m_b_muon_phi_sys.push_back (std::vector <double> (0));
      for (auto& systematic : muonSysList) {
        m_muonPtEtaPhiECorrector->applySystematicVariation (systematic);

        xAOD::Muon* muonSysVar = nullptr;
        if (m_muonPtEtaPhiECorrector->correctedCopy (*init_muon, muonSysVar)  != CP::CorrectionCode::Ok) {
          ANA_MSG_INFO ("Cannot calibrate particle!");
        }

        m_b_muon_pt_sys[m_b_muon_n].push_back (muonSysVar->pt () * 1e-3);
        m_b_muon_eta_sys[m_b_muon_n].push_back (muonSysVar->eta ());
        m_b_muon_phi_sys[m_b_muon_n].push_back (muonSysVar->phi ());

        //max_dpt = std::fmax (max_dpt, std::fabs (muonSysVar->pt () * 1e-3 - muon_pt));
        //max_deta = std::fmax (max_deta, std::fabs (muonSysVar->eta () - muon_eta));
        //max_dphi = std::fmax (max_dphi, std::fabs (muonSysVar->phi () - muon_phi));

        if (muonSysVar) delete muonSysVar;
      }

      //m_b_muon_pt_sys[m_b_muon_n] = max_dpt;
      //m_b_muon_eta_sys[m_b_muon_n] = max_deta;
      //m_b_muon_phi_sys[m_b_muon_n] = max_dphi;

      m_b_muon_n++;
      if (muon) delete muon;
    } // end muon loop
  } // end muon scope



  ////----------------------------------------------------------------------
  //// Implement rudimentary Z cuts to reduce impact on grid
  ////----------------------------------------------------------------------
  //{
  //  bool isZEvent = !m_isCollisions;
  //  TLorentzVector l1, l2;
  //  for (int iE1 = 0; iE1 < m_b_electron_n && !isZEvent; iE1++) {
  //    l1.SetPtEtaPhiM (m_b_electron_pt[iE1], m_b_electron_eta[iE1], m_b_electron_phi[iE1], 0.000511);
  //    for (int iE2 = 0; iE2 < iE1 && !isZEvent; iE2++) {
  //      l2.SetPtEtaPhiM (m_b_electron_pt[iE2], m_b_electron_eta[iE2], m_b_electron_phi[iE2], 0.000511);

  //      const float mZ = (l1+l2).M ();
  //      isZEvent = isZEvent || (40 < mZ && mZ < 140); // window around true Z mass
  //    }
  //  }
  //  for (int iM1 = 0; iM1 < m_b_muon_n && !isZEvent; iM1++) {
  //    l1.SetPtEtaPhiM (m_b_muon_pt[iM1], m_b_muon_eta[iM1], m_b_muon_phi[iM1], 0.105658);
  //    for (int iM2 = 0; iM2 < iM1 && !isZEvent; iM2++) {
  //      l2.SetPtEtaPhiM (m_b_muon_pt[iM2], m_b_muon_eta[iM2], m_b_muon_phi[iM2], 0.105658);

  //      const float mZ = (l1+l2).M ();
  //      isZEvent = isZEvent || (40 < mZ && mZ < 140); // window around true Z mass
  //    }
  //  }
  //  if (!isZEvent) {
  //    return EL::StatusCode::SUCCESS;
  //  }
  //} // end Z boson selection



  //----------------------------------------------------------------------
  // Get tracking info
  //----------------------------------------------------------------------
  std::vector<float> vec_trk_eta;
  vec_trk_eta.clear ();
  {
    m_b_ntrk = 0;

    const xAOD::TrackParticleContainer* trackContainer = 0;
    if (!evtStore ()->retrieve (trackContainer, "InDetTrackParticles").isSuccess ()) {
      Error ("GetInDetTrackParticles()", "Failed to retrieve InDetTrackParticles container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    std::pair<unsigned int, unsigned int> res;

    for (const auto* track : *trackContainer) {
      if (m_b_ntrk >= 10000) {
        Error ("GetInDetTrackParticles ()", "Tried to overflow track arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      const bool passHITight = m_trackSelectionToolHITight->accept (*track, priVtx);
      const bool passHILoose = m_trackSelectionToolHILoose->accept (*track, priVtx);
      const bool passTightPrimary = m_trackSelectionToolTightPrimary->accept (*track, priVtx);

      if (track->pt () * 1e-3 > 0.4 && passHILoose)
        vec_trk_eta.push_back (track->eta ());

      if (track->pt () * 1e-3 < m_trkPtCut)
        continue;

      //if (!passHITight && !passHILoose)
      //  continue;

      m_b_trk_pt[m_b_ntrk] = track->pt () * 1e-3;
      m_b_trk_eta[m_b_ntrk] = track->eta ();
      m_b_trk_phi[m_b_ntrk] = track->phi ();
      m_b_trk_charge[m_b_ntrk] = track->charge ();

      m_b_trk_HItight[m_b_ntrk] = passHITight;
      m_b_trk_HIloose[m_b_ntrk] = passHILoose;
      m_b_trk_TightPrimary[m_b_ntrk] = passTightPrimary;

      m_b_trk_d0[m_b_ntrk] = track->d0 ();
      m_b_trk_d0sig[m_b_ntrk] = xAOD::TrackingHelpers::d0significance (track, eventInfo->beamPosSigmaX (), eventInfo->beamPosSigmaY (), eventInfo->beamPosSigmaXY());
      m_b_trk_z0[m_b_ntrk] = track->z0 ();
      try {
        m_b_trk_z0sig[m_b_ntrk] = xAOD::TrackingHelpers::z0significance (track);
      }
      catch (const std::runtime_error& error) {
        return EL::StatusCode::SUCCESS;
      }
      m_b_trk_theta[m_b_ntrk] = track->theta ();
      m_b_trk_vz[m_b_ntrk] = track->vz ();

      m_b_trk_nBLayerHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfBLayerHits);
      m_b_trk_nBLayerSharedHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfBLayerSharedHits);
      m_b_trk_nPixelHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfPixelHits);
      m_b_trk_nPixelDeadSensors[m_b_ntrk] = getSum (*track, xAOD :: numberOfPixelDeadSensors);
      m_b_trk_nPixelSharedHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfPixelSharedHits);
      m_b_trk_nSCTHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfSCTHits);
      m_b_trk_nSCTDeadSensors[m_b_ntrk] = getSum (*track, xAOD :: numberOfSCTDeadSensors);
      m_b_trk_nSCTSharedHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfSCTSharedHits);
      m_b_trk_nTRTHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfTRTHits);
      m_b_trk_nTRTSharedHits[m_b_ntrk] = getSum (*track, xAOD :: numberOfTRTSharedHits);
    

      if (m_dataType != Collisions) {
        res = m_mcTruthClassifier->particleTruthClassifier (track);
        //m_b_trk_prob_truth[m_b_ntrk] = m_mcTruthClassifier->getProbTrktoTruth ());

        if (track->isAvailable <ElementLink <xAOD::TruthParticleContainer>> ("truthParticleLink")) {
          ElementLink <xAOD::TruthParticleContainer> link = track->auxdata <ElementLink<xAOD::TruthParticleContainer>> ("truthParticleLink");

          if (link.isValid ()) {
            m_b_trk_prob_truth[m_b_ntrk] = m_mcTruthClassifier->getProbTrktoTruth ();

            const xAOD::TruthParticle* thePart = xAOD::TruthHelpers::getTruthParticle (*(track));

            if (thePart) {
              m_b_trk_truth_pt[m_b_ntrk] = thePart->pt () * 1e-3;
              m_b_trk_truth_eta[m_b_ntrk] = thePart->eta ();
              m_b_trk_truth_phi[m_b_ntrk] = thePart->phi ();
              m_b_trk_truth_charge[m_b_ntrk] = thePart->charge ();

              res = m_mcTruthClassifier->particleTruthClassifier (track);
              m_b_trk_truth_type[m_b_ntrk] = res.first;
              m_b_trk_truth_orig[m_b_ntrk] = res.second;
              m_b_trk_truth_pdgid[m_b_ntrk] = thePart->pdgId ();
              m_b_trk_truth_barcode[m_b_ntrk] = thePart->barcode ();

              if (thePart->hasProdVtx ()) {
                const xAOD::TruthVertex* vtx = thePart->prodVtx ();
                m_b_trk_truth_vz[m_b_ntrk] = vtx->z ();
                m_b_trk_truth_nIn[m_b_ntrk] = vtx->nIncomingParticles ();
              }
              else {
                m_b_trk_truth_vz[m_b_ntrk] = -999;
                m_b_trk_truth_nIn[m_b_ntrk] = -999;
              }
              m_b_trk_truth_isHadron[m_b_ntrk] = thePart->isHadron ();

            }

            else {
              m_b_trk_truth_pt[m_b_ntrk] = -999;
              m_b_trk_truth_eta[m_b_ntrk] = -999;
              m_b_trk_truth_phi[m_b_ntrk] = -999;
              m_b_trk_truth_charge[m_b_ntrk] = -999;

              m_b_trk_truth_type[m_b_ntrk] = -999;
              m_b_trk_truth_orig[m_b_ntrk] = -999;
              m_b_trk_truth_pdgid[m_b_ntrk] = -999;
              m_b_trk_truth_barcode[m_b_ntrk] = -999;

              m_b_trk_truth_vz[m_b_ntrk] = -999;
              m_b_trk_truth_nIn[m_b_ntrk] = -999;
              m_b_trk_truth_isHadron[m_b_ntrk] = false;
            }
          }

          else {
            m_b_trk_prob_truth[m_b_ntrk] = 0;

            m_b_trk_truth_pt[m_b_ntrk] = -999;
            m_b_trk_truth_eta[m_b_ntrk] = -999;
            m_b_trk_truth_phi[m_b_ntrk] = -999;
            m_b_trk_truth_charge[m_b_ntrk] = -999;

            m_b_trk_truth_type[m_b_ntrk] = -999;
            m_b_trk_truth_orig[m_b_ntrk] = -999;
            m_b_trk_truth_pdgid[m_b_ntrk] = -999;
            m_b_trk_truth_barcode[m_b_ntrk] = -999;

            m_b_trk_truth_vz[m_b_ntrk] = -999;
            m_b_trk_truth_nIn[m_b_ntrk] = -999;
            m_b_trk_truth_isHadron[m_b_ntrk] = false;
          }
          
        } // end if TruthParticleLink is available (

      } // end if not collisions

      m_b_ntrk++;
    } // end tracks loop
  } // end tracks scope


  //----------------------------------------------------------------------
  // Gets gap information for PbPb collisions (for UPC bkg. est.)
  //----------------------------------------------------------------------
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18) {
    // Gap information
    const xAOD::CaloClusterContainer* caloClusters = 0;
    ANA_CHECK (evtStore ()->retrieve (caloClusters, "CaloCalTopoClusters"));
    std::vector<float> vec_cl_eta;
    vec_cl_eta.clear ();

    for (const auto cluster : *caloClusters) {
      if (cluster->pt () < 200.0) continue;
      //if (!passSinfigCut (cluster->eta(), cluster->auxdata< float >("CELL_SIG_SAMPLING"), cluster->auxdata<float>("CELL_SIGNIFICANCE"))) continue;
      vec_cl_eta.push_back (cluster->eta());
    }

    m_b_sum_gap_A  = -1;
    m_b_sum_gap_C  = -1;
    m_b_edge_gap_A = -1;
    m_b_edge_gap_C = -1;
    GapCut (vec_trk_eta, vec_cl_eta);

    m_b_clusterOnly_sum_gap_A  = -1;
    m_b_clusterOnly_sum_gap_C  = -1;
    m_b_clusterOnly_edge_gap_A = -1;
    m_b_clusterOnly_edge_gap_C = -1;
    ClusterGapCut (vec_cl_eta);

    vec_cl_eta.clear ();
    vec_trk_eta.clear ();
  }




  //----------------------------------------------------------------------
  // Get EMTopo jets in pp collisions
  //----------------------------------------------------------------------
  if (m_collisionSystem == pp17) {
    m_b_akt4emtopo_jet_n = 0;

    const xAOD::JetContainer* jetContainer = 0;
    if (!evtStore ()->retrieve (jetContainer, "AntiKt4EMTopoJets").isSuccess ()) {
      Error ("GetAntiKt4EMTopoJets ()", "Failed to retrieve AntiKt4EMTopoJets collection. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    for (const auto* jet : *jetContainer) {
      if (m_b_akt4emtopo_jet_n >= 100) {
        Info ("GetAntiKt4EMTopoJets ()", "Tried to overflow jets arrays, continuing.");
        continue;
      }

      const xAOD::JetFourMom_t jet_4mom = jet->jetP4 ();
      const float jet_pt = 1e-3 * jet_4mom.pt ();
      const float jet_eta = jet_4mom.eta ();
      const float jet_phi = jet_4mom.phi ();
      const float jet_e = 1e-3 * jet_4mom.e ();

      if (jet_pt < m_jetPtCut)
        continue;

      m_b_akt4emtopo_jet_pt[m_b_akt4emtopo_jet_n] = jet_pt;
      m_b_akt4emtopo_jet_eta[m_b_akt4emtopo_jet_n] = jet_eta;
      m_b_akt4emtopo_jet_phi[m_b_akt4emtopo_jet_n] = jet_phi;
      m_b_akt4emtopo_jet_e[m_b_akt4emtopo_jet_n] = jet_e;
      m_b_akt4emtopo_jet_n++;
    }
  } // end jet scope




  //----------------------------------------------------------------------
  // Get HI jets in pp collisions
  //----------------------------------------------------------------------
  {
    m_b_akt4hi_jet_n = 0;

    const xAOD::JetContainer* jetContainer = 0;
    if (!evtStore ()->retrieve (jetContainer, "AntiKt4HIJets").isSuccess ()) {
      Error ("GetAntiKt4HIJets ()", "Failed to retrieve AntiKt4HIJets collection. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    for (const auto* jet : *jetContainer) {
      if (m_b_akt4hi_jet_n >= 100) {
        Info ("GetAntiKt4HIJets ()", "Tried to overflow jets arrays, continuing.");
        continue;
      }

      //xAOD::Jet* jet = nullptr;
      //m_Akt4HI_EM_EtaJES_CalibTool->calibratedCopy (*init_jet, jet);
      //if (jet && jet->jetP4 ().pt () * 1e-3 < m_jetPtCut) {
      //  delete jet;
      //  continue;
      //}

      const xAOD::JetFourMom_t jet_4mom = jet->jetP4 ();
      const float jet_pt = 1e-3 * jet_4mom.pt ();
      const float jet_eta = jet_4mom.eta ();
      const float jet_phi = jet_4mom.phi ();
      const float jet_e = 1e-3 * jet_4mom.e ();

      m_b_akt4hi_jet_pt[m_b_akt4hi_jet_n] = jet_pt;
      m_b_akt4hi_jet_eta[m_b_akt4hi_jet_n] = jet_eta;
      m_b_akt4hi_jet_phi[m_b_akt4hi_jet_n] = jet_phi;
      m_b_akt4hi_jet_e[m_b_akt4hi_jet_n] = jet_e;
      m_b_akt4hi_jet_n++;

      //if (jet)
      //  delete jet;
    }
  } // end jet scope



  //----------------------------------------------------------------------
  // Get truth info if MC 
  //----------------------------------------------------------------------
  if (m_dataType != Collisions) {
    m_b_truth_electron_n = 0;
    m_b_truth_muon_n = 0;
    m_b_truth_trk_n = 0;

    const xAOD::TruthParticleContainer* truthParticleContainer = 0;
    if (!evtStore ()->retrieve (truthParticleContainer, "TruthParticles").isSuccess ()) {
      Error ("GetTruthParticles()", "Failed to retrieve TruthParticles collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    for (const auto* truthParticle : *truthParticleContainer) {
    
      if (truthParticle->status() != 1)
        continue; // if not final state continue
      if (truthParticle->pt() * 1e-3 < m_truthTrkPtCut)
        continue; // pT cut
      if (!truthParticle->isCharged ())
        continue; // require charged particles
      if (fabs (truthParticle->eta()) > 3)
        continue; // require particles inside tracker
      if (truthParticle->absPdgId () == 12 || truthParticle->absPdgId () == 14 || truthParticle->absPdgId () == 16)
        continue; // don't count neutrinos
      //if (truthParticle->barcode() >= 2e5 || truthParticle->barcode() == 0)
      //  continue;

      const int pdgId = truthParticle->pdgId ();

      if (pdgId == 11 || pdgId == -11) { // if electron or anti-electron
        if (truthParticle->pt () * 1e-3 < m_electronPtCut)
          continue;
        if (m_b_truth_electron_n >= 1000) {
          Error ("GetTruthElectrons ()", "Tried to overflow truth electron arrays. Exiting.");
          return EL::StatusCode::FAILURE;
        }
        m_b_truth_electron_pt[m_b_truth_electron_n] = truthParticle->pt () * 1e-3;
        m_b_truth_electron_eta[m_b_truth_electron_n] = truthParticle->eta ();
        m_b_truth_electron_phi[m_b_truth_electron_n] = truthParticle->phi ();
        m_b_truth_electron_charge[m_b_truth_electron_n] = truthParticle->charge ();
        m_b_truth_electron_barcode[m_b_truth_electron_n] = truthParticle->barcode ();
        m_b_truth_electron_n++;
      }

      else if (pdgId == 13 || pdgId == -13) { // if muon or anti-muon
        if (truthParticle->pt () * 1e-3 < m_muonPtCut)
          continue;
        if (m_b_truth_muon_n >= 1000) {
          Error ("GetTruthMuons ()", "Tried to overflow truth muon arrays. Exiting.");
          return EL::StatusCode::FAILURE;
        }
        m_b_truth_muon_pt[m_b_truth_muon_n] = truthParticle->pt () * 1e-3;
        m_b_truth_muon_eta[m_b_truth_muon_n] = truthParticle->eta ();
        m_b_truth_muon_phi[m_b_truth_muon_n] = truthParticle->phi ();
        m_b_truth_muon_charge[m_b_truth_muon_n] = truthParticle->charge ();
        m_b_truth_muon_barcode[m_b_truth_muon_n] = truthParticle->barcode ();
        m_b_truth_muon_n++;
      }

      else { // otherwise it must be some charged hadron
        if (m_b_truth_trk_n >= 10000) {
          Error ("GetTruthTracks ()", "Tried to overflow truth track arrays. Exiting.");
          return EL::StatusCode::FAILURE;
        }
        m_b_truth_trk_pt[m_b_truth_trk_n] = truthParticle->pt () * 1e-3;
        m_b_truth_trk_eta[m_b_truth_trk_n] = truthParticle->eta ();
        m_b_truth_trk_phi[m_b_truth_trk_n] = truthParticle->phi ();
        m_b_truth_trk_charge[m_b_truth_trk_n] = truthParticle->charge ();
        m_b_truth_trk_pdgid[m_b_truth_trk_n] = truthParticle->pdgId ();
        m_b_truth_trk_barcode[m_b_truth_trk_n] = truthParticle->barcode ();
        m_b_truth_trk_isHadron[m_b_truth_trk_n] = truthParticle->isHadron ();
        m_b_truth_trk_n++;
      }
    } // end truth particles loop


    m_b_akt4_truth_jet_n = 0;

    const xAOD::JetContainer* truthJetContainer = 0;
    if (!evtStore ()->retrieve(truthJetContainer, "AntiKt4TruthJets").isSuccess()) {
      Error("GetAntiKt4TruthJets()", "Failed to retrieve AntiKt4TruthJets collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }
 
    for (const auto* truthJet : *truthJetContainer) {
      if (truthJet->pt() * 1e-3 < m_jetPtCut)
        continue;
      if (m_b_akt4_truth_jet_n >= 100) {
        Info ("GetTruthJets ()", "Tried to overflow truth jet arrays, continuing.");
        continue;
      }
      m_b_akt4_truth_jet_pt[m_b_akt4_truth_jet_n] = truthJet->pt() * 1e-3;
      m_b_akt4_truth_jet_eta[m_b_akt4_truth_jet_n] = truthJet->eta();
      m_b_akt4_truth_jet_phi[m_b_akt4_truth_jet_n] = truthJet->phi();
      m_b_akt4_truth_jet_e[m_b_akt4_truth_jet_n] = truthJet->e() * 1e-3;
      m_b_akt4_truth_jet_n++;
    } // end truth jet loop

  } // end truth info scope


  m_tree->Fill();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: postExecute () {
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: finalize () {
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  if (m_dataType == Collisions || m_dataType == MCDataOverlay) {
    if (m_pp17_grl) {
      delete m_pp17_grl;
      m_pp17_grl = nullptr;
    }
    if (m_PbPb15_grl) {
      delete m_PbPb15_grl;
      m_PbPb15_grl = nullptr;
    }
    if (m_PbPb18_grl) {
      delete m_PbPb18_grl;
      m_PbPb18_grl = nullptr;
    }
    if (m_PbPb18_ignoreToroid_grl) {
      delete m_PbPb18_ignoreToroid_grl;
      m_PbPb18_ignoreToroid_grl = nullptr;
    }
  }


  if (m_dataType == Collisions) {
    if (m_trigConfigTool) {
      delete m_trigConfigTool;
      m_trigConfigTool = nullptr;
    }
    if (m_trigDecisionTool) {
      delete m_trigDecisionTool;
      m_trigDecisionTool = nullptr;
    }
    if (m_trigMatchingTool) {
      delete m_trigMatchingTool;
      m_trigMatchingTool = nullptr;
    }
  }


  if ((m_collisionSystem == PbPb18 || m_collisionSystem == PbPb15) && m_dataType == Collisions) {
    delete m_zdcAnalysisTool;
    m_zdcAnalysisTool = nullptr;
  }


  if (m_egammaPtEtaPhiECorrector) {
    delete m_egammaPtEtaPhiECorrector;
    m_egammaPtEtaPhiECorrector = nullptr;
  }

  if (m_electronLHTightSelectorTool) {
    delete m_electronLHTightSelectorTool;
    m_electronLHTightSelectorTool = nullptr;
  }
  if (m_electronLHMediumSelectorTool) {
    delete m_electronLHMediumSelectorTool;
    m_electronLHMediumSelectorTool = nullptr;
  }
  if (m_electronLHLooseSelectorTool) {
    delete m_electronLHLooseSelectorTool;
    m_electronLHLooseSelectorTool = nullptr;
  }
  if (m_electronLHMediumHISelectorTool) {
    delete m_electronLHMediumHISelectorTool;
    m_electronLHMediumHISelectorTool = nullptr;
  }
  if (m_electronLHLooseHISelectorTool) {
    delete m_electronLHLooseHISelectorTool;
    m_electronLHLooseHISelectorTool = nullptr;
  }


  if (m_muonPtEtaPhiECorrector) {
    delete m_muonPtEtaPhiECorrector;
    m_muonPtEtaPhiECorrector = nullptr;
  }

  if (m_muonTightSelectorTool) {
    delete m_muonTightSelectorTool;
    m_muonTightSelectorTool = nullptr;
  }
  if (m_muonMediumSelectorTool) {
    delete m_muonMediumSelectorTool;
    m_muonMediumSelectorTool = nullptr;
  }
  if (m_muonLooseSelectorTool) {
    delete m_muonLooseSelectorTool;
    m_muonLooseSelectorTool = nullptr;
  }


  //if (m_Akt4HI_EM_EtaJES_CalibTool) {
  //  delete m_Akt4HI_EM_EtaJES_CalibTool;
  //  m_Akt4HI_EM_EtaJES_CalibTool = nullptr;
  //}


  if (m_trackSelectionToolTightPrimary) {
    delete m_trackSelectionToolTightPrimary;
    m_trackSelectionToolTightPrimary = nullptr;
  }
  //if (m_trackSelectionToolMinbias) {
  //  delete m_trackSelectionToolMinbias;
  //  m_trackSelectionToolMinbias = nullptr;
  //}
  if (m_trackSelectionToolHITight) {
    delete m_trackSelectionToolHITight;
    m_trackSelectionToolHITight = nullptr;
  }
  if (m_trackSelectionToolHILoose) {
    delete m_trackSelectionToolHILoose;
    m_trackSelectionToolHILoose = nullptr;
  }


  if (m_dataType != Collisions) {
    delete m_mcTruthClassifier;
    m_mcTruthClassifier = nullptr;
  }


  if (m_oop_hMean) {
    delete m_oop_hMean;
    m_oop_hMean = nullptr;
  }
  if (m_oop_hSigma) {
    delete m_oop_hSigma;
    m_oop_hSigma = nullptr;
  }
  

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeMaker :: histFinalize () {
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}



/////////////////////////////////////////////////////////////////////
// 2018 tracker misalignment
/////////////////////////////////////////////////////////////////////
bool TreeMaker :: isDesynEvent (const int _runNumber, const int _lumiBlock) {
  switch (_runNumber) {
    case 366627: { if (_lumiBlock >= 139  && _lumiBlock <= 250) return true; return false; }
    case 366691: { if (_lumiBlock >= 236  && _lumiBlock <= 360) return true; return false; }
    case 366754: { if (_lumiBlock >= 208  && _lumiBlock <= 340) return true; return false; }
    case 366805: { if (_lumiBlock >= 81   && _lumiBlock <= 200) return true; return false; }
    case 366860: { if (_lumiBlock >= 147  && _lumiBlock <= 210) return true; return false; }
    case 366878: { if (_lumiBlock >= 87   && _lumiBlock <= 160) return true; return false; }
    case 366919: { if (_lumiBlock >= 110  && _lumiBlock <= 200) return true; return false; }
    case 366931: { if (_lumiBlock >= 139  && _lumiBlock <= 200) return true; return false; }
    case 367023: { if (_lumiBlock >= 140  && _lumiBlock <= 220) return true; return false; }
    case 367099: { if (_lumiBlock >= 200  && _lumiBlock <= 260) return true; return false; }
    case 367134: { if (_lumiBlock >= 140  && _lumiBlock <= 170) return true; return false; }
    case 367233: { if (_lumiBlock >= 180  && _lumiBlock <= 250) return true; return false; }
    default: return false;
  }
}



/////////////////////////////////////////////////////////////////////
// 2018 out-of-time pile-up removal
/////////////////////////////////////////////////////////////////////
bool TreeMaker :: is_Outpileup(const xAOD::HIEventShapeContainer& evShCont, const int nTrack) {

  if (nTrack > 3000) // The selection is only for [0, 3000]
    return 0;
  
  float Fcal_Et = 0.0;
  float Tot_Et = 0.0;
  float oop_Et = 0.0;
  Fcal_Et = evShCont.at(5)->et()*1e-6;
  Tot_Et = evShCont.at(0)->et()*1e-6;
  oop_Et = Tot_Et - Fcal_Et;// Barrel + Endcap calo
  
  int nBin{m_oop_hMean->GetXaxis()->FindFixBin(nTrack)};
  double mean{m_oop_hMean->GetBinContent(nBin)};
  double sigma{m_oop_hSigma->GetBinContent(nBin)};

  ANA_MSG_DEBUG (" oop_Et = " << oop_Et << "TeV"  );
  
  if (m_nside == 1) // one side cut
    if (oop_Et - mean > -4 * sigma) // 4 sigma cut
      return 0; 

  if (m_nside == 2) // two side cut
    if (abs(oop_Et - mean) < 4 * sigma) // 4 sigma cut
      return 0;

  return 1;
}



/////////////////////////////////////////////////////////////////////
// Helper function for gap cuts
/////////////////////////////////////////////////////////////////////
bool TreeMaker :: passSinfigCut (float eta, int cellsigsamp, float cellsig) {
  bool Use_cluster = false;
  float sig_cut = CellSigCut (eta);
  //Check if cell sig is above threshold
  if (cellsig > sig_cut) Use_cluster = 1;
  (void) cellsigsamp;
  //Tile cut off!!!!!!!!!
  //Check if significant cell is in tile calorimeter
  //if (cellsigsamp < 21 && cellsigsamp > 11)
  //Use_cluster = 0;
  if (fabs (eta) > 4.9) Use_cluster = 0;
  return Use_cluster;
}



/////////////////////////////////////////////////////////////////////
// Helper function for gap cuts
/////////////////////////////////////////////////////////////////////
float TreeMaker :: CellSigCut (float x) {
  float eta[101] = {-5, -4.9, -4.8, -4.7, -4.6, -4.5, -4.4, -4.3, -4.2, -4.1, -4, -3.9, -3.8, -3.7, -3.6, -3.5, -3.4, -3.3, -3.2, -3.1, -3, -2.9, -2.8, -2.7, -2.6, -2.5, -2.4, -2.3, -2.2, -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.5, -1.4, -1.3, -1.2, -1.1, -1, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9};
  float sig[101] = {0, 4.7426, 5.11018, 5.07498, 5.0969, 5.10695, 5.04098, 5.07106, 4.98087, 5.11647, 5.08988, 5.16267, 5.17202, 5.23803, 5.25314, 5.29551, 5.35092, 5.40863, 5.44375, 5.38075, 5.25022, 5.37933, 5.25459, 5.37719, 5.25169, 5.73985, 5.79174, 5.79266, 5.79588, 5.7963, 5.81949, 5.82273, 5.85658, 5.85442, 5.84779, 5.77679, 5.83323, 5.84524, 5.84439, 5.84488, 5.84744, 5.84683, 5.84524, 5.84594, 5.84656, 5.84639, 5.84461, 5.84515, 5.84206, 5.8396, 5.84497, 5.84801, 5.84608, 5.84608, 5.84783, 5.84726, 5.84844, 5.8477, 5.84796, 5.84757, 5.84822, 5.84814, 5.84617, 5.83451, 5.77658, 5.84309, 5.85496, 5.85761, 5.82555, 5.82206, 5.78982, 5.78482, 5.7778, 5.78327, 5.74898, 5.25459, 5.37503, 5.25459, 5.37283, 5.25169, 5.37862, 5.44473, 5.41041, 5.34498, 5.29551, 5.25602, 5.2283, 5.17428, 5.14504, 5.09342, 5.12256, 4.98721, 5.07106, 5.02642, 5.10031, 5.11018, 5.05447, 5.10031, 4.7426, 0};
  float sig_cut = 0;
  for (int i = 0; i < 100; i++) {
    if (x < eta[i]) {
      sig_cut = sig[i];
      break;
    }
  }
  return sig_cut;
}



/////////////////////////////////////////////////////////////////////
// Calculate gaps using only clusters
/////////////////////////////////////////////////////////////////////
void TreeMaker :: ClusterGapCut (std::vector <float>& clus) {
  //Define a c++ Set for organiZed list eta vals fro gap calc
  //C++ Set automatically organizes inserted vals
  //Intialized with with tracks and then clusters are added.
  std::set<float> eta_vals (clus.begin (), clus.end ());

  //add goast particles to define edge of detector
  eta_vals.insert (4.9);
  eta_vals.insert (-4.9);
  //----------------------------------------------------
  //Edge gap calculations
  // A side eta > 0 
  // C side eta < 0 
  float edge_gap_A = 0;
  float edge_gap_C = 0;

  //Edge gap calc (gap between detector edge and closest particle)
  std::set<float>::iterator neg_itr1 = ++eta_vals.begin ();
  std::set<float>::iterator neg_itr2 = eta_vals.begin ();
  edge_gap_C = *neg_itr1 - *neg_itr2;

  std::set<float>::iterator pos_itr1 = ----eta_vals.end ();
  std::set<float>::iterator pos_itr2 = --eta_vals.end ();
  edge_gap_A = *pos_itr2 - *pos_itr1;

  /*  
  int MedianEta = round(eta_vals.size()/2);
  for (set<float>::iterator itr1 = ++eta_vals.begin(), itr2 = eta_vals.begin(); itr1 != next(eta_vals.begin(),MedianEta); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_a+=delta_eta;
  }

  for ( set<float>::iterator itr1= next(eta_vals.begin(),MedianEta), itr2 = next(eta_vals.begin(),MedianEta-1); itr1 != eta_vals.end(); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_p+=delta_eta;
  }
  */

  //insert goast particle at eta=0.
  //This breaks up the gaps makeing the max gap=4.9
  //This also splits the detector into two separate sides
  //the photon side and the nucleus side
  //this causes a pile up effect in gap quantities at 4.9
  //because this is a synthetic particle.
  eta_vals.insert (0);

  //Sum of gaps to zero rapidity for both sides using gap cut
  float sum_gap_A = 0;
  float sum_gap_C = 0;
  float eta_cut = 0.5;

  for (std::set<float>::iterator itr1 = ++eta_vals.begin (), itr2 = eta_vals.begin (); itr1!=eta_vals.end (); itr1++, itr2++) {
    float delta_eta = *itr1 - *itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (*itr2 < 0) {
      if (delta_eta > eta_cut) sum_gap_C += delta_eta;
    }
    //define the photon side of the detector
    if (*itr1 > 0) {
      if (delta_eta > eta_cut) sum_gap_A += delta_eta;
    }
  }

  //Store gap info
  m_b_clusterOnly_sum_gap_A  = sum_gap_A;
  m_b_clusterOnly_sum_gap_C  = sum_gap_C;
  m_b_clusterOnly_edge_gap_A = edge_gap_A;
  m_b_clusterOnly_edge_gap_C = edge_gap_C;

  eta_vals.erase (0.0);//erase goast particle
}



/////////////////////////////////////////////////////////////////////
// Calculate gaps using only clusters
/////////////////////////////////////////////////////////////////////
void TreeMaker :: GapCut (std::vector<float>& trks, std::vector<float>& clus) {
  //Define a c++ Set for organiZed list eta vals fro gap calc
  //C++ Set automatically organizes inserted vals
  //Intialized with with tracks and then clusters are added.
  std::set<float> eta_vals (clus.begin (), clus.end ());

  //add goast particles to define edge of detector
  eta_vals.insert (4.9);
  eta_vals.insert (-4.9);

  std::vector<float>::iterator itrE = trks.begin ();
  for(; itrE!=trks.end (); itrE++) {
    eta_vals.insert (*itrE);
  }

  //----------------------------------------------------
  //Edge gap calculations
  // A side eta > 0 
  // C side eta < 0 
  float edge_gap_A = 0;
  float edge_gap_C = 0;

  //Edge gap calc (gap between detector edge and closest particle)
  std::set<float>::iterator neg_itr1 = ++eta_vals.begin ();
  std::set<float>::iterator neg_itr2 = eta_vals.begin ();
  edge_gap_C = *neg_itr1 - *neg_itr2;

  std::set<float>::iterator pos_itr1 = ----eta_vals.end ();
  std::set<float>::iterator pos_itr2 = --eta_vals.end ();
  edge_gap_A = *pos_itr2 - *pos_itr1;
  /*  
  int MedianEta = round(eta_vals.size()/2);
  for (set<float>::iterator itr1 = ++eta_vals.begin(), itr2 = eta_vals.begin(); itr1 != next(eta_vals.begin(),MedianEta); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_a+=delta_eta;
  }

  for ( set<float>::iterator itr1= next(eta_vals.begin(),MedianEta), itr2 = next(eta_vals.begin(),MedianEta-1); itr1 != eta_vals.end(); itr1++,itr2++) {
    float delta_eta=*itr1-*itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (delta_eta > eta_cut) sum_gap_p+=delta_eta;
  }
  */

  //insert goast particle at eta=0.
  //This breaks up the gaps makeing the max gap=4.9
  //This also splits the detector into two separate sides
  //the photon side and the nucleus side
  //this causes a pile up effect in gap quantities at 4.9
  //because this is a synthetic particle.
  eta_vals.insert (0);

  //Sum of gaps to zero rapidity for both sides using gap cut
  float sum_gap_A = 0;
  float sum_gap_C = 0;
  float eta_cut = 0.5;

  for (std::set<float>::iterator itr1=++eta_vals.begin (), itr2=eta_vals.begin (); itr1!=eta_vals.end (); itr1++, itr2++) {
    float delta_eta = *itr1 - *itr2; //calculate gaop size
    //define the atomic side gap as this part of detector
    if (*itr2 < 0) {
      if (delta_eta > eta_cut) sum_gap_C += delta_eta;
    }
    //define the photon side of the detector
    if (*itr1 > 0) {
      if (delta_eta > eta_cut) sum_gap_A += delta_eta;
    }
  }

  //Store gap info
  m_b_sum_gap_A  = sum_gap_A;
  m_b_sum_gap_C  = sum_gap_C;
  m_b_edge_gap_A = edge_gap_A;
  m_b_edge_gap_C = edge_gap_C;

  eta_vals.erase (0.0);//erase goast particle
}




/////////////////////////////////////////////////////////////////////
// Checks the summary value for some value.
/////////////////////////////////////////////////////////////////////
uint8_t TreeMaker :: getSum (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType ) {
  uint8_t sumVal=0;
  if (!trk.summaryValue(sumVal, sumType)) {
    Error ("getSum ()", "Could not get summary type %i", sumType);
  }
  return sumVal;
}
