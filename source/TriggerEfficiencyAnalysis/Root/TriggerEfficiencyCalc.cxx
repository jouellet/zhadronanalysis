#include <TriggerEfficiencyAnalysis/TriggerEfficiencyCalc.h>

// EventLoop includes
#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

// root includes
#include <TFile.h>
#include <TSystem.h>

// xAOD includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"
#include "xAODTrigger/EmTauRoIContainer.h"

// HI event includes
#include "xAODHIEvent/HIEventShapeContainer.h"

// Tracking & vertex includes
#include "xAODTracking/VertexContainer.h"

// EGamma includes
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Electron.h"

// Muon includes
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/Muon.h>

// this is needed to distribute the algorithm to the workers
ClassImp(TriggerEfficiencyCalc)

TriggerEfficiencyCalc :: TriggerEfficiencyCalc () {
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  m_pp17_grl = nullptr;
  m_PbPb15_grl = nullptr;
  m_PbPb18_grl = nullptr;
  m_PbPb18_ignoreToroid_grl = nullptr;

  m_muonPtEtaPhiECorrector = nullptr;
  m_egammaPtEtaPhiECorrector = nullptr;

  m_trigDecisionTool = nullptr;
  m_trigConfigTool = nullptr;
  m_trigMatchingTool = nullptr;
  //m_trigMuonMatchingTool = nullptr;

  m_electronLHLooseHISelectorTool = nullptr;
  m_electronLHMediumHISelectorTool = nullptr;

  m_muonLooseSelectorTool = nullptr;
  m_muonMediumSelectorTool = nullptr;
  m_muonTightSelectorTool = nullptr;
}



EL::StatusCode TriggerEfficiencyCalc :: setupJob (EL::Job& job) {
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD (); 
 
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerEfficiencyCalc :: histInitialize () {
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk()->getOutputFile (m_outputName);

  m_tree = new TTree ("bush", "bush");
  m_tree->SetDirectory (outputFile);

  m_tree->Branch ("fcal_et", &m_b_fcal_et);

  m_tree->Branch ("muon_n", &m_b_muon_n);
  m_tree->Branch ("muon_pt", &m_b_muon_pt);
  m_tree->Branch ("muon_eta", &m_b_muon_eta);
  m_tree->Branch ("muon_phi", &m_b_muon_phi);
  m_tree->Branch ("muon_charge", &m_b_muon_charge);
  m_tree->Branch ("muon_loose", &m_b_muon_loose);
  m_tree->Branch ("muon_medium", &m_b_muon_medium);
  m_tree->Branch ("muon_tight", &m_b_muon_tight);
  m_tree->Branch ("muon_matched", &m_b_muon_matched);

  m_tree->Branch ("electron_n", &m_b_electron_n);
  m_tree->Branch ("electron_pt", &m_b_electron_pt);
  m_tree->Branch ("electron_eta", &m_b_electron_eta);
  m_tree->Branch ("electron_phi", &m_b_electron_phi);
  m_tree->Branch ("electron_charge", &m_b_electron_charge);
  m_tree->Branch ("electron_lhloose_hi", &m_b_electron_lhloose_hi);
  m_tree->Branch ("electron_lhmedium_hi", &m_b_electron_lhmedium_hi);
  m_tree->Branch ("electron_matched", &m_b_electron_matched);

  //h2_muonTrigEffNum = new TH2D ("h2_muonTrigEffNum", ";#Sigma#it{E}_{T}^{FCal} [TeV];#it{p}_{T}^{#mu} [GeV];Passed", 12, 0, 5, nPt, pt);
  //h2_muonTrigEffNum->Sumw2 ();
  //h2_muonTrigEffDen = new TH2D ("h2_muonTrigEffDen", ";#Sigma#it{E}_{T}^{FCal} [TeV];#it{p}_{T}^{#mu} [GeV];Total", 12, 0, 5, nPt, pt);
  //h2_muonTrigEffDen->Sumw2 ();

  //h2_electronTrigEffNum = new TH2D ("h2_electronTrigEffNum", ";#Sigma#it{E}_{T}^{FCal} [TeV];#it{p}_{T}^{e} [GeV];Passed", 12, 0, 5, nPt, pt);
  //h2_electronTrigEffNum->Sumw2 ();
  //h2_electronTrigEffDen = new TH2D ("h2_electronTrigEffDen", ";#Sigma#it{E}_{T}^{FCal} [TeV];#it{p}_{T}^{e} [GeV];Total", 12, 0, 5, nPt, pt);
  //h2_electronTrigEffDen->Sumw2 ();

  //h2_muonTrigEffNum->SetDirectory (outputFile);
  //h2_muonTrigEffDen->SetDirectory (outputFile);
  //h2_electronTrigEffNum->SetDirectory (outputFile);
  //h2_electronTrigEffDen->SetDirectory (outputFile);

  //h_z_m = new TH1D ("h_z_m", ";m_{Z} [GeV];Counts", 50, 65, 115);
  //h_z_m->Sumw2 ();
  //h_mu_pt = new TH1D ("h_mu_pt", ";#it{p}_{T}^{#mu} [GeV];Counts", 75, 0, 150);
  //h_mu_pt->Sumw2 ();
  //h_e_pt = new TH1D ("h_e_pt", ";#it{p}_{T}^{e} [GeV];Counts", 75, 0, 150);
  //h_e_pt->Sumw2 ();

  //h_z_m->SetDirectory (outputFile);
  //h_mu_pt->SetDirectory (outputFile);
  //h_e_pt->SetDirectory (outputFile);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerEfficiencyCalc :: fileExecute () {
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerEfficiencyCalc :: changeInput (bool /*firstFile*/) {
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerEfficiencyCalc :: initialize () {
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_CHECK_SET_TYPE (EL::StatusCode);


  //----------------------------------------------------------------------
  // Trigger initialization
  //----------------------------------------------------------------------
  // Initialize trigger configuration tool
  m_trigConfigTool = new TrigConf::xAODConfigTool ("xAODConfigTool"); // gives us access to the meta-data
  ANA_CHECK (m_trigConfigTool->initialize ());
  ToolHandle<TrigConf::ITrigConfigTool> trigConfigHandle (m_trigConfigTool);

  // Initialize trigger decision tool and connect to trigger configuration tool (NOTE: must be after trigger configuration tool is initialized!!!)
  m_trigDecisionTool = new Trig::TrigDecisionTool ("TrigDecisionTool");
  ANA_CHECK (m_trigDecisionTool->setProperty ("ConfigTool", trigConfigHandle)); // connect the TrigDecisionTool to the ConfigTool
  ANA_CHECK (m_trigDecisionTool->setProperty ("TrigDecisionKey", "xTrigDecision"));
  ANA_CHECK (m_trigDecisionTool->initialize ());
  ToolHandle <Trig::TrigDecisionTool> m_trigDecHandle (m_trigDecisionTool);

  m_trigMatchingTool = new Trig::MatchingTool ("xAODMatchingTool");
  //ANA_CHECK (m_trigMatchingTool->setProperty ("OutputLevel", MSG::DEBUG));
  ANA_CHECK (m_trigMatchingTool->initialize ());

 
  //----------------------------------------------------------------------
  // GRL
  //----------------------------------------------------------------------
  const char* GRLFilePath = "$UserAnalysis_DIR/data/ZTrackAnalysis";
  const char* pp17_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml", GRLFilePath));
  const char* PbPb15_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data15_hi.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_HeavyIonP_All_Good.xml", GRLFilePath));
  const char* PbPb18_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good.xml", GRLFilePath));
  const char* PbPb18_ignoreToroid_fullGRLFilePath = gSystem->ExpandPathName (Form ("%s/data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml", GRLFilePath));

  std::vector<std::string> vecStringGRL;

  vecStringGRL.clear ();
  m_pp17_grl = new GoodRunsListSelectionTool("pp17_GoodRunsListSelectionTool");
  vecStringGRL.push_back (pp17_fullGRLFilePath);
  ANA_CHECK (m_pp17_grl->setProperty ("GoodRunsListVec", vecStringGRL));
  ANA_CHECK (m_pp17_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK (m_pp17_grl->initialize ());

  vecStringGRL.clear ();
  m_PbPb15_grl = new GoodRunsListSelectionTool("PbPb15_GoodRunsListSelectionTool");
  vecStringGRL.push_back (PbPb15_fullGRLFilePath);
  ANA_CHECK (m_PbPb15_grl->setProperty ("GoodRunsListVec", vecStringGRL));
  ANA_CHECK (m_PbPb15_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK (m_PbPb15_grl->initialize ());

  vecStringGRL.clear ();
  m_PbPb18_grl = new GoodRunsListSelectionTool("PbPb18_GoodRunsListSelectionTool");
  vecStringGRL.push_back (PbPb18_fullGRLFilePath);
  ANA_CHECK (m_PbPb18_grl->setProperty ("GoodRunsListVec", vecStringGRL));
  ANA_CHECK (m_PbPb18_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK (m_PbPb18_grl->initialize ());

  vecStringGRL.clear ();
  m_PbPb18_ignoreToroid_grl = new GoodRunsListSelectionTool("PbPb18_ignoreToroid_GoodRunsListSelectionTool");
  vecStringGRL.push_back (PbPb18_ignoreToroid_fullGRLFilePath);
  ANA_CHECK (m_PbPb18_ignoreToroid_grl->setProperty ("GoodRunsListVec", vecStringGRL));
  ANA_CHECK (m_PbPb18_ignoreToroid_grl->setProperty ("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK (m_PbPb18_ignoreToroid_grl->initialize ());


  //----------------------------------------------------------------------
  // Initialize muon corrector tool
  //----------------------------------------------------------------------
  m_muonPtEtaPhiECorrector = new CP::MuonCalibrationAndSmearingTool ("MuonPtEtaPhiECorrector");
  if (m_collisionSystem == PbPb18) {
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Year", "Data18"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Release", "Recs2018_05_20"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("StatComb", false));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaCorr", true));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data18"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("doSagittaMCDistortion", false));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaCorrPhaseSpace", true));
  }
  else if (m_collisionSystem == pp17) {
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Year", "Data17"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("Release", "Recs2018_05_20"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("StatComb", false));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaCorr", true));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaRelease", "sagittaBiasDataAll_03_02_19_Data17"));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("doSagittaMCDistortion", false));
    ANA_CHECK (m_muonPtEtaPhiECorrector->setProperty ("SagittaCorrPhaseSpace", true));
  }
  ANA_CHECK (m_muonPtEtaPhiECorrector->initialize ());


  //----------------------------------------------------------------------
  // Egamma calibration tool
  //----------------------------------------------------------------------
  m_egammaPtEtaPhiECorrector = new CP::EgammaCalibrationAndSmearingTool ("EgammaPtEtaPhiECorrector");
  if (m_collisionSystem == PbPb18) {
    ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("ESModel", "es2017_R21_ofc0_v1"));
  }
  else if (m_collisionSystem == pp17) {
    ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("ESModel", "es2017_R21_v1"));
  }
  else if (m_collisionSystem == PbPb15 || m_collisionSystem == pp15) {
    ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("ESModel", "es2015_5TeV"));
  }
  else {
    Error ("InitEgammaCalibrationAndSmearingTool ()", "Failed to recognize collision system!");
    return EL::StatusCode::FAILURE;
  }
  ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("randomRunNumber", EgammaCalibPeriodRunNumbersExample::run_2016)); // if testing in MC
  ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("decorrelationModel", "FULL_v1")); // 1NP_v1
  ANA_CHECK (m_egammaPtEtaPhiECorrector->setProperty ("useAFII", 0));
  ANA_CHECK (m_egammaPtEtaPhiECorrector->initialize ());

  
  //----------------------------------------------------------------------
  // Electron selector tool
  //----------------------------------------------------------------------
  m_electronLHLooseHISelectorTool = new AsgElectronLikelihoodTool ("ElectronLHLooseHISelectorTool");
  ANA_CHECK (m_electronLHLooseHISelectorTool->setProperty ("ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20160907_HI/ElectronLikelihoodLooseOfflineConfig2016_HI.conf"));
  ANA_CHECK (m_electronLHLooseHISelectorTool->initialize ());

  m_electronLHMediumHISelectorTool = new AsgElectronLikelihoodTool ("ElectronLHMediumHISelectorTool");
  ANA_CHECK (m_electronLHMediumHISelectorTool->setProperty ("ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20160907_HI/ElectronLikelihoodMediumOfflineConfig2016_HI.conf"));
  ANA_CHECK (m_electronLHMediumHISelectorTool->initialize ());


  //----------------------------------------------------------------------
  // Muon selector tool
  //----------------------------------------------------------------------
  m_muonLooseSelectorTool = new CP::MuonSelectionTool ("MuonLooseSelection");
  ANA_CHECK (m_muonLooseSelectorTool->setProperty ("MaxEta", 2.5));
  ANA_CHECK (m_muonLooseSelectorTool->setProperty ("MuQuality", 2));
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18)
    ANA_CHECK (m_muonLooseSelectorTool->setProperty ("TrtCutOff", true));
  ANA_CHECK (m_muonLooseSelectorTool->initialize ());

  m_muonMediumSelectorTool = new CP::MuonSelectionTool ("MuonMediumSelection");
  ANA_CHECK (m_muonMediumSelectorTool->setProperty ("MaxEta", 2.5));
  ANA_CHECK (m_muonMediumSelectorTool->setProperty ("MuQuality", 1));
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18)
    ANA_CHECK (m_muonMediumSelectorTool->setProperty ("TrtCutOff", true));
  ANA_CHECK (m_muonMediumSelectorTool->initialize ());

  m_muonTightSelectorTool = new CP::MuonSelectionTool ("MuonTightSelection");
  ANA_CHECK (m_muonTightSelectorTool->setProperty ("MaxEta", 2.5));
  ANA_CHECK (m_muonTightSelectorTool->setProperty ("MuQuality", 0));
  if (m_collisionSystem == PbPb15 || m_collisionSystem == PbPb18)
    ANA_CHECK (m_muonTightSelectorTool->setProperty ("TrtCutOff", true));
  ANA_CHECK (m_muonTightSelectorTool->initialize ());


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerEfficiencyCalc :: execute () {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // set type of return code you are expecting
  // (add to top of each function once)

  ANA_CHECK_SET_TYPE (EL::StatusCode);

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK (evtStore ()->retrieve (eventInfo, "EventInfo"));  

  //----------------------------------------------------------------------
  // Event & collision information
  //----------------------------------------------------------------------
  {
    // if data, check if event passes GRL
    if (m_collisionSystem == pp17) {
      m_b_passes_toroid = true;
      if (!m_pp17_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
    } 
    else if (m_collisionSystem == PbPb18) {
      m_b_passes_toroid = m_PbPb18_grl->passRunLB (*eventInfo);
      if (!m_PbPb18_ignoreToroid_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
    }
    else if (m_collisionSystem == PbPb15) {
      m_b_passes_toroid = true;
      if (!m_PbPb15_grl->passRunLB (*eventInfo)) {
        return EL::StatusCode::SUCCESS;
      }
    }
    else {
      Error ("CheckGRL()", "Undefined collision system. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    // if events passes event cleaning
    if (eventInfo->errorState (xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ||
        eventInfo->errorState (xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ||
        eventInfo->errorState (xAOD::EventInfo::SCT) == xAOD::EventInfo::Error ||
        eventInfo->isEventFlagBitSet (xAOD::EventInfo::Core, 18)) {
      return EL::StatusCode::SUCCESS;
    }
  } // end GRL scope

  
  //----------------------------------------------------------------------
  // Gather verticies information
  //----------------------------------------------------------------------
  {
    const xAOD::VertexContainer* primaryVertices = 0;
    if (!evtStore ()->retrieve (primaryVertices, "PrimaryVertices") .isSuccess ())  {
      Error ("GetPrimaryVertices ()", "Failed to retrieve PrimaryVertices container. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    bool hasPrimaryVert = false;
    for (const auto* vert : *primaryVertices) {
      if (vert->vertexType () == xAOD::VxType::PriVtx)
        hasPrimaryVert = true;
    }

    if (!hasPrimaryVert) {
      Info ("GetPrimaryVertex ()", "Did not find a primary vertex. Exiting.");
      return EL::StatusCode::SUCCESS;
    }
  } // end vertex scope


  //----------------------------------------------------------------------
  // Calculate total FCal energies
  //----------------------------------------------------------------------
  {
    m_b_fcal_et = 0;

    const xAOD::HIEventShapeContainer* hiueContainer = 0;  
    if (!evtStore ()->retrieve (hiueContainer, "HIEventShape").isSuccess ()) {
      Error ("GetHIEventShape ()", "Failed to retrieve HIEventShape container. Exiting." );
      return EL::StatusCode::FAILURE; 
    }

    for (const auto* hiue : *hiueContainer) {
      int layer = hiue->layer ();

      if (layer != 21 && layer != 22 && layer != 23)
        continue;

      m_b_fcal_et += hiue->et () * 1e-6; // convert MeV to TeV
    }
  } // end fcal scope


  //----------------------------------------------------------------------
  // Get muon container
  //----------------------------------------------------------------------
  m_b_muon_n = 0;
  m_b_muon_pt.clear ();
  m_b_muon_eta.clear ();
  m_b_muon_phi.clear ();
  m_b_muon_charge.clear ();
  m_b_muon_loose.clear ();
  m_b_muon_medium.clear ();
  m_b_muon_tight.clear ();
  m_b_muon_matched.clear ();

  std::string muonTrigName;
  if (m_collisionSystem == PbPb15)
    muonTrigName = "HLT_mu14";
  else // default is non-ion lhloose trigger
    muonTrigName = "HLT_mu14";

  if (m_b_passes_toroid && m_trigDecisionTool->getChainGroup (muonTrigName)->isPassed ()) {
    const xAOD::MuonContainer* muons = 0;
    if (!evtStore ()->retrieve (muons, "Muons").isSuccess()) {
      Error ("GetMuons()", "Failed to retrieve Muons collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    std::vector<const xAOD::IParticle*> myParticles;

    for (int iMuon1 = 0; iMuon1 < (int)muons->size (); iMuon1++) {
      const xAOD::Muon* muon1 = muons->at (iMuon1);

      //if (!m_muonLooseSelectorTool->accept (muon1) && !m_muonMediumSelectorTool->accept (muon1) && !m_muonTightSelectorTool->accept (muon1))
      //  continue;

      // create calibrated copy of muon
      xAOD::Muon* tag = nullptr;
      if (m_muonPtEtaPhiECorrector->correctedCopy (*muon1, tag) != CP::CorrectionCode::Ok) {
        ANA_MSG_INFO ("Cannot calibrate particle!");
      }

      myParticles.clear ();
      myParticles.push_back ((const xAOD::IParticle*)muon1);

      bool matched = m_trigMatchingTool->match (myParticles, muonTrigName, 0.1, false);
      //if (!matched) {
      if (tag->pt () * 1e-3 < pt[0]) {
        if (tag)
          delete tag;
        continue;
      }

      //h_mu_pt->Fill (tag->pt () * 1e-3);

      m_b_muon_n++;
      m_b_muon_pt.push_back (tag->pt () * 1e-3);
      m_b_muon_eta.push_back (tag->eta ());
      m_b_muon_phi.push_back (tag->phi ());
      m_b_muon_charge.push_back (tag->charge ());
      m_b_muon_loose.push_back (m_muonLooseSelectorTool->accept (muon1));
      m_b_muon_medium.push_back (m_muonMediumSelectorTool->accept (muon1));
      m_b_muon_tight.push_back (m_muonTightSelectorTool->accept (muon1));
      m_b_muon_matched.push_back (matched);

      if (tag)
        delete tag;
    } // end muon loop

  } // end muon scope


  //----------------------------------------------------------------------
  // Get electron container
  //----------------------------------------------------------------------
  m_b_electron_n = 0;
  m_b_electron_pt.clear ();
  m_b_electron_eta.clear ();
  m_b_electron_phi.clear ();
  m_b_electron_charge.clear ();
  m_b_electron_lhloose_hi.clear ();
  m_b_electron_lhmedium_hi.clear ();
  m_b_electron_matched.clear ();
  std::string electronTrigName;
  if (m_collisionSystem == PbPb18)
    electronTrigName = "HLT_e15_lhloose_ion_L1EM12";
  else if (m_collisionSystem == PbPb15)
    electronTrigName = "HLT_e15_loose_ion_L1EM12";
  else // default is non-ion lhloose trigger
    electronTrigName = "HLT_e15_lhloose_L1EM12";

  if (m_trigDecisionTool->getChainGroup (electronTrigName)->isPassed ()) {
    const xAOD::ElectronContainer* electrons = 0;
    if (!evtStore ()->retrieve (electrons, "Electrons").isSuccess()) {
      Error ("GetElectrons ()", "Failed to retrieve Electrons collection. Exiting.");
      return EL::StatusCode::FAILURE;
    }

    std::vector<const xAOD::IParticle*> myParticles;

    for (int iElectron1 = 0; iElectron1 < (int)electrons->size (); iElectron1++) {
      const xAOD::Electron* electron1 = electrons->at (iElectron1);

      //if (!m_electronLHLooseHISelectorTool->accept (electron1) && !m_electronLHMediumHISelectorTool->accept (electron1))
      //  continue;

      // create calibrated copy of electron
      xAOD::Electron* tag = nullptr;
      if (m_egammaPtEtaPhiECorrector->correctedCopy (*electron1, tag) != CP::CorrectionCode::Ok) {
        ANA_MSG_INFO ("Cannot calibrate particle!");
      }
 
      myParticles.clear ();
      myParticles.push_back ((const xAOD::IParticle*)tag);

      bool matched = m_trigMatchingTool->match (myParticles, electronTrigName, 0.07, false);
      if (tag->pt () * 1e-3 < pt[0]) {
        if (tag)
          delete tag;
        continue;
      }

      m_b_electron_n++;
      m_b_electron_pt.push_back (tag->pt () * 1e-3);
      m_b_electron_eta.push_back (tag->caloCluster ()->etaBE (2));
      m_b_electron_phi.push_back (tag->phi ());
      m_b_electron_charge.push_back (tag->charge ());
      m_b_electron_lhloose_hi.push_back (m_electronLHLooseHISelectorTool->accept (electron1));
      m_b_electron_lhmedium_hi.push_back (m_electronLHMediumHISelectorTool->accept (electron1));
      m_b_electron_matched.push_back (matched);

      if (tag)
        delete tag;
    } // end electron loop

  } // end electron scope

  if (m_b_muon_n != 0 || m_b_electron_n != 0)
    m_tree->Fill ();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerEfficiencyCalc :: postExecute () {
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerEfficiencyCalc :: finalize () {
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  if (m_pp17_grl) {
    delete m_pp17_grl;
    m_pp17_grl = nullptr;
  }
  if (m_PbPb18_grl) {
    delete m_PbPb18_grl;
    m_PbPb18_grl = nullptr;
  }
  if (m_PbPb18_ignoreToroid_grl) {
    delete m_PbPb18_ignoreToroid_grl;
    m_PbPb18_ignoreToroid_grl = nullptr;
  }
  
  if (m_trigMatchingTool) {
    delete m_trigMatchingTool;
    m_trigMatchingTool = nullptr;
  }

  if (m_muonPtEtaPhiECorrector) {
    delete m_muonPtEtaPhiECorrector;
    m_muonPtEtaPhiECorrector = nullptr;
  }

  if (m_egammaPtEtaPhiECorrector) {
    delete m_egammaPtEtaPhiECorrector;
    m_egammaPtEtaPhiECorrector = nullptr;
  }

  if (m_muonLooseSelectorTool) {
    delete m_muonLooseSelectorTool;
    m_muonLooseSelectorTool = nullptr;
  }
  if (m_muonMediumSelectorTool) {
    delete m_muonMediumSelectorTool;
    m_muonMediumSelectorTool = nullptr;
  }
  if (m_muonTightSelectorTool) {
    delete m_muonTightSelectorTool;
    m_muonTightSelectorTool = nullptr;
  }

  if (m_electronLHLooseHISelectorTool) {
    delete m_electronLHLooseHISelectorTool;
    m_electronLHLooseHISelectorTool = nullptr;
  }
  if (m_electronLHMediumHISelectorTool) {
    delete m_electronLHMediumHISelectorTool;
    m_electronLHMediumHISelectorTool = nullptr;
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerEfficiencyCalc :: histFinalize () {
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
