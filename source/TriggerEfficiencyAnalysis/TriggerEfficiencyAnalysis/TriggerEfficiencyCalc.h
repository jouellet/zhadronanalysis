#ifndef TriggerEfficiencyAnalysis_TriggerEfficiencyCalc_H
#define TriggerEfficiencyAnalysis_TriggerEfficiencyCalc_H

#include <TTree.h>
#include <TH2.h>

#include <EventLoop/Algorithm.h>
#include <AsgTools/AnaToolHandle.h>

// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

// Trigger
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "TriggerMatchingTool/MatchingTool.h"

// Muon tools
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"

// Electron tools
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"

#include <vector>

enum CollisionSystem { pp15 = 0, PbPb15 = 1, pPb16 = 2, Pbp16 = 3, pp17 = 4, PbPb18 = 5 };

class TriggerEfficiencyCalc : public EL::Algorithm
{
private:

  double pt[11] = {5, 6.29463, 7.92447, 9.97631, 12.5594, 15.8114, 19.9054, 25.0594, 31.5479, 39.7164, 50};
  int nPt = 10;

public:
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  // configuration variables
  CollisionSystem m_collisionSystem;
  std::string m_outputName;

  TTree* m_tree; //!

  //TH2D* h2_muonTrigEffNum; //!
  //TH2D* h2_muonTrigEffDen; //!

  //TH2D* h2_electronTrigEffNum; //!
  //TH2D* h2_electronTrigEffDen; //!

  //TH1D* h_z_m; //!
  //TH1D* h_mu_pt; //!
  //TH1D* h_e_pt; //!
  

  // whether event passes toroid GRL
  bool m_b_passes_toroid; //!

  // FCal Et info
  double m_b_fcal_et; //!

  // Muons
  int m_b_muon_n; //!
  std::vector<float> m_b_muon_pt; //!
  std::vector<float> m_b_muon_eta; //!
  std::vector<float> m_b_muon_phi; //!
  std::vector<int> m_b_muon_charge; //!
  std::vector<bool> m_b_muon_loose; //!
  std::vector<bool> m_b_muon_medium; //!
  std::vector<bool> m_b_muon_tight; //!
  std::vector<bool> m_b_muon_matched; //!

  int m_b_electron_n; //!
  std::vector<float> m_b_electron_pt; //!
  std::vector<float> m_b_electron_eta; //!
  std::vector<float> m_b_electron_phi; //!
  std::vector<int> m_b_electron_charge; //!
  std::vector<bool> m_b_electron_lhloose_hi; //!
  std::vector<bool> m_b_electron_lhmedium_hi; //!
  std::vector<bool> m_b_electron_matched; //!


  // Muon tools
  CP::MuonCalibrationAndSmearingTool* m_muonPtEtaPhiECorrector; //!

  // Electron tools
  CP::EgammaCalibrationAndSmearingTool* m_egammaPtEtaPhiECorrector; //!

  // GRL tools
  GoodRunsListSelectionTool* m_pp17_grl; //!
  GoodRunsListSelectionTool* m_PbPb15_grl; //!
  GoodRunsListSelectionTool* m_PbPb18_grl; //!
  GoodRunsListSelectionTool* m_PbPb18_ignoreToroid_grl; //!

  // Trigger tools
  Trig::TrigDecisionTool* m_trigDecisionTool; //!
  TrigConf::xAODConfigTool* m_trigConfigTool; //!
  Trig::MatchingTool* m_trigMatchingTool; //!

  // Electron selection tool
  AsgElectronLikelihoodTool* m_electronLHLooseHISelectorTool; //!
  AsgElectronLikelihoodTool* m_electronLHMediumHISelectorTool; //!

  // Muon selection tool
  CP::MuonSelectionTool* m_muonLooseSelectorTool; //!
  CP::MuonSelectionTool* m_muonMediumSelectorTool; //!
  CP::MuonSelectionTool* m_muonTightSelectorTool; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // this is a standard constructor
  TriggerEfficiencyCalc ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TriggerEfficiencyCalc, 1);
};

#endif
