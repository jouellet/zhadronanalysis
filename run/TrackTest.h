//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jun 18 17:26:12 2019 by ROOT version 6.14/08
// from TTree bush/bush
// found on file: mc16_5TeV.root
//////////////////////////////////////////////////////////

#ifndef TrackTest_h
#define TrackTest_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class TrackTest {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Bool_t          passes_toroid;
   UInt_t          lumi_block;
   UInt_t          event_number;
   Float_t         actualInteractionsPerCrossing;
   Float_t         averageInteractionsPerCrossing;
   vector<float>   *mcEventWeights;
   Bool_t          HLT_e15_lhloose_L1EM12;
   Float_t         HLT_e15_lhloose_L1EM12_prescale;
   Bool_t          HLT_e15_lhloose_ion_L1EM12;
   Float_t         HLT_e15_lhloose_ion_L1EM12_prescale;
   Bool_t          HLT_mu14;
   Float_t         HLT_mu14_prescale;
   Int_t           nvert;
   vector<float>   *vert_x;
   vector<float>   *vert_y;
   vector<float>   *vert_z;
   vector<int>     *vert_ntrk;
   vector<int>     *vert_type;
   Float_t         fcalA_et;
   Float_t         fcalC_et;
   Float_t         fcalA_et_Cos;
   Float_t         fcalC_et_Cos;
   Float_t         fcalA_et_Sin;
   Float_t         fcalC_et_Sin;
   Int_t           electron_n;
   vector<float>   *electron_pt;
   vector<float>   *electron_eta;
   vector<float>   *electron_phi;
   vector<int>     *electron_charge;
   vector<float>   *electron_etcone20;
   vector<float>   *electron_etcone30;
   vector<float>   *electron_etcone40;
   vector<float>   *electron_topoetcone20;
   vector<float>   *electron_topoetcone30;
   vector<float>   *electron_topoetcone40;
   vector<bool>    *electron_lhtight;
   vector<bool>    *electron_lhmedium;
   vector<bool>    *electron_lhloose;
   vector<bool>    *electron_lhmedium_hi;
   vector<bool>    *electron_lhloose_hi;
   vector<float>   *electron_tp_pt;
   vector<float>   *electron_tp_eta;
   vector<float>   *electron_tp_phi;
   vector<float>   *electron_tp_charge;
   vector<int>     *electron_ntrk;
   vector<float>   *electron_track_pt;
   vector<float>   *electron_track_eta;
   vector<float>   *electron_track_phi;
   vector<float>   *electron_track_charge;
   vector<float>   *electron_track_d0sig;
   vector<float>   *electron_track_z0;
   vector<float>   *electron_track_vz;
   vector<float>   *electron_track_theta;
   vector<float>   *electron_pt_sys;
   vector<float>   *electron_eta_sys;
   vector<float>   *electron_phi_sys;
   Int_t           muon_n;
   vector<float>   *muon_pt;
   vector<float>   *muon_eta;
   vector<float>   *muon_phi;
   vector<int>     *muon_charge;
   vector<bool>    *muon_tight;
   vector<bool>    *muon_medium;
   vector<bool>    *muon_loose;
   vector<float>   *muon_track_pt;
   vector<float>   *muon_track_eta;
   vector<float>   *muon_track_phi;
   vector<float>   *muon_track_charge;
   vector<float>   *muon_track_d0sig;
   vector<float>   *muon_track_z0;
   vector<float>   *muon_track_vz;
   vector<float>   *muon_track_theta;
   vector<float>   *muon_pt_sys;
   vector<float>   *muon_eta_sys;
   vector<float>   *muon_phi_sys;
   Int_t           ntrk;
   vector<float>   *trk_pt;
   vector<float>   *trk_eta;
   vector<float>   *trk_phi;
   vector<float>   *trk_charge;
   vector<bool>    *trk_tight_primary;
   vector<bool>    *trk_minbias;
   vector<bool>    *trk_HItight;
   vector<bool>    *trk_HIloose;
   vector<float>   *trk_d0;
   vector<float>   *trk_z0;
   vector<float>   *trk_vz;
   vector<float>   *trk_theta;
   vector<float>   *trk_prob_truth;
   vector<float>   *trk_truth_pt;
   vector<float>   *trk_truth_eta;
   vector<float>   *trk_truth_phi;
   vector<float>   *trk_truth_charge;
   vector<int>     *trk_truth_type;
   vector<int>     *trk_truth_orig;
   vector<int>     *trk_truth_barcode;
   vector<int>     *trk_truth_pdgid;
   vector<float>   *trk_truth_vz;
   vector<int>     *trk_truth_nIn;
   vector<bool>    *trk_truth_isHadron;
   Int_t           truth_electron_n;
   vector<float>   *truth_electron_pt;
   vector<float>   *truth_electron_eta;
   vector<float>   *truth_electron_phi;
   vector<int>     *truth_electron_charge;
   vector<int>     *truth_electron_barcode;
   Int_t           truth_muon_n;
   vector<float>   *truth_muon_pt;
   vector<float>   *truth_muon_eta;
   vector<float>   *truth_muon_phi;
   vector<int>     *truth_muon_charge;
   vector<int>     *truth_muon_barcode;
   Int_t           truth_trk_n;
   vector<float>   *truth_trk_pt;
   vector<float>   *truth_trk_eta;
   vector<float>   *truth_trk_phi;
   vector<float>   *truth_trk_charge;
   vector<int>     *truth_trk_pdgid;
   vector<int>     *truth_trk_barcode;
   vector<bool>    *truth_trk_isHadron;
   Int_t           akt4_truth_jet_n;
   vector<float>   *akt4_truth_jet_pt;
   vector<float>   *akt4_truth_jet_eta;
   vector<float>   *akt4_truth_jet_phi;
   vector<float>   *akt4_truth_jet_e;

   // List of branches
   TBranch        *b_passes_toroid;   //!
   TBranch        *b_lumi_block;   //!
   TBranch        *b_event_number;   //!
   TBranch        *b_actualInteractionsPerCrossing;   //!
   TBranch        *b_averageInteractionsPerCrossing;   //!
   TBranch        *b_mcEventWeights;   //!
   TBranch        *b_HLT_e15_lhloose_L1EM12;   //!
   TBranch        *b_HLT_e15_lhloose_L1EM12_prescale;   //!
   TBranch        *b_HLT_e15_lhloose_ion_L1EM12;   //!
   TBranch        *b_HLT_e15_lhloose_ion_L1EM12_prescale;   //!
   TBranch        *b_HLT_mu14;   //!
   TBranch        *b_HLT_mu14_prescale;   //!
   TBranch        *b_nvert;   //!
   TBranch        *b_vert_x;   //!
   TBranch        *b_vert_y;   //!
   TBranch        *b_vert_z;   //!
   TBranch        *b_vert_ntrk;   //!
   TBranch        *b_vert_type;   //!
   TBranch        *b_fcalA_et;   //!
   TBranch        *b_fcalC_et;   //!
   TBranch        *b_fcalA_et_Cos;   //!
   TBranch        *b_fcalC_et_Cos;   //!
   TBranch        *b_fcalA_et_Sin;   //!
   TBranch        *b_fcalC_et_Sin;   //!
   TBranch        *b_electron_n;   //!
   TBranch        *b_electron_pt;   //!
   TBranch        *b_electron_eta;   //!
   TBranch        *b_electron_phi;   //!
   TBranch        *b_electron_charge;   //!
   TBranch        *b_electron_etcone20;   //!
   TBranch        *b_electron_etcone30;   //!
   TBranch        *b_electron_etcone40;   //!
   TBranch        *b_electron_topoetcone20;   //!
   TBranch        *b_electron_topoetcone30;   //!
   TBranch        *b_electron_topoetcone40;   //!
   TBranch        *b_electron_lhtight;   //!
   TBranch        *b_electron_lhmedium;   //!
   TBranch        *b_electron_lhloose;   //!
   TBranch        *b_electron_lhmedium_hi;   //!
   TBranch        *b_electron_lhloose_hi;   //!
   TBranch        *b_electron_tp_pt;   //!
   TBranch        *b_electron_tp_eta;   //!
   TBranch        *b_electron_tp_phi;   //!
   TBranch        *b_electron_tp_charge;   //!
   TBranch        *b_electron_ntrk;   //!
   TBranch        *b_electron_track_pt;   //!
   TBranch        *b_electron_track_eta;   //!
   TBranch        *b_electron_track_phi;   //!
   TBranch        *b_electron_track_charge;   //!
   TBranch        *b_electron_track_d0sig;   //!
   TBranch        *b_electron_track_z0;   //!
   TBranch        *b_electron_track_vz;   //!
   TBranch        *b_electron_track_theta;   //!
   TBranch        *b_electron_pt_sys;   //!
   TBranch        *b_electron_eta_sys;   //!
   TBranch        *b_electron_phi_sys;   //!
   TBranch        *b_muon_n;   //!
   TBranch        *b_muon_pt;   //!
   TBranch        *b_muon_eta;   //!
   TBranch        *b_muon_phi;   //!
   TBranch        *b_muon_charge;   //!
   TBranch        *b_muon_tight;   //!
   TBranch        *b_muon_medium;   //!
   TBranch        *b_muon_loose;   //!
   TBranch        *b_muon_track_pt;   //!
   TBranch        *b_muon_track_eta;   //!
   TBranch        *b_muon_track_phi;   //!
   TBranch        *b_muon_track_charge;   //!
   TBranch        *b_muon_track_d0sig;   //!
   TBranch        *b_muon_track_z0;   //!
   TBranch        *b_muon_track_vz;   //!
   TBranch        *b_muon_track_theta;   //!
   TBranch        *b_muon_pt_sys;   //!
   TBranch        *b_muon_eta_sys;   //!
   TBranch        *b_muon_phi_sys;   //!
   TBranch        *b_ntrk;   //!
   TBranch        *b_trk_pt;   //!
   TBranch        *b_trk_eta;   //!
   TBranch        *b_trk_phi;   //!
   TBranch        *b_trk_charge;   //!
   TBranch        *b_trk_tight_primary;   //!
   TBranch        *b_trk_minbias;   //!
   TBranch        *b_trk_HItight;   //!
   TBranch        *b_trk_HIloose;   //!
   TBranch        *b_trk_d0;   //!
   TBranch        *b_trk_z0;   //!
   TBranch        *b_trk_vz;   //!
   TBranch        *b_trk_theta;   //!
   TBranch        *b_trk_prob_truth;   //!
   TBranch        *b_trk_truth_pt;   //!
   TBranch        *b_trk_truth_eta;   //!
   TBranch        *b_trk_truth_phi;   //!
   TBranch        *b_trk_truth_charge;   //!
   TBranch        *b_trk_truth_type;   //!
   TBranch        *b_trk_truth_orig;   //!
   TBranch        *b_trk_truth_barcode;   //!
   TBranch        *b_trk_truth_pdgid;   //!
   TBranch        *b_trk_truth_vz;   //!
   TBranch        *b_trk_truth_nIn;   //!
   TBranch        *b_trk_truth_isHadron;   //!
   TBranch        *b_truth_electron_n;   //!
   TBranch        *b_truth_electron_pt;   //!
   TBranch        *b_truth_electron_eta;   //!
   TBranch        *b_truth_electron_phi;   //!
   TBranch        *b_truth_electron_charge;   //!
   TBranch        *b_truth_electron_barcode;   //!
   TBranch        *b_truth_muon_n;   //!
   TBranch        *b_truth_muon_pt;   //!
   TBranch        *b_truth_muon_eta;   //!
   TBranch        *b_truth_muon_phi;   //!
   TBranch        *b_truth_muon_charge;   //!
   TBranch        *b_truth_muon_barcode;   //!
   TBranch        *b_truth_trk_n;   //!
   TBranch        *b_truth_trk_pt;   //!
   TBranch        *b_truth_trk_eta;   //!
   TBranch        *b_truth_trk_phi;   //!
   TBranch        *b_truth_trk_charge;   //!
   TBranch        *b_truth_trk_pdgid;   //!
   TBranch        *b_truth_trk_barcode;   //!
   TBranch        *b_truth_trk_isHadron;   //!
   TBranch        *b_akt4_truth_jet_n;   //!
   TBranch        *b_akt4_truth_jet_pt;   //!
   TBranch        *b_akt4_truth_jet_eta;   //!
   TBranch        *b_akt4_truth_jet_phi;   //!
   TBranch        *b_akt4_truth_jet_e;   //!

   TrackTest(TTree *tree=0);
   virtual ~TrackTest();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TrackTest_cxx
TrackTest::TrackTest(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("mc16_5TeV.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("mc16_5TeV.root");
      }
      f->GetObject("bush",tree);

   }
   Init(tree);
}

TrackTest::~TrackTest()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TrackTest::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TrackTest::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TrackTest::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mcEventWeights = 0;
   vert_x = 0;
   vert_y = 0;
   vert_z = 0;
   vert_ntrk = 0;
   vert_type = 0;
   electron_pt = 0;
   electron_eta = 0;
   electron_phi = 0;
   electron_charge = 0;
   electron_etcone20 = 0;
   electron_etcone30 = 0;
   electron_etcone40 = 0;
   electron_topoetcone20 = 0;
   electron_topoetcone30 = 0;
   electron_topoetcone40 = 0;
   electron_lhtight = 0;
   electron_lhmedium = 0;
   electron_lhloose = 0;
   electron_lhmedium_hi = 0;
   electron_lhloose_hi = 0;
   electron_tp_pt = 0;
   electron_tp_eta = 0;
   electron_tp_phi = 0;
   electron_tp_charge = 0;
   electron_ntrk = 0;
   electron_track_pt = 0;
   electron_track_eta = 0;
   electron_track_phi = 0;
   electron_track_charge = 0;
   electron_track_d0sig = 0;
   electron_track_z0 = 0;
   electron_track_vz = 0;
   electron_track_theta = 0;
   electron_pt_sys = 0;
   electron_eta_sys = 0;
   electron_phi_sys = 0;
   muon_pt = 0;
   muon_eta = 0;
   muon_phi = 0;
   muon_charge = 0;
   muon_tight = 0;
   muon_medium = 0;
   muon_loose = 0;
   muon_track_pt = 0;
   muon_track_eta = 0;
   muon_track_phi = 0;
   muon_track_charge = 0;
   muon_track_d0sig = 0;
   muon_track_z0 = 0;
   muon_track_vz = 0;
   muon_track_theta = 0;
   muon_pt_sys = 0;
   muon_eta_sys = 0;
   muon_phi_sys = 0;
   trk_pt = 0;
   trk_eta = 0;
   trk_phi = 0;
   trk_charge = 0;
   trk_tight_primary = 0;
   trk_minbias = 0;
   trk_HItight = 0;
   trk_HIloose = 0;
   trk_d0 = 0;
   trk_z0 = 0;
   trk_vz = 0;
   trk_theta = 0;
   trk_prob_truth = 0;
   trk_truth_pt = 0;
   trk_truth_eta = 0;
   trk_truth_phi = 0;
   trk_truth_charge = 0;
   trk_truth_type = 0;
   trk_truth_orig = 0;
   trk_truth_barcode = 0;
   trk_truth_pdgid = 0;
   trk_truth_vz = 0;
   trk_truth_nIn = 0;
   trk_truth_isHadron = 0;
   truth_electron_pt = 0;
   truth_electron_eta = 0;
   truth_electron_phi = 0;
   truth_electron_charge = 0;
   truth_electron_barcode = 0;
   truth_muon_pt = 0;
   truth_muon_eta = 0;
   truth_muon_phi = 0;
   truth_muon_charge = 0;
   truth_muon_barcode = 0;
   truth_trk_pt = 0;
   truth_trk_eta = 0;
   truth_trk_phi = 0;
   truth_trk_charge = 0;
   truth_trk_pdgid = 0;
   truth_trk_barcode = 0;
   truth_trk_isHadron = 0;
   akt4_truth_jet_pt = 0;
   akt4_truth_jet_eta = 0;
   akt4_truth_jet_phi = 0;
   akt4_truth_jet_e = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("passes_toroid", &passes_toroid, &b_passes_toroid);
   fChain->SetBranchAddress("lumi_block", &lumi_block, &b_lumi_block);
   fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
   fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("mcEventWeights", &mcEventWeights, &b_mcEventWeights);
   fChain->SetBranchAddress("HLT_e15_lhloose_L1EM12", &HLT_e15_lhloose_L1EM12, &b_HLT_e15_lhloose_L1EM12);
   fChain->SetBranchAddress("HLT_e15_lhloose_L1EM12_prescale", &HLT_e15_lhloose_L1EM12_prescale, &b_HLT_e15_lhloose_L1EM12_prescale);
   fChain->SetBranchAddress("HLT_e15_lhloose_ion_L1EM12", &HLT_e15_lhloose_ion_L1EM12, &b_HLT_e15_lhloose_ion_L1EM12);
   fChain->SetBranchAddress("HLT_e15_lhloose_ion_L1EM12_prescale", &HLT_e15_lhloose_ion_L1EM12_prescale, &b_HLT_e15_lhloose_ion_L1EM12_prescale);
   fChain->SetBranchAddress("HLT_mu14", &HLT_mu14, &b_HLT_mu14);
   fChain->SetBranchAddress("HLT_mu14_prescale", &HLT_mu14_prescale, &b_HLT_mu14_prescale);
   fChain->SetBranchAddress("nvert", &nvert, &b_nvert);
   fChain->SetBranchAddress("vert_x", &vert_x, &b_vert_x);
   fChain->SetBranchAddress("vert_y", &vert_y, &b_vert_y);
   fChain->SetBranchAddress("vert_z", &vert_z, &b_vert_z);
   fChain->SetBranchAddress("vert_ntrk", &vert_ntrk, &b_vert_ntrk);
   fChain->SetBranchAddress("vert_type", &vert_type, &b_vert_type);
   fChain->SetBranchAddress("fcalA_et", &fcalA_et, &b_fcalA_et);
   fChain->SetBranchAddress("fcalC_et", &fcalC_et, &b_fcalC_et);
   fChain->SetBranchAddress("fcalA_et_Cos", &fcalA_et_Cos, &b_fcalA_et_Cos);
   fChain->SetBranchAddress("fcalC_et_Cos", &fcalC_et_Cos, &b_fcalC_et_Cos);
   fChain->SetBranchAddress("fcalA_et_Sin", &fcalA_et_Sin, &b_fcalA_et_Sin);
   fChain->SetBranchAddress("fcalC_et_Sin", &fcalC_et_Sin, &b_fcalC_et_Sin);
   fChain->SetBranchAddress("electron_n", &electron_n, &b_electron_n);
   fChain->SetBranchAddress("electron_pt", &electron_pt, &b_electron_pt);
   fChain->SetBranchAddress("electron_eta", &electron_eta, &b_electron_eta);
   fChain->SetBranchAddress("electron_phi", &electron_phi, &b_electron_phi);
   fChain->SetBranchAddress("electron_charge", &electron_charge, &b_electron_charge);
   fChain->SetBranchAddress("electron_etcone20", &electron_etcone20, &b_electron_etcone20);
   fChain->SetBranchAddress("electron_etcone30", &electron_etcone30, &b_electron_etcone30);
   fChain->SetBranchAddress("electron_etcone40", &electron_etcone40, &b_electron_etcone40);
   fChain->SetBranchAddress("electron_topoetcone20", &electron_topoetcone20, &b_electron_topoetcone20);
   fChain->SetBranchAddress("electron_topoetcone30", &electron_topoetcone30, &b_electron_topoetcone30);
   fChain->SetBranchAddress("electron_topoetcone40", &electron_topoetcone40, &b_electron_topoetcone40);
   fChain->SetBranchAddress("electron_lhtight", &electron_lhtight, &b_electron_lhtight);
   fChain->SetBranchAddress("electron_lhmedium", &electron_lhmedium, &b_electron_lhmedium);
   fChain->SetBranchAddress("electron_lhloose", &electron_lhloose, &b_electron_lhloose);
   fChain->SetBranchAddress("electron_lhmedium_hi", &electron_lhmedium_hi, &b_electron_lhmedium_hi);
   fChain->SetBranchAddress("electron_lhloose_hi", &electron_lhloose_hi, &b_electron_lhloose_hi);
   fChain->SetBranchAddress("electron_tp_pt", &electron_tp_pt, &b_electron_tp_pt);
   fChain->SetBranchAddress("electron_tp_eta", &electron_tp_eta, &b_electron_tp_eta);
   fChain->SetBranchAddress("electron_tp_phi", &electron_tp_phi, &b_electron_tp_phi);
   fChain->SetBranchAddress("electron_tp_charge", &electron_tp_charge, &b_electron_tp_charge);
   fChain->SetBranchAddress("electron_ntrk", &electron_ntrk, &b_electron_ntrk);
   fChain->SetBranchAddress("electron_track_pt", &electron_track_pt, &b_electron_track_pt);
   fChain->SetBranchAddress("electron_track_eta", &electron_track_eta, &b_electron_track_eta);
   fChain->SetBranchAddress("electron_track_phi", &electron_track_phi, &b_electron_track_phi);
   fChain->SetBranchAddress("electron_track_charge", &electron_track_charge, &b_electron_track_charge);
   fChain->SetBranchAddress("electron_track_d0sig", &electron_track_d0sig, &b_electron_track_d0sig);
   fChain->SetBranchAddress("electron_track_z0", &electron_track_z0, &b_electron_track_z0);
   fChain->SetBranchAddress("electron_track_vz", &electron_track_vz, &b_electron_track_vz);
   fChain->SetBranchAddress("electron_track_theta", &electron_track_theta, &b_electron_track_theta);
   fChain->SetBranchAddress("electron_pt_sys", &electron_pt_sys, &b_electron_pt_sys);
   fChain->SetBranchAddress("electron_eta_sys", &electron_eta_sys, &b_electron_eta_sys);
   fChain->SetBranchAddress("electron_phi_sys", &electron_phi_sys, &b_electron_phi_sys);
   fChain->SetBranchAddress("muon_n", &muon_n, &b_muon_n);
   fChain->SetBranchAddress("muon_pt", &muon_pt, &b_muon_pt);
   fChain->SetBranchAddress("muon_eta", &muon_eta, &b_muon_eta);
   fChain->SetBranchAddress("muon_phi", &muon_phi, &b_muon_phi);
   fChain->SetBranchAddress("muon_charge", &muon_charge, &b_muon_charge);
   fChain->SetBranchAddress("muon_tight", &muon_tight, &b_muon_tight);
   fChain->SetBranchAddress("muon_medium", &muon_medium, &b_muon_medium);
   fChain->SetBranchAddress("muon_loose", &muon_loose, &b_muon_loose);
   fChain->SetBranchAddress("muon_track_pt", &muon_track_pt, &b_muon_track_pt);
   fChain->SetBranchAddress("muon_track_eta", &muon_track_eta, &b_muon_track_eta);
   fChain->SetBranchAddress("muon_track_phi", &muon_track_phi, &b_muon_track_phi);
   fChain->SetBranchAddress("muon_track_charge", &muon_track_charge, &b_muon_track_charge);
   fChain->SetBranchAddress("muon_track_d0sig", &muon_track_d0sig, &b_muon_track_d0sig);
   fChain->SetBranchAddress("muon_track_z0", &muon_track_z0, &b_muon_track_z0);
   fChain->SetBranchAddress("muon_track_vz", &muon_track_vz, &b_muon_track_vz);
   fChain->SetBranchAddress("muon_track_theta", &muon_track_theta, &b_muon_track_theta);
   fChain->SetBranchAddress("muon_pt_sys", &muon_pt_sys, &b_muon_pt_sys);
   fChain->SetBranchAddress("muon_eta_sys", &muon_eta_sys, &b_muon_eta_sys);
   fChain->SetBranchAddress("muon_phi_sys", &muon_phi_sys, &b_muon_phi_sys);
   fChain->SetBranchAddress("ntrk", &ntrk, &b_ntrk);
   fChain->SetBranchAddress("trk_pt", &trk_pt, &b_trk_pt);
   fChain->SetBranchAddress("trk_eta", &trk_eta, &b_trk_eta);
   fChain->SetBranchAddress("trk_phi", &trk_phi, &b_trk_phi);
   fChain->SetBranchAddress("trk_charge", &trk_charge, &b_trk_charge);
   fChain->SetBranchAddress("trk_tight_primary", &trk_tight_primary, &b_trk_tight_primary);
   fChain->SetBranchAddress("trk_minbias", &trk_minbias, &b_trk_minbias);
   fChain->SetBranchAddress("trk_HItight", &trk_HItight, &b_trk_HItight);
   fChain->SetBranchAddress("trk_HIloose", &trk_HIloose, &b_trk_HIloose);
   fChain->SetBranchAddress("trk_d0", &trk_d0, &b_trk_d0);
   fChain->SetBranchAddress("trk_z0", &trk_z0, &b_trk_z0);
   fChain->SetBranchAddress("trk_vz", &trk_vz, &b_trk_vz);
   fChain->SetBranchAddress("trk_theta", &trk_theta, &b_trk_theta);
   fChain->SetBranchAddress("trk_prob_truth", &trk_prob_truth, &b_trk_prob_truth);
   fChain->SetBranchAddress("trk_truth_pt", &trk_truth_pt, &b_trk_truth_pt);
   fChain->SetBranchAddress("trk_truth_eta", &trk_truth_eta, &b_trk_truth_eta);
   fChain->SetBranchAddress("trk_truth_phi", &trk_truth_phi, &b_trk_truth_phi);
   fChain->SetBranchAddress("trk_truth_charge", &trk_truth_charge, &b_trk_truth_charge);
   fChain->SetBranchAddress("trk_truth_type", &trk_truth_type, &b_trk_truth_type);
   fChain->SetBranchAddress("trk_truth_orig", &trk_truth_orig, &b_trk_truth_orig);
   fChain->SetBranchAddress("trk_truth_barcode", &trk_truth_barcode, &b_trk_truth_barcode);
   fChain->SetBranchAddress("trk_truth_pdgid", &trk_truth_pdgid, &b_trk_truth_pdgid);
   fChain->SetBranchAddress("trk_truth_vz", &trk_truth_vz, &b_trk_truth_vz);
   fChain->SetBranchAddress("trk_truth_nIn", &trk_truth_nIn, &b_trk_truth_nIn);
   fChain->SetBranchAddress("trk_truth_isHadron", &trk_truth_isHadron, &b_trk_truth_isHadron);
   fChain->SetBranchAddress("truth_electron_n", &truth_electron_n, &b_truth_electron_n);
   fChain->SetBranchAddress("truth_electron_pt", &truth_electron_pt, &b_truth_electron_pt);
   fChain->SetBranchAddress("truth_electron_eta", &truth_electron_eta, &b_truth_electron_eta);
   fChain->SetBranchAddress("truth_electron_phi", &truth_electron_phi, &b_truth_electron_phi);
   fChain->SetBranchAddress("truth_electron_charge", &truth_electron_charge, &b_truth_electron_charge);
   fChain->SetBranchAddress("truth_electron_barcode", &truth_electron_barcode, &b_truth_electron_barcode);
   fChain->SetBranchAddress("truth_muon_n", &truth_muon_n, &b_truth_muon_n);
   fChain->SetBranchAddress("truth_muon_pt", &truth_muon_pt, &b_truth_muon_pt);
   fChain->SetBranchAddress("truth_muon_eta", &truth_muon_eta, &b_truth_muon_eta);
   fChain->SetBranchAddress("truth_muon_phi", &truth_muon_phi, &b_truth_muon_phi);
   fChain->SetBranchAddress("truth_muon_charge", &truth_muon_charge, &b_truth_muon_charge);
   fChain->SetBranchAddress("truth_muon_barcode", &truth_muon_barcode, &b_truth_muon_barcode);
   fChain->SetBranchAddress("truth_trk_n", &truth_trk_n, &b_truth_trk_n);
   fChain->SetBranchAddress("truth_trk_pt", &truth_trk_pt, &b_truth_trk_pt);
   fChain->SetBranchAddress("truth_trk_eta", &truth_trk_eta, &b_truth_trk_eta);
   fChain->SetBranchAddress("truth_trk_phi", &truth_trk_phi, &b_truth_trk_phi);
   fChain->SetBranchAddress("truth_trk_charge", &truth_trk_charge, &b_truth_trk_charge);
   fChain->SetBranchAddress("truth_trk_pdgid", &truth_trk_pdgid, &b_truth_trk_pdgid);
   fChain->SetBranchAddress("truth_trk_barcode", &truth_trk_barcode, &b_truth_trk_barcode);
   fChain->SetBranchAddress("truth_trk_isHadron", &truth_trk_isHadron, &b_truth_trk_isHadron);
   fChain->SetBranchAddress("akt4_truth_jet_n", &akt4_truth_jet_n, &b_akt4_truth_jet_n);
   fChain->SetBranchAddress("akt4_truth_jet_pt", &akt4_truth_jet_pt, &b_akt4_truth_jet_pt);
   fChain->SetBranchAddress("akt4_truth_jet_eta", &akt4_truth_jet_eta, &b_akt4_truth_jet_eta);
   fChain->SetBranchAddress("akt4_truth_jet_phi", &akt4_truth_jet_phi, &b_akt4_truth_jet_phi);
   fChain->SetBranchAddress("akt4_truth_jet_e", &akt4_truth_jet_e, &b_akt4_truth_jet_e);
   Notify();
}

Bool_t TrackTest::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TrackTest::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->GetEntry(entry);

   std::cout << "Electron track particle" << std::endl;
   std::cout << std::setw (14) << "electron_n" << std::setw (14) << "pT [GeV]" << std::setw (14) << "eta" << std::setw (14) << "phi" << std::setw (10) << "charge" << std::endl;
   std::cout << "----------------------------------------------------------------------------------------------------" << std::endl;
   for (int iE = 0; iE < electron_n; iE++) {
      std::cout << std::setw (14) << iE << std::setw (14) << electron_tp_pt->at (iE) << std::setw (14) << electron_tp_eta->at (iE) << std::setw (14) << electron_tp_phi->at (iE) << std::setw (10) << electron_tp_charge->at (iE) << std::endl;
   }
   std::cout << std::endl;

   std::cout << "Electron track particle container" << std::endl;
   std::cout << std::setw (14) << "electron_n" << std::setw (14) << "pT [GeV]" << std::setw (14) << "eta" << std::setw (14) << "phi" << std::setw (10) << "charge" << std::endl;
   std::cout << "----------------------------------------------------------------------------------------------------" << std::endl;
   for (int iE = 0; iE < electron_track_pt->size (); iE++) {
      std::cout << std::setw (14) << iE << std::setw (14) << electron_track_pt->at (iE) << std::setw (14) << electron_track_eta->at (iE) << std::setw (14) << electron_track_phi->at (iE) << std::setw (10) << electron_track_charge->at (iE) << std::endl;
   }
   std::cout << std::endl;
}
Int_t TrackTest::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TrackTest_cxx
